package com.deltatechnepal.helper;


import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.kcbl.CollectionFollowUp;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.CollectionFollowUps;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class fetchCollections {
    private Context mCtx;
    boolean result;
    DateConversion dc;
    long nextFollowUpDate;
    String ageing,uploadedOn;

    public fetchCollections() {

    }


    public boolean initializeFetch() {
        dc = new DateConversion();

        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_FETCH_COLLECTIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        // looping through json and adding to movies list
                        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                            @Override
                            public void manipulateInTransaction() {
                                try {

                                    Collection.deleteAll(Collection.class);
                                    Collection.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "COLLECTION" + "'");
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray collectionArray = obj.getJSONArray("collections");
                                    for (int i = 0; i < collectionArray.length(); i++) {

                                        JSONObject collection = collectionArray.getJSONObject(i);

                                        int customerId = collection.getInt("id");
                                        String customerMobile = collection.getString("customer_mobile");
                                        String customerName = collection.getString("customer_name");
                                        String pendingAmount = collection.getString("pending_amount");
                                        int followupStartFlag = collection.getInt("followup_start_flag");
                                        if(TextUtils.isEmpty(collection.getString("next_followup_date")))
                                            nextFollowUpDate=0;
                                        else
                                           nextFollowUpDate = dc.getMilli(collection.getString("next_followup_date"));

                                        String less_than_30 = collection.getString("less_than_30");
                                        String between_30_60= collection.getString("between_40_60");
                                        String between_60_90 = collection.getString("between_60_90");
                                        String greater_than_90 = collection.getString("between_90_120");
                                        String onAccount = collection.getString("on_account");
                                        uploadedOn = collection.getString("created_at");




                                        Collection coll = new Collection(customerId, customerMobile, customerName, pendingAmount, nextFollowUpDate,uploadedOn,less_than_30,
                                                between_30_60,between_60_90,greater_than_90,onAccount,followupStartFlag);
                                        coll.save();


                                    }

                                    if (CollectionFollowUps.count(CollectionFollowUps.class) == 0) {
                                        JSONArray followupArray = obj.getJSONArray("followups");
                                        for (int i = 0; i < followupArray.length(); i++) {

                                            JSONObject followup = followupArray.getJSONObject(i);

                                            String customerMobile = followup.getString("collection_id");
                                            int followUpStatus = followup.getInt("followup_status");
                                            int collectedAmount = followup.getInt("collected_amount");
                                            int nextFollowUpMode = followup.getInt("next_followup_mode");
                                            String followUpNote = followup.getString("followup_note");
                                            long nextFollowUpDate = dc.getMilli(followup.getString("next_followup_date"));
                                            long createdAt = dc.getFormattedMilli("yyyy-MM-dd HH:mm:ss",followup.getString("created_at"));


                                            CollectionFollowUps coll = new CollectionFollowUps(customerMobile, followUpStatus, collectedAmount, nextFollowUpMode, nextFollowUpDate, followUpNote, createdAt);
                                            coll.save();


                                        }
                                    }

                                    if (ActivityLog.count(ActivityLog.class) == 0) {
                                        JSONArray activityLog = obj.getJSONArray("activity");
                                        for (int i = 0; i < activityLog.length(); i++) {

                                            JSONObject followup = activityLog.getJSONObject(i);

                                            int activity_type = followup.getInt("activity_type");
                                            long activity_time = followup.getLong("activity_time");
                                            long activity_id = followup.getLong("activity_id");
                                            ActivityLog aLog = new ActivityLog(activity_type, activity_time, activity_id);
                                            aLog.save();


                                        }
                                    }


                                    result = true;

                                } catch (JSONException e) {
                                    result = false;

                                }
                            }
                        });


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result = false;

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("salesman_name", String.valueOf(SharedPreManager.getInstance(mCtx).getUser().getUsername()));
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);

        return result;

    }
}

