package com.deltatechnepal.helper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.deltatechnepal.service.BrochureFetchJob;
import com.deltatechnepal.service.CategoriesFetchJob;
import com.deltatechnepal.service.CollectionsFetchJob;
import com.deltatechnepal.service.EnquirySyncJob;
import com.deltatechnepal.service.NotificationSyncJob;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class SyncJobCreator implements JobCreator {


    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        switch (tag) {
            case EnquirySyncJob.TAG:
                return new EnquirySyncJob();
            case NotificationSyncJob.TAG:
                return new NotificationSyncJob();
            case BrochureFetchJob.TAG:
                return new BrochureFetchJob();
            case CategoriesFetchJob.TAG:
                return new CategoriesFetchJob();
            case CollectionsFetchJob.TAG:
                return new CollectionsFetchJob();
            default:
                return null;
        }
    }
}
