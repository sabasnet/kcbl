package com.deltatechnepal.helper;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.kcbl.R;


public class CustomToast {


    public void showToast(Activity activity, String message) {


        LayoutInflater inflater = activity.getLayoutInflater();

        View view = inflater.inflate(R.layout.done_toast, null);
        TextView tv = view.findViewById(R.id.toastMessage);
        Toast toast = new Toast(activity);
        tv.setText(message);
        toast.setGravity(Gravity.BOTTOM, 0, 50);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }
}
