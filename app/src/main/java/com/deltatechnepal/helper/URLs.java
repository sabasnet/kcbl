package com.deltatechnepal.helper;

public class URLs {

     //private static final String ROOT_URL = "http://192.168.100.41/kcbl_latest/kcbl";
      //private static final String ROOT_URL = "http://kcbl.deltatech.com.np";
    private static final String ROOT_URL = "http://sales.kcbl.com.np";

    public static final String URL_LOGIN = ROOT_URL + "/api/login";
    public static final String URL_ENQUIRIES = ROOT_URL + "/api/fetchEnquiries";
    public static final String URL_PRODUCTS = ROOT_URL + "/api/fetchProducts";
    public static final String URL_ADD_ENQUIRY = ROOT_URL + "/api/insertEnquiry";
    public static final String URL_UPDATE_ENQUIRY = ROOT_URL + "/api/updateEnquiry";
    public static final String URL_FETCH_COUNTERS = ROOT_URL + "/api/fetchCounters";
    public static final String URL_UPDATE_RESULT = ROOT_URL + "/api/updateResult";
    public static final String URL_FETCH_SALESMAN_LIST = ROOT_URL + "/api/fetchSalesmanList";
    public static final String URL_FETCH_NOTIFICATIONS = ROOT_URL + "/api/fetchNotifications";
    public static final String URL_TRANSFER_ENQUIRY = ROOT_URL + "/api/transferEnquiry";
    public static final String URL_ADD_FOLLOWUP = ROOT_URL + "/api/addFollowUp";
    public static final String URL_ADD_FOLLOWUP_COLLECTIONS = ROOT_URL + "/api/addCollectionFollowUp";
    public static final String URL_PROFILE_IMAGE = ROOT_URL + "/cms/public/uploads/user-images/";
    public static final String URL_PRODUCT_IMAGES = ROOT_URL + "/cms/public/uploads/product-images/";
    public static final String URL_PRODUCT_BROCHURES = ROOT_URL + "/cms/public/uploads/product_brochures/";
    public static final String URL_SYNC_DATA = ROOT_URL + "/api/syncEnquiry";
    public static final String URL_SYNC_NOTIFICATIONS = ROOT_URL + "/api/syncNotifications";
    public static final String URL_EMAIL_DOCUMENTS = ROOT_URL + "/api/sendDocumentsNew";
    public static final String URL_FETCH_BROCHURES = ROOT_URL + "/api/fetchBrochures";
    public static final String URL_FETCH_TENQUIRY = ROOT_URL + "/api/fetchTransEnquiry";
    public static final String URL_FETCH_CATEGORIES = ROOT_URL + "/api/fetchCategories";
    public static final String URL_FETCH_COLLECTIONS = ROOT_URL + "/api/fetchCollections";
    public static final String URL_FETCH_QUOTATIONS = ROOT_URL + "/api/fetchQuotations";




}