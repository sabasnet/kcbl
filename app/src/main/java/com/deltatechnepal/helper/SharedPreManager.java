package com.deltatechnepal.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.deltatechnepal.kcbl.CollectionFollowUp;
import com.deltatechnepal.kcbl.LoginActivity;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.kcbl.SplashActivity;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Brochure;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.CollectionFollowUps;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.orm.Token;
import com.deltatechnepal.orm.User;
import com.deltatechnepal.orm.Subsource;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.IOException;

public class SharedPreManager {

    //the constants
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_USERNAME = "keyusername";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_IMAGE = "keyimage";
    private static final String KEY_DESIGNATION = "keydesignation";
    private static final String KEY_NUMBER = "keynumber";
    private static final String KEY_ID = "keyid";
    private static final String KEY_NOTIFICATION = "check_notifications";

    private static SharedPreManager mInstance;
    private Context mCtx;

    private SharedPreManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPreManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreManager(context);
        }
        return mInstance;
    }

    //method to let the user login
    //this method will store the user data in shared preferences
    public void userLogin(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putString(KEY_USERNAME, user.getUsername());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_IMAGE, user.getImage());
        editor.putString(KEY_DESIGNATION, user.getDesignation());
        editor.putString(KEY_NUMBER, user.getNumber());
        editor.apply();
    }

    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if(!TextUtils.isEmpty(sharedPreferences.getString(KEY_USERNAME, null)))
        return true;
        else
            return false;
    }

    //this method will give the logged in user
    public User getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getInt(KEY_ID, -1),
                sharedPreferences.getString(KEY_USERNAME, null),
                sharedPreferences.getString(KEY_EMAIL, null),
                sharedPreferences.getString(KEY_IMAGE, null),
                sharedPreferences.getString(KEY_DESIGNATION, null),
                sharedPreferences.getString(KEY_NUMBER, null)
        );
    }

    public void setCheckNotification(boolean value) {


        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_NOTIFICATION, value);
        editor.apply();
    }


    public boolean checkForNotifications() {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_NOTIFICATION, true);

    }


    //this method will logout the user
    public void logout() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    /*redirecting to login page*/
                    Intent loginIntent = new Intent(mCtx, SplashActivity.class);
                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mCtx.startActivity(loginIntent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        Offenquiry.deleteAll(Offenquiry.class);
        Offenquiry.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "OFFENQUIRY" + "'");
        Counter.deleteAll(Counter.class);
        Counter.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "COUNTER" + "'");
        EnquiryFollowUps.deleteAll(EnquiryFollowUps.class);
        EnquiryFollowUps.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "ENQUIRY_FOLLOW_UPS" + "'");
        CollectionFollowUps.deleteAll(CollectionFollowUps.class);
        CollectionFollowUps.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "COLLECTION_FOLLOW_UPS" + "'");
        Collection.deleteAll(Collection.class);
        Collection.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "COLLECTION" + "'");
        ActivityLog.deleteAll(ActivityLog.class);
        ActivityLog.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "ACTIVITY_LOG" + "'");


        Subsource.deleteAll(Subsource.class);
        Subsource.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "SUBSOURCE" + "'");
        Notification.deleteAll(Notification.class);
        Notification.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "NOTIFICATION" + "'");
        Products.deleteAll(Products.class);
        Products.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "PRODUCTS" + "'");
        Brochure.deleteAll(Brochure.class);
        Brochure.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "BROCHURE" + "'");
        Category.deleteAll(Category.class);
        Category.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "CATEGORY" + "'");
        Token.deleteAll(Token.class);
        Token.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "TOKEN" + "'");


    }
}