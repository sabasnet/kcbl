package com.deltatechnepal.helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.kcbl.AddEnquiryActivity;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Uenquiry;
import com.google.gson.Gson;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class syncNotifications {
    private Context mCtx;
    boolean result;
    List<Notification> notif = new ArrayList<>();
    String strData;

    public syncNotifications() {

    }


    public boolean initializeSync() {


        notif = Notification.find(Notification.class, "mark_read = ?", "1");
        strData = new Gson().toJson(notif);


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_SYNC_NOTIFICATIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final String finalData = response;
                        try {
                            JSONObject obj = new JSONObject(finalData);
                            if (obj.getBoolean("success")) {


                                for (int i = 0; i < notif.size(); i++) {

                                    notif.get(i).delete();
                                }
                                result = true;

                            } else {
                                result = false;
                            }
                        } catch (JSONException e) {

                        }


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                result = false;

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("marked_read", strData);


                return params;
            }
        };

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


        return result;
    }
}
