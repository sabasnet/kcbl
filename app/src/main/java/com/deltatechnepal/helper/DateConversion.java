package com.deltatechnepal.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateConversion {

    Date date;
    SimpleDateFormat formatter;
    Calendar mcurrentDate;
    String formattedDate;


    public String getCurrentDate() {
        formatter = new SimpleDateFormat("MM/dd/yyyy");
        mcurrentDate = Calendar.getInstance();
        String currentDate = formatter.format(mcurrentDate.getTime());

        return currentDate;
    }


    public String getTomorrowDateinMilli() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        formattedDate = sdf.format(dt);

        try {
            date = sdf.parse(formattedDate);
        } catch (Exception e) {

        }
        return String.valueOf(date.getTime());

    }


    public String getCurrentDateinMilli() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        formattedDate = sdf.format(new Date());

        try {
            date = sdf.parse(formattedDate);
        } catch (Exception e) {

        }
        return String.valueOf(date.getTime());

    }

    public long getCurrentDateinMilliseonds(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        formattedDate = sdf.format(new Date());

        try {
            date = sdf.parse(formattedDate);
        } catch (Exception e) {

        }


        return date.getTime();


    }


    public String getFormattedDate(String format, long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(format);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public long getFormattedMilli(String format,String paramDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            date = sdf.parse(paramDate);
        } catch (Exception e) {

        }
        return date.getTime();
    }

    public long getMilli(String paramDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        try {
            date = sdf.parse(paramDate);
        } catch (Exception e) {

        }
        return date.getTime();
    }
    public long get(String paramDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        try {
            date = sdf.parse(paramDate);
        } catch (Exception e) {

        }
        return date.getTime();
    }

    public long getMilliAlt(String paramDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:m:s");

        try {
            date = sdf.parse(paramDate);
        } catch (Exception e) {

        }
        return date.getTime();
    }


}
