package com.deltatechnepal.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.kcbl.EprActivity;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResyncEnquiries {
    private String TAG = EprActivity.class.getSimpleName();
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_ID = "keyid";
    private static JSONObject obj;
    private static JSONArray enquiriesArray, eFollowArray;
    private static ResyncEnquiries mInstance;
    private static Context mCtx;
    private static Offenquiry off;
    private static DateConversion dc;
    private static EnquiryFollowUps followUps;
    public static String Status;

    private ResyncEnquiries(Context context) {
        mCtx = context;
    }


    public static synchronized ResyncEnquiries getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ResyncEnquiries(context);
        }
        return mInstance;
    }


    public void fetchserverEnquiries() {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_ENQUIRIES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d(TAG, response);
                        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                            @Override
                            public void manipulateInTransaction() {
                                Offenquiry.deleteAll(Offenquiry.class);
                                Offenquiry.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "OFFENQUIRY" + "'");
                                EnquiryFollowUps.deleteAll(EnquiryFollowUps.class);
                                EnquiryFollowUps.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "ENQUIRY_FOLLOW_UPS" + "'");

                                if (Offenquiry.count(Offenquiry.class) == 0 && EnquiryFollowUps.count(EnquiryFollowUps.class) == 0) {
                                    try {
                                        obj = new JSONObject(response);
                                        enquiriesArray = obj.getJSONArray("data");
                                        eFollowArray = obj.getJSONArray("followups");
                                        //Enquiries save
                                        dc = new DateConversion();
                                        for (int i = 0; i < (enquiriesArray.length()); i++) {


                                            JSONObject enquiry = enquiriesArray.getJSONObject(i);
                                            long enquiryId = enquiry.getLong("id");
                                            String productName = enquiry.getString("name");
                                            int sourceofEnquiry = enquiry.getInt("source_of_enquiry");
                                            int subSource = enquiry.getInt("sub_source");
                                            String customerName = enquiry.getString("customer_name");
                                            String customerAddress = enquiry.getString("customer_address");
                                            int titleName = enquiry.getInt("contact_title");
                                            String contactPerson = enquiry.getString("contact_person");
                                            int contactAge = enquiry.getInt("contact_age");
                                            String contactOccupation = enquiry.getString("contact_occupation");
                                            String contactPhone = enquiry.getString("contact_phone");
                                            String contactNumber = enquiry.getString("contact_number");
                                            String altContactNumber = enquiry.getString("alt_contact_number");
                                            String contactAddress = enquiry.getString("contact_address");
                                            String contactEmail = enquiry.getString("contact_email");
                                            int purchaseMode = enquiry.getInt("purchase_mode");
                                            int firstTime = enquiry.getInt("first_time");
                                            int interestedComp = enquiry.getInt("competition_interest");
                                            int status = enquiry.getInt("status");
                                            String locationofEnquiry = enquiry.getString("location_of_enquiry");
                                            String dateofEnquiry = enquiry.getString("doe");
                                            String nextFollowUpDate = enquiry.getString("next_followup_date");
                                            long rate = enquiry.getLong("rate");
                                            int result = enquiry.getInt("result");
                                            int reason = enquiry.getInt("reason");
                                            int reg = enquiry.getInt("is_reg");
                                            String note = enquiry.getString("note");
                                            String bankName = enquiry.getString("bank_name");


                                            off = new Offenquiry(enquiryId, productName, sourceofEnquiry, subSource, customerName, customerAddress, titleName,
                                                    contactPerson, contactAge,
                                                    contactOccupation, contactPhone, contactNumber, altContactNumber, contactAddress, contactEmail, purchaseMode,
                                                    firstTime, interestedComp,
                                                    status, locationofEnquiry, dc.getMilli(dateofEnquiry), dc.getMilli(nextFollowUpDate),
                                                    rate, result, reason, reg, note, bankName);
                                            off.save();
                                        }

                                        //FollowUp History Save

                                        for (int i = 0; i < eFollowArray.length(); i++) {

                                            JSONObject efollow = eFollowArray.getJSONObject(i);

                                            long created = dc.getMilliAlt(efollow.getString("created_at"));
                                            long next = dc.getMilli(efollow.getString("next_followup_date"));
                                            followUps = new EnquiryFollowUps(efollow.getString("enquiry_id"),
                                                    efollow.getString("product_id"), efollow.getInt("next_followup_mode")
                                                    , efollow.getString("remark"), created, next
                                                    , efollow.getInt("followup_status"));

                                            followUps.save();

                                        }


                                        Handler handler = new Handler(Looper.getMainLooper());

                                        handler.post(new Runnable() {

                                            @Override
                                            public void run() {
                                               Toast.makeText(mCtx,"Synced Successfully",Toast.LENGTH_SHORT).show();
                                            }
                                        });



                                    } catch (Exception e) {
                                        Status = "Error";
                                        e.printStackTrace();
                                    }
                                }
                            }


                        });
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());

                Toast.makeText(mCtx, error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                String id = String.valueOf(sharedPreferences.getInt(KEY_ID, 0));
                params.put("id", id);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


    }

}
