package com.deltatechnepal.helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.kcbl.AddEnquiryActivity;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Uenquiry;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class syncEnquiry {
    private Context mCtx;
    boolean result;

    public syncEnquiry() {

    }


    public boolean initializeSync() {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final String finalData = response;
                        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                            @Override
                            public void manipulateInTransaction() {
                                try {
                                    JSONObject obj = new JSONObject(finalData);
                                    JSONArray enquiriesArray = obj.getJSONArray("data");
                                    //Enquiries save
                                    DateConversion dc = new DateConversion();
                                    for (int i = 0; i < (enquiriesArray.length()); i++) {
                                        JSONObject enquiry = enquiriesArray.getJSONObject(i);
                                        long enquiryId = enquiry.getLong("id");
                                        String productName = enquiry.getString("name");
                                        int sourceofEnquiry = enquiry.getInt("source_of_enquiry");
                                        int subSource = enquiry.getInt("sub_source");
                                        String customerName = enquiry.getString("customer_name");
                                        String customerAddress = enquiry.getString("customer_address");
                                        int titleName = enquiry.getInt("contact_title");
                                        String contactPerson = enquiry.getString("contact_person");
                                        int contactAge = enquiry.getInt("contact_age");
                                        String contactOccupation = enquiry.getString("contact_occupation");
                                        String contactPhone = enquiry.getString("contact_phone");
                                        String contactNumber = enquiry.getString("contact_number");
                                        String altContactNumber = enquiry.getString("alt_contact_number");
                                        String contactAddress = enquiry.getString("contact_address");
                                        String contactEmail = enquiry.getString("contact_email");
                                        int purchaseMode = enquiry.getInt("purchase_mode");
                                        int firstTime = enquiry.getInt("first_time");
                                        int interestedComp = enquiry.getInt("competition_interest");
                                        int status = enquiry.getInt("status");
                                        String locationofEnquiry = enquiry.getString("location_of_enquiry");
                                        String dateofEnquiry = enquiry.getString("doe");
                                        String nextFollowUpDate = enquiry.getString("next_followup_date");
                                        long rate = enquiry.getLong("rate");
                                        int result = 0;
                                        int reason = enquiry.getInt("reason");
                                        int reg = enquiry.getInt("is_reg");
                                        String note = enquiry.getString("note");
                                        String bankName = enquiry.getString("bank_name");

                                        Offenquiry off = new Offenquiry(enquiryId, productName, sourceofEnquiry, subSource, customerName, customerAddress, titleName,
                                                contactPerson, contactAge,
                                                contactOccupation, contactPhone, contactNumber, altContactNumber, contactAddress, contactEmail, purchaseMode,
                                                firstTime, interestedComp,
                                                status, locationofEnquiry, dc.getMilli(dateofEnquiry), dc.getMilli(nextFollowUpDate),
                                                rate, result, reason, reg, note, bankName);
                                        off.save();
                                    }

                                    Uenquiry.deleteAll(Uenquiry.class);
                                    Uenquiry.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "UENQUIRY" + "'");
                                    result = true;
                                } catch (JSONException e) {

                                }


                            }
                        });


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                result = false;

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("unsynced_data", Uenquiry.getUnsyncedData());


                return params;
            }
        };

        // Adding request to request queue

        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(req);


        return result;
    }
}
