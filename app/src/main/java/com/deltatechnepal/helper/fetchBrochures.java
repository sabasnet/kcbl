package com.deltatechnepal.helper;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.orm.Brochure;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Products;
import com.google.gson.Gson;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class fetchBrochures {
    private Context mCtx;
    boolean result;
    List<Notification> notif = new ArrayList<>();
    String strData;

    public fetchBrochures() {

    }


    public boolean initializeFetch() {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_FETCH_BROCHURES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        // looping through json and adding to movies list
                        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                            @Override
                            public void manipulateInTransaction() {
                                try {

                                    JSONObject obj = new JSONObject(response);

                                    if (Brochure.count(Brochure.class) == 0) {
                                        JSONArray productsArray = obj.getJSONArray("brochures");
                                        for (int i = 0; i < productsArray.length(); i++) {

                                            JSONObject product = productsArray.getJSONObject(i);

                                            int productId = product.getInt("product_id");
                                            String brochure = product.getString("brochure");


                                            Brochure brochureList = new Brochure(productId, brochure);
                                            brochureList.save();


                                        }

                                    }
                                    result = true;

                                } catch (JSONException e) {
                                    result = false;

                                }
                            }
                        });


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result = false;

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);
        return result;

    }
}

