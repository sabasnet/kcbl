package com.deltatechnepal.helper;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.deltatechnepal.orm.Offenquiry;

import java.util.List;

public class OffenquiryDiffCallback extends DiffUtil.Callback {

    private final List<Offenquiry> mOldOffenquiryList;
    private final List<Offenquiry> mNewOffenquiryList;

    public OffenquiryDiffCallback(List<Offenquiry> oldOffenquiryList, List<Offenquiry> newOffEnquiryList) {
        this.mOldOffenquiryList = oldOffenquiryList;
        this.mNewOffenquiryList = newOffEnquiryList;
    }

    @Override
    public int getOldListSize() {
        return mOldOffenquiryList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewOffenquiryList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldOffenquiryList.get(oldItemPosition).getId() == mNewOffenquiryList.get(
                newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Offenquiry oldOffenquiryList = mOldOffenquiryList.get(oldItemPosition);
        final Offenquiry newOffenquiryList = mNewOffenquiryList.get(newItemPosition);

        return oldOffenquiryList.getCustomDiffChecker().equals(newOffenquiryList.getCustomDiffChecker());
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}