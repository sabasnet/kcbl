package com.deltatechnepal.helper;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;

import java.util.List;

public class EnquiryFollowUpDiffCallback extends DiffUtil.Callback {

    private final List<EnquiryFollowUps> mOldOffenquiryList;
    private final List<EnquiryFollowUps> mNewOffenquiryList;

    public EnquiryFollowUpDiffCallback(List<EnquiryFollowUps> oldOffenquiryList, List<EnquiryFollowUps> newOffEnquiryList) {
        this.mOldOffenquiryList = oldOffenquiryList;
        this.mNewOffenquiryList = newOffEnquiryList;
    }

    @Override
    public int getOldListSize() {
        return mOldOffenquiryList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewOffenquiryList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldOffenquiryList.get(oldItemPosition).getId() == mNewOffenquiryList.get(
                newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final EnquiryFollowUps oldOffenquiryList = mOldOffenquiryList.get(oldItemPosition);
        final EnquiryFollowUps newOffenquiryList = mNewOffenquiryList.get(newItemPosition);

        return oldOffenquiryList.getCreatedAtDate().equals(newOffenquiryList.getCreatedAtDate());
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}