package com.deltatechnepal.utility;

/**
 * Created by DeltaTech on 1/18/2018.
 */

public class MConstant {
    public static final String APP_NAME = "KCBL";
    public static final String MY_APP_ID = "com.deltatechnepal.kcbl";
    public static final String DEVELOPER_URL = "http://www.deltatechnepal.com";
    public static final String HELP_LINE_NUM = "";

    //Server
    //public static final String SERVER = "http://192.168.100.228/foodpal";
    public static final String SERVER = "http://192.168.100.12/foodpal";

    public static final String API_ID_ACCESS_TOKEN_STRING = "qwertyuiopasdfghjklzxcv";
    public static final String API_END = SERVER + "/testApi";

    /*For Encryption*/
    public static final String  PREFERENCE_NAME = "kcbl-preference";
    public static final String  PREFERENCE_ENCRYPT_KEY = "kcbl";
}













