package com.deltatechnepal.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.deltatechnepal.helper.EnquiryFollowUpDiffCallback;
import com.deltatechnepal.helper.OffenquiryDiffCallback;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;

import java.util.Calendar;
import java.util.List;

;

public class EnquiryFollowUpAdapter extends RecyclerView.Adapter<EnquiryFollowUpAdapter.ViewHolder> {

    private List<EnquiryFollowUps> enquiryList;
    private Activity activity;

    private Spinner reasons;
    String semi = ": ", mode, status;
    int adapterPosition;
    ProgressDialog progressDialog;
    private int lastPosition = -1;


    public EnquiryFollowUpAdapter(Activity activity, List<EnquiryFollowUps> enquiryList) {
        this.enquiryList = enquiryList;
        this.activity = activity;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvMode, tvStatus, tvCreatedAt, tvDatedFor, tvRemark;
        public ConstraintLayout viewBackground, viewForeground;
        public ImageButton btnWin, btnLoss, btnDrop, btnFollowUp;
        public View cornerRibbon;

        public ViewHolder(View view) {
            super(view);
            this.tvMode = view.findViewById(R.id.tvFollowUpMode);
            this.tvStatus = view.findViewById(R.id.tvFollowUpStatus);
            this.tvCreatedAt = view.findViewById(R.id.tvFollowUpCreatedAt);
            this.tvDatedFor = view.findViewById(R.id.tvFollowUpDatedFor);
            this.tvRemark = view.findViewById(R.id.tvFollowUpRemark);
            this.cornerRibbon = view.findViewById(R.id.corner_ribbon);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {


        }

    }

    @Override
    public EnquiryFollowUpAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.followup_history_row, viewGroup, false);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {

        switch (enquiryList.get(i).nextFollowUpMode) {
            case 0:
                mode = "N/A";
                break;
            case 1:
                mode = "Visit";
                break;
            case 2:
                mode = "Call";
                break;

        }


        switch (enquiryList.get(i).followUpStatus) {
            case 1:
                status = "Received";
                break;
            case 2:
                status = "Not Reachable";
                break;
            case 3:
                status = "Not Received";
                break;
            case 4:
                status = "Met";
                break;
            case 5:
                status = "Not Met";
                break;



        }

        holder.tvMode.setText(semi + mode);
        holder.tvStatus.setText(semi + status);
        holder.tvCreatedAt.setText(semi + enquiryList.get(i).getCreatedAtDate());

        if (enquiryList.get(i).nextFollowUpDate != 0)
            holder.tvDatedFor.setText(semi + enquiryList.get(i).getNextFollowUpDate());
        else
            holder.tvDatedFor.setText(semi + "N/A");

        holder.tvRemark.setText(semi + enquiryList.get(i).remark);

        // Here you apply the animation when the view is bound

        setAnimation(holder.itemView, i);


    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    public void updateOffenquiryListItems(List<EnquiryFollowUps> history) {
        final EnquiryFollowUpDiffCallback diffCallback = new EnquiryFollowUpDiffCallback(this.enquiryList, history);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.enquiryList.clear();
        this.enquiryList.addAll(history);
        diffResult.dispatchUpdatesTo(this);
    }

    public void NextFollowUp() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.next_follow_up_decider_dialog, null);
        dialogBuilder.setView(dialogView);

        RadioGroup rGroup = dialogView.findViewById(R.id.status_radio_group);
        dialogBuilder.setTitle("Next FollowUp Mode");
        final AlertDialog b = dialogBuilder.create();
        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    // Changes the textview's text to "Checked: example radiobutton text"
                    String selected = checkedRadioButton.getText().toString();
                    if (selected.matches("Visit")) {
                        b.dismiss();
                        openCalendarDialog(1);
                    } else {
                        b.dismiss();
                        openCalendarDialog(2);
                    }
                }

            }
        });


        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);

    }


    public void openCalendarDialog(int follow_mode) {
        // To show current date in the datepicker
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(
                activity, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);
                calendar.set(Calendar.HOUR_OF_DAY, 10);
                calendar.set(Calendar.MINUTE, 45);
                calendar.set(Calendar.SECOND, 0);
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", calendar.getTimeInMillis());
                intent.putExtra("allDay", true);
                intent.putExtra("endTime", calendar.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra("title", " FollowUp");
                activity.startActivity(intent);

            }
        }, mYear, mMonth, mDay);

        mDatePicker.setTitle("Select FollowUp Date");
        Calendar cal = Calendar.getInstance();

        switch (follow_mode) {
            case 1:
                cal.add(Calendar.DAY_OF_MONTH, 7);
                mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
                break;
            case 2:
                cal.add(Calendar.DAY_OF_MONTH, 1);
                mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
                break;

        }

        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }


    @Override
    public int getItemCount() {
        return (enquiryList.size());
    }


    public void updateFollowUpListItems(List<EnquiryFollowUps> followUps) {
        final EnquiryFollowUpDiffCallback diffCallback = new EnquiryFollowUpDiffCallback(this.enquiryList, followUps);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.enquiryList.clear();
        this.enquiryList.addAll(followUps);
        diffResult.dispatchUpdatesTo(this);
    }


}



