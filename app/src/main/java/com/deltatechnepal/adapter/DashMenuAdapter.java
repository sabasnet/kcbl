package com.deltatechnepal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.model.DashMenu;
import com.deltatechnepal.orm.Counter;

import java.util.List;

/**
 * Created by DeltaTech on 4/11/2018.
 */

public class DashMenuAdapter extends BaseAdapter {
    private Context mContext;
    private List<DashMenu> menuList;
    private List<Counter> counterList;
    private LayoutInflater inflater;

    public DashMenuAdapter(Context context, List<DashMenu> menuList) {
        this.mContext = context;
        this.menuList = menuList;
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int i) {
        return menuList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DashMenu menu = menuList.get(position);
        if (inflater == null)
            inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) convertView = inflater.inflate(R.layout.grid_item_dash, null);
        TextView tvTitle = convertView.findViewById(R.id.tvTitle);
        ImageView ivMenu = convertView.findViewById(R.id.ivMenu);
        TextView tvCounter = convertView.findViewById(R.id.tvCounter);
        if (position == 4 || position == 5) {
            tvCounter.setVisibility(View.INVISIBLE);

        } else {
            tvCounter.setText(String.valueOf(menuList.get(position).counter));
        }
        tvTitle.setText(menuList.get(position).title);
        ivMenu.setImageResource(menuList.get(position).drawable);
        return convertView;
    }


}
