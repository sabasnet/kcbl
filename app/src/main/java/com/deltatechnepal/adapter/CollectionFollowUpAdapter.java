package com.deltatechnepal.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.EnquiryFollowUpDiffCallback;
import com.deltatechnepal.kcbl.CollectionFollowUp;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.CollectionFollowUps;
import com.deltatechnepal.orm.EnquiryFollowUps;

import java.util.Calendar;
import java.util.List;

;

public class CollectionFollowUpAdapter extends RecyclerView.Adapter<CollectionFollowUpAdapter.ViewHolder> {

    private List<CollectionFollowUps> collectionList;
    private Activity activity;

    private Spinner reasons;
    String semi = ": ", mode, status;
    int adapterPosition;
    ProgressDialog progressDialog;
    private int lastPosition = -1;
    DateConversion dc = new DateConversion();


    public CollectionFollowUpAdapter(Activity activity, List<CollectionFollowUps> collectionList) {
        this.collectionList = collectionList;
        this.activity = activity;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvMode, tvStatus, tvCreatedAt, tvDatedFor, tvRemark, tvCollectedAmount;
        public ConstraintLayout viewBackground, viewForeground;
        public ImageButton btnWin, btnLoss, btnDrop, btnFollowUp;
        public View cornerRibbon;

        public ViewHolder(View view) {
            super(view);
            this.tvMode = view.findViewById(R.id.tvFollowUpMode);
            this.tvStatus = view.findViewById(R.id.tvFollowUpStatus);
            this.tvCreatedAt = view.findViewById(R.id.tvFollowUpCreatedAt);
            this.tvDatedFor = view.findViewById(R.id.tvFollowUpDatedFor);
            this.tvRemark = view.findViewById(R.id.tvFollowUpRemark);
            this.cornerRibbon = view.findViewById(R.id.corner_ribbon);
            this.tvCollectedAmount = view.findViewById(R.id.tvCollectedAmount);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {


        }

    }

    @Override
    public CollectionFollowUpAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.collection_history_row, viewGroup, false);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {

        switch (collectionList.get(i).nextFollowUpMode) {
            case 0:
                mode = "N/A";
                break;
            case 1:
                mode = "Visit";
                break;
            case 2:
                mode = "Call";
                break;

        }
        switch (collectionList.get(i).followUpStatus) {
            case 1:
                status = "Received";
                break;
            case 2:
                status = "Not Reachable";
                break;
            case 3:
                status = "Not Received";
                break;
            case 4:
                status = "Met";
                break;
            case 5:
                status = "Not Met";
                break;


        }

        holder.tvMode.setText(semi + mode);
        holder.tvStatus.setText(semi + status);
        holder.tvCreatedAt.setText(semi + dc.getFormattedDate("MM/dd/yyyy H:m:s", collectionList.get(i).createdAt));
        holder.tvCollectedAmount.setText(semi + "Rs." + collectionList.get(i).collectedAmount);


        if (collectionList.get(i).nextFollowUpDate != 0)
            holder.tvDatedFor.setText(semi + dc.getFormattedDate("MM/dd/yyyy", collectionList.get(i).nextFollowUpDate));
        else
            holder.tvDatedFor.setText(semi + "N/A");

        holder.tvRemark.setText(semi + collectionList.get(i).followUpNote);

        // Here you apply the animation when the view is bound

        setAnimation(holder.itemView, i);


    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return (collectionList.size());
    }


}



