package com.deltatechnepal.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.deltatechnepal.kcbl.EnquiryDetailsActivity;
import com.deltatechnepal.kcbl.QuotationProductsListActivity;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.Quotation;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class QuotationListAdapter extends RecyclerView.Adapter<QuotationListAdapter.ViewHolder> {
    private List<Quotation> quotationList;
    private Activity activity;

    public QuotationListAdapter(Activity activity, List<Quotation> list) {
        this.quotationList = list;
        this.activity = activity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvDocumentType, tvGrandTotal, tvSentDate;


        public ViewHolder(View v) {
            super(v);
            tvDocumentType = v.findViewById(R.id.tvDocumentTypeValue);
            tvGrandTotal = v.findViewById(R.id.tvGrandTotalValue);
            tvSentDate = v.findViewById(R.id.tvSentDateValue);
            v.setOnClickListener(this);


        }
        @Override
        public void onClick(View v) {

            Intent myIntent = new Intent(activity, QuotationProductsListActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
            myIntent.putExtra("products_list", quotationList.get(getAdapterPosition()).productList);
            activity.startActivity(myIntent, options.toBundle());
        }
    }

    @Override
    public QuotationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_quotation_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuotationListAdapter.ViewHolder holder, int position) {
        final Quotation quotation = quotationList.get(position);
        holder.tvDocumentType.setText(quotation.documentType);
        holder.tvGrandTotal.setText("Rs. "+quotation.grandTotal);
        if(quotation.sentDate.matches("null"))
        holder.tvSentDate.setText("Not Available");
        else
        holder.tvSentDate.setText(quotation.sentDate);

    }

    @Override
    public int getItemCount() {
        return (null != quotationList ? quotationList.size() : 0);
    }


}







































