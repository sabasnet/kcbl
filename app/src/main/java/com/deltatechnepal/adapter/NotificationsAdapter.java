package com.deltatechnepal.adapter;

import android.animation.LayoutTransition;
import android.animation.ValueAnimator;
import android.app.Activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.Button;

import android.widget.TextView;

import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.kcbl.AddEnquiryActivity;
import com.deltatechnepal.kcbl.EnquiryDetailsActivity;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;
import com.transitionseverywhere.extra.Scale;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private List<Notification> notifList;
    private Activity activity;
    String semi = ": ";
    private int lastPosition = -1;
    TextView temp = null;
    boolean expanded = false;
    DateConversion dc = new DateConversion();


    public NotificationsAdapter(Activity activity, List<Notification> notifList) {
        this.notifList = notifList;
        this.activity = activity;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvLetterIcon, tvTitle, tvDesc, tvDate, tvTransBy;
        public Button btnClear, btnView;
        final ViewGroup transitionsContainer;
        public ConstraintLayout clayout;


        public ViewHolder(View view) {
            super(view);
            this.transitionsContainer = view.findViewById(R.id.card_view);
            this.tvLetterIcon = view.findViewById(R.id.tv_notification_letter_icon);
            this.tvTitle = view.findViewById(R.id.tv_notification_title);
            this.tvDesc = view.findViewById(R.id.tv_notification_description);
            this.tvTransBy = view.findViewById(R.id.tv_trans_by);
            this.tvDate = view.findViewById(R.id.tv_notification_date);
            this.btnClear = view.findViewById(R.id.btn_notification_clear);
            this.btnView = view.findViewById(R.id.btn_notification_view);


            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    int category = notifList.get(position).category;
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);

                    switch (category) {
                        case 1:
                            Intent myIntent1 = new Intent(activity, EnquiryDetailsActivity.class);
                            myIntent1.putExtra("enquiry_id", notifList.get(position).actionId);
                            activity.startActivity(myIntent1, options.toBundle());
                            break;
                        case 2:
                            Intent myIntent2 = new Intent(activity, AddEnquiryActivity.class);
                            ActivityOptions options2 =
                                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
                            myIntent2.putExtra("product_id", notifList.get(position).actionId);
                            activity.startActivity(myIntent2, options.toBundle());
                            break;
                        case 4:
                            Intent myIntent3 = new Intent(activity, AddEnquiryActivity.class);
                            ActivityOptions options3 =
                                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
                            myIntent3.putExtra("product_id", notifList.get(position).actionId);
                            activity.startActivity(myIntent3, options.toBundle());
                            break;


                    }


                }
            });


        }


    }

    @Override
    public NotificationsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_notification_row, viewGroup, false);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int i) {

        String title = notifList.get(i).title;
        holder.tvLetterIcon.setText(String.valueOf(title.charAt(0)));
        holder.tvTitle.setText(title);
        holder.tvDesc.setText(notifList.get(i).description);
        int category = (notifList.get(i).category);
        switch (category) {

            case 1:
                holder.btnView.setVisibility(View.VISIBLE);
                holder.tvTransBy.setVisibility(View.VISIBLE);
                holder.tvTransBy.setText("Transferred By: " + notifList.get(i).transBy);
                break;

            case 2:
                holder.tvTransBy.setVisibility(View.INVISIBLE);
                holder.btnView.setVisibility(View.VISIBLE);
                break;
            case 3:
                holder.tvTransBy.setVisibility(View.INVISIBLE);
                holder.btnView.setVisibility(View.INVISIBLE);
                break;
            case 5:
                holder.tvTransBy.setVisibility(View.INVISIBLE);
                holder.btnView.setVisibility(View.INVISIBLE);
                break;

        }

        if (notifList.get(i).markRead == 1) {

            holder.tvTitle.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
            holder.tvDesc.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
            holder.tvTransBy.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
            holder.tvDate.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
            holder.tvLetterIcon.setBackground(activity.getResources().getDrawable(R.drawable.circle_layout_disabled));
            holder.btnClear.setVisibility(View.GONE);

        }


        holder.tvDate.setText(dc.getFormattedDate("yyyy-MM-dd", notifList.get(i).date));
        final Animation animation = AnimationUtils.loadAnimation(activity,
                R.anim.expand);
        animation.setDuration(300);

        holder.transitionsContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //  List<Notification> notif=Notification.find(Notification.class,"notification_id = ?",String.valueOf(notifList.get(i).notificationId));

                if (temp != null) {

                    expanded = !expanded;
                    temp.setMaxLines(2);
                    temp.setTextColor(activity.getResources().getColor(R.color.textGrey1));

                }
                temp = holder.tvDesc;


                holder.tvDesc.setMaxLines(5);
                temp.setTextColor(activity.getResources().getColor(R.color.flatblue));
                holder.tvDesc.startAnimation(animation);


            }
        });
        holder.btnClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                List<Notification> markRead = new ArrayList<>();
                markRead = Notification.find(Notification.class, "notification_id = ?", String.valueOf(notifList.get(i).notificationId));
                markRead.get(0).markRead = 1;
                markRead.get(0).save();
                holder.tvTitle.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
                holder.tvDesc.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
                holder.tvTransBy.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
                holder.tvDate.setTextColor(activity.getResources().getColor(R.color.textGreyLite));
                holder.tvLetterIcon.setBackground(activity.getResources().getDrawable(R.drawable.circle_layout_disabled));
                holder.btnClear.setVisibility(View.GONE);


            }
        });


        // Here you apply the animation when the view is bound

        setAnimation(holder.itemView, i);


    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    // Filter method
    public void filter(String charText) {

    }


   /* public void updateNotificationListItems(List<Notification> offenquiry) {
        final NotificationDiffCallback diffCallback = new NotificationDiffCallback(this.notifList, offenquiry);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.notifList.clear();
        this.notifList.addAll(offenquiry);
        diffResult.dispatchUpdatesTo(this);
    }
*/


    @Override
    public int getItemCount() {
        return (notifList.size());
    }


}



