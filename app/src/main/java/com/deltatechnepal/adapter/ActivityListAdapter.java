package com.deltatechnepal.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.kcbl.CollectionFollowUpHistoryActivity;
import com.deltatechnepal.kcbl.EnquiryDetailsActivity;
import com.deltatechnepal.kcbl.FollowUpHistoryActivity;
import com.deltatechnepal.kcbl.MyApplication;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class ActivityListAdapter extends RecyclerView.Adapter<ActivityListAdapter.ViewHolder> {
    private List<ActivityLog> activityList;
    private Activity activity;
    private String type;
    private ProgressDialog progressDialog;
    public ActivityListAdapter(Activity activity, List<ActivityLog> list) {
        this.activityList = list;
        this.activity = activity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTime, tvType;

        public ViewHolder(View v) {
            super(v);
            tvTime = v.findViewById(R.id.activity_time);
            tvType = v.findViewById(R.id.activity_type);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            ActivityLog aLog = activityList.get(position);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
            switch (activityList.get(position).activityType) {

                case 1:
                    Intent case1 = new Intent(activity, EnquiryDetailsActivity.class);
                    case1.putExtra("enquiry_id", Long.valueOf(aLog.activityId));
                    activity.startActivity(case1, options.toBundle());
                    break;

                case 2:
                    progressDialog = new ProgressDialog(activity);
                    progressDialog.setTitle(null); // Setting Title
                    progressDialog.setCancelable(false);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setMessage("Fetching...");
                    progressDialog.show();
                    fetchEnquiry(String.valueOf(aLog.activityId));
                    break;
                case 3:
                    List<Offenquiry> off = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", String.valueOf(aLog.activityId));
                    Intent case3 = new Intent(activity, FollowUpHistoryActivity.class);
                    case3.putExtra("enquiry_id", Long.valueOf(aLog.activityId));
                    case3.putExtra("mobile_number", off.get(0).mobileNumber);
                    activity.startActivity(case3, options.toBundle());
                    break;
                case 4:
                    Intent case4 = new Intent(activity, CollectionFollowUpHistoryActivity.class);
                    case4.putExtra("mobile_number", String.valueOf(aLog.activityId));
                    activity.startActivity(case4, options.toBundle());
                    break;
                case 5:
                    Intent case5 = new Intent(activity, EnquiryDetailsActivity.class);
                    case5.putExtra("enquiry_id", Long.valueOf(aLog.activityId));
                    activity.startActivity(case5, options.toBundle());
                    break;
                case 6:
                    List<Offenquiry> off6 = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", String.valueOf(aLog.activityId));
                    Intent case6 = new Intent(activity, FollowUpHistoryActivity.class);
                    case6.putExtra("enquiry_id", Long.valueOf(aLog.activityId));
                    case6.putExtra("mobile_number", off6.get(0).mobileNumber);
                    activity.startActivity(case6, options.toBundle());
                    break;
                case 7:
                    List<Offenquiry> off7 = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", String.valueOf(aLog.activityId));
                    Intent case7 = new Intent(activity, FollowUpHistoryActivity.class);
                    case7.putExtra("enquiry_id", Long.valueOf(aLog.activityId));
                    case7.putExtra("mobile_number", off7.get(0).mobileNumber);
                    activity.startActivity(case7, options.toBundle());
                    break;
                case 8:
                    List<Offenquiry> off8 = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", String.valueOf(aLog.activityId));
                    Intent case8 = new Intent(activity, FollowUpHistoryActivity.class);
                    case8.putExtra("enquiry_id", Long.valueOf(aLog.activityId));
                    case8.putExtra("mobile_number", off8.get(0).mobileNumber);
                    activity.startActivity(case8, options.toBundle());
                    break;



            }


        }
    }

    @Override
    public ActivityListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_activity_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ActivityListAdapter.ViewHolder holder, int position) {
        final ActivityLog log = activityList.get(position);
        holder.tvTime.setText(log.getActivityTime());
        switch (String.valueOf(log.activityType)) {
            case "1":
                type = "Added New Enquiry";
                break;
            case "2":
                type = "Transferred Enquiry";
                break;
            case "3":
                type = "Added Enquiry FollowUp";
                break;
            case "4":
                type = "Added Collection FollowUp";
                break;
            case "5":
                type = "Sent Quotation/Proforma to Client";
                break;
            case "6":
                type = "Confirmed Enquiry Win";
                break;
            case "7":
                type = "Confirmed Enquiry Loss";
                break;
            case "8":
                type = "Confirmed Enquiry Drop";
                break;


        }
        holder.tvType.setText(type);
    }

    @Override
    public int getItemCount() {
        return (null != activityList ? activityList.size() : 0);
    }

    public void fetchEnquiry(final String enquiry_id) {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_FETCH_TENQUIRY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        // looping through json and adding to movies list
                        Log.e("Results",response);

                      progressDialog.dismiss();
                        Intent case2 = new Intent(activity, EnquiryDetailsActivity.class);
                        case2.putExtra("trans_enquiry",true);
                        case2.putExtra("response", response);
                        activity.startActivity(case2);

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("enquiry_id",enquiry_id);
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


    }

}







































