package com.deltatechnepal.adapter;


import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.kcbl.MainActivity;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.model.Prolist;
import com.deltatechnepal.orm.Products;

import java.util.List;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ViewHolder> {

    private List<Prolist> prodList;
    private Context mContext;
    private List<String> selectedNames;


    public ProductsListAdapter(Context context, List<Prolist> prodList,List<String> selectedNames) {
        this.prodList = prodList;
        this.mContext = context;
        this.selectedNames=selectedNames;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_card_view, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Glide.with(mContext).load(URLs.URL_PRODUCT_IMAGES + prodList.get(i).getImageName())
                .apply(new RequestOptions()
                        .skipMemoryCache(true).placeholder(R.drawable.kcbl))
                .into(holder.ivProduct);
        holder.tvName.setText(prodList.get(i).getProductName());
        holder.tvQuantity.setText("Quantity: " + prodList.get(i).getProductQuantity());
        holder.tvPrice.setText("Price: Rs " + String.valueOf(prodList.get(i).getProductPrice()));
    }

    @Override
    public int getItemCount() {
        return prodList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProduct;
        protected TextView tvName, tvQuantity, tvPrice;
        ImageButton moreMenu;

        public ViewHolder(View view) {
            super(view);


            this.ivProduct = view.findViewById(R.id.product_image);
            this.tvName = view.findViewById(R.id.tvName);
            this.tvQuantity = view.findViewById(R.id.tvQuantity);
            this.tvPrice = view.findViewById(R.id.tvPrice);
            this.moreMenu = view.findViewById(R.id.moreMenu);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* int position = getAdapterPosition();
                    ActivityLog.i("adapterPosition",""+position);
                    JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                    JsonObject jsonObj = jsonElement.getAsJsonObject();
                    Intent intent = new Intent(mContext,BlogDetailActivity.class);
                    String blog = jsonObj.toString();
                    intent.putExtra("blog",blog);
                    mContext.startActivity(intent);*/
                }
            });


            moreMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int pos = getAdapterPosition();
                    PopupMenu popup = new PopupMenu(mContext, moreMenu);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater()
                            .inflate(R.menu.popup, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getTitle().toString()) {
                                case "Remove":
                                    prodList.remove(pos);
                                    notifyItemRemoved(pos);
                                    selectedNames.remove(pos);
                                    break;


                            }

                            return true;
                        }
                    });

                    popup.show(); //showing popup menu
                }

            });

        }

    }
}
