package com.deltatechnepal.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.OffenquiryDiffCallback;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.helper.VolleySingleton;
import com.deltatechnepal.kcbl.EnquiryDetailsActivity;
import com.deltatechnepal.kcbl.FollowUpHistoryActivity;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.Offenquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

;

public class EprAdapter extends RecyclerView.Adapter<EprAdapter.ViewHolder> {

    private List<Offenquiry> enquiryList;
    private Activity activity;
    String semi = ": ";

    private int lastPosition = -1;


    public EprAdapter(Activity activity, List<Offenquiry> enquiryList) {
        this.enquiryList = enquiryList;
        this.activity = activity;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvName, tvNumber, tvProduct, tvReg;
        public ConstraintLayout viewBackground, viewForeground;
        public Button btnFollowUp;
        public View cornerRibbon;

        public ViewHolder(View view) {
            super(view);
            viewBackground = view.findViewById(R.id.viewBackground);
            viewForeground = view.findViewById(R.id.viewForeground);
            this.tvName = view.findViewById(R.id.tvName);
            this.tvNumber = view.findViewById(R.id.tvNumber);
            this.tvProduct = view.findViewById(R.id.tvProduct);
            this.btnFollowUp = view.findViewById(R.id.btnFollowUp);
            this.cornerRibbon = view.findViewById(R.id.corner_ribbon);
            this.tvReg = view.findViewById(R.id.tvReg);


            btnFollowUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(activity, FollowUpHistoryActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
                    myIntent.putExtra("enquiry_id", enquiryList.get(getAdapterPosition()).enquiryId);
                    myIntent.putExtra("mobile_number", enquiryList.get(getAdapterPosition()).mobileNumber);
                    activity.startActivity(myIntent, options.toBundle());
                }
            });


            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            Intent myIntent = new Intent(activity, EnquiryDetailsActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
            myIntent.putExtra("enquiry_id", enquiryList.get(getAdapterPosition()).enquiryId);
            activity.startActivity(myIntent, options.toBundle());
        }

    }

    @Override
    public EprAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_epr_row, viewGroup, false);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        holder.tvName.setText(semi + enquiryList.get(i).customerName);
        holder.tvNumber.setText(semi + enquiryList.get(i).mobileNumber);
        holder.tvProduct.setText(semi + enquiryList.get(i).productName);
        holder.btnFollowUp.setAllCaps(false);

        if (enquiryList.get(i).reg == 0) {
            holder.tvReg.setText("UNREG");
            holder.tvReg.setBackgroundColor(activity.getResources().getColor(R.color.textGrey1));
        } else {
            holder.tvReg.setText("REG");
            holder.tvReg.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }

        switch (enquiryList.get(i).status) {
            case 7:
                holder.cornerRibbon.setBackgroundColor(activity.getResources().getColor(R.color.flatred));
                break;
            case 15:
                holder.cornerRibbon.setBackgroundColor(activity.getResources().getColor(R.color.flatorange));
                break;
            case 25:
                holder.cornerRibbon.setBackgroundColor(activity.getResources().getColor(R.color.flatblue));
                break;
            default:
                holder.cornerRibbon.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
                break;


        }


        // Here you apply the animation when the view is bound

        setAnimation(holder.itemView, i);


    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    public void updateOffenquiryListItems(List<Offenquiry> offenquiry) {
        final OffenquiryDiffCallback diffCallback = new OffenquiryDiffCallback(this.enquiryList, offenquiry);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.enquiryList.clear();
        this.enquiryList.addAll(offenquiry);
        diffResult.dispatchUpdatesTo(this);
    }


    @Override
    public int getItemCount() {
        return (enquiryList.size());
    }


}



