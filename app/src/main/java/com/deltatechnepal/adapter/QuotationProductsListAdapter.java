package com.deltatechnepal.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.model.Prolist;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.orm.Quotation;

import java.util.List;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class QuotationProductsListAdapter extends RecyclerView.Adapter<QuotationProductsListAdapter.ViewHolder> {
    private List<Prolist> productsList;
    private Activity activity;

    public QuotationProductsListAdapter(Activity activity, List<Prolist> list) {
        this.productsList = list;
        this.activity = activity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvQuantity, tvRate;


        public ViewHolder(View v) {
            super(v);
            tvProductName = v.findViewById(R.id.tvProductName);
            tvQuantity = v.findViewById(R.id.tvQuantityValue);
            tvRate = v.findViewById(R.id.tvRateValue);


        }
    }

    @Override
    public QuotationProductsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_quotation_products_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuotationProductsListAdapter.ViewHolder holder, int position) {
        final Prolist pro = productsList.get(position);

        holder.tvProductName.setText(pro.getProductName());
        holder.tvQuantity.setText(String.valueOf(pro.getProductQuantity()));
        holder.tvRate.setText(String.valueOf(pro.getProductPrice()));




    }

    @Override
    public int getItemCount() {
        return (null != productsList ? productsList.size() : 0);
    }


}







































