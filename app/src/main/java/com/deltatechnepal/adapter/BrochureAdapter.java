package com.deltatechnepal.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.kcbl.R;

import java.util.List;

public class BrochureAdapter extends PagerAdapter {
    Context context;
    private List<String> brochure;

    public BrochureAdapter(Context context, List<String> brochure) {
        this.context = context;
        this.brochure = brochure;
    }

    @Override
    public int getCount() {
        return brochure.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        Glide.with(context).load(URLs.URL_PRODUCT_BROCHURES + brochure.get(position))
                .apply(new RequestOptions()
                        .skipMemoryCache(true).placeholder(R.drawable.kcbl))
                .into(imageView);

        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}