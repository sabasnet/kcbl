package com.deltatechnepal.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.kcbl.AddFollowUpCollectionsActivity;
import com.deltatechnepal.kcbl.CollectionFollowUpHistoryActivity;
import com.deltatechnepal.kcbl.FollowUpHistoryActivity;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.Collection;

import java.util.List;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class CollectionListAdapter extends RecyclerView.Adapter<CollectionListAdapter.ViewHolder> {
    private List<Collection> collectionList;
    private Activity activity;

    public CollectionListAdapter(Activity activity, List<Collection> list) {
        this.collectionList = list;
        this.activity = activity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCName, tvPAmount, tvNextFollowUp,tvCollectionAgeing,tvUploadedOn;
        public Button btnAddFollwUp;

        public ViewHolder(View v) {
            super(v);
            tvCName = v.findViewById(R.id.tvCustomerNameValue);
            tvPAmount = v.findViewById(R.id.tvPendingAmountValue);
            tvUploadedOn = v.findViewById(R.id.tvUploadedOnValue);
            tvNextFollowUp = v.findViewById(R.id.tvNextFollowUpValue);
            btnAddFollwUp = v.findViewById(R.id.btnFollowUp);
            btnAddFollwUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent myIntent = new Intent(activity, CollectionFollowUpHistoryActivity.class);
                    myIntent.putExtra("mobile_number",collectionList.get(position).customerMobile);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_right, R.anim.slide_left);
                    activity.startActivity(myIntent, options.toBundle());
                }
            });

           /* v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showAgeingDialog(getAdapterPosition());
                }
            });*/

        }
    }

    @Override
    public CollectionListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_collection, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CollectionListAdapter.ViewHolder holder, int position) {
        final Collection collection = collectionList.get(position);
        holder.tvCName.setText(collection.customerName);
        holder.tvPAmount.setText(collection.pendingAmount);
        holder.tvNextFollowUp.setText(collection.getNextFollowupDate());
        holder.tvUploadedOn.setText(collection.uploadedOn);
    }

    @Override
    public int getItemCount() {
        return (null != collectionList ? collectionList.size() : 0);
    }

    public void setItems(List<Collection> collections) {
        this.collectionList = collections;
    }


    public void showAgeingDialog(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.collection_ageing_detail, null);
        dialogBuilder.setView(dialogView);

        TextView tvPendingBill = dialogView.findViewById(R.id.tvPendingBillValue);
        TextView tvLessThan30 = dialogView.findViewById(R.id.tvLT30DaysValue);
        TextView tvBetween30to60 = dialogView.findViewById(R.id.tvB30To60DaysValue);
        TextView tvBetween60to90 = dialogView.findViewById(R.id.tvB60To90DaysValue);
        TextView tvGreaterThan90 = dialogView.findViewById(R.id.tvGT90DaysValue);
        TextView tvOnAccount = dialogView.findViewById(R.id.tvOnAccountValue);


        Collection col =collectionList.get(position);

        String semiRs=": Rs.";

        tvPendingBill.setText(semiRs+col.pendingAmount);
        tvLessThan30.setText(TextUtils.isEmpty(col.lessThanThirty)?": N/A":semiRs+col.lessThanThirty );
        tvBetween30to60.setText(TextUtils.isEmpty(col.betweenThirtySixty)?": N/A":semiRs+col.betweenThirtySixty);
        tvBetween60to90.setText(TextUtils.isEmpty(col.betweenSixtyNinety)?": N/A":semiRs+col.betweenSixtyNinety);
        tvGreaterThan90.setText(TextUtils.isEmpty(col.greaterThanNinety)?": N/A":semiRs+col.greaterThanNinety);
        tvOnAccount.setText(TextUtils.isEmpty(col.onAccount)?": N/A":semiRs+col.onAccount);








        dialogBuilder.setTitle("Collection Ageing: "+col.customerName);

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {



            }
        });

        AlertDialog b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white_background);
        b.show();


    }
}







































