package com.deltatechnepal.kcbl;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.deltatechnepal.adapter.CollectionListAdapter;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.Offenquiry;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class CollectionFollowUp extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvCollection;
    private CollectionListAdapter adapteroff;
    private List<Collection> coll;
    DateConversion dc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvCollection = findViewById(R.id.rvEnquiry);
        linearLayoutManager =
                new LinearLayoutManager(CollectionFollowUp.this);
        rvCollection.setLayoutManager(linearLayoutManager);
        rvCollection.setHasFixedSize(true);
        rvCollection.setNestedScrollingEnabled(true);
        dc = new DateConversion();
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        if (Collection.count(Collection.class) > 0 && ConnectionChecker.getInstance(mContext).Check())
            fetchlocalCollections();


    }


    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        fetchlocalCollections();
        swipeRefreshLayout.setRefreshing(false);


    }


    private void fetchlocalCollections() {

        coll = Select.from(Collection.class)
                .where(Condition.prop("next_followup_date").lt(dc.getTomorrowDateinMilli()),Condition.prop("next_followup_date").notEq(0))
                .orderBy("next_followup_date DESC")
                .list();
        adapteroff = new CollectionListAdapter(CollectionFollowUp.this, coll);
        rvCollection.setAdapter(adapteroff);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}




