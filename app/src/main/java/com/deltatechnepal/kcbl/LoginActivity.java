package com.deltatechnepal.kcbl;


import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Token;
import com.deltatechnepal.orm.User;
import com.deltatechnepal.orm.Subsource;
import com.deltatechnepal.service.MyFirebaseInstanceIDService;
import com.orm.SugarTransactionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    private EditText Email, Password;
    ProgressDialog progressDialog;
    String formattedDate, name, number, designation;
    String firebase_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("KCBL");
        Email = (EditText) findViewById(R.id.email);
        Password = (EditText) findViewById(R.id.password);


        //make translucent statusBar on kitkat devices
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        if (SharedPreManager.getInstance(this).isLoggedIn()) {
            finish();
            Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
            startActivity(myIntent, options.toBundle());

        }


    }


    public void Submit(View v) {
        if (Counter.count(Counter.class) == 0) {
            Counter ecount = new Counter(0, 0, 0);
            ecount.save();
        }
        if (Token.count(Token.class) == 0) {
            //FirebaseInstanceId.getInstance().getToken();
            MyFirebaseInstanceIDService fbase = new MyFirebaseInstanceIDService();
            fbase.onTokenRefresh();
        }


        if (!Email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            Email.setError("Invalid Email");
            Email.requestFocus();
        } else {
            progressDialog = new ProgressDialog(LoginActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setMessage("Signing In..."); // Setting Message
            progressDialog.setTitle(null); // Setting Title
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);
            userLogin();
        }


    }

    private void userLogin() {
        //first getting the values
        Token token = Token.findById(Token.class, 1);
        firebase_token = token.ftoken;
        final String email = Email.getText().toString();
        final String password = Password.getText().toString();
        formattedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        System.out.println("Formatted time => " + formattedDate);



       /* //validating inputs
        if (TextUtils.isEmpty(email)) {
            Email.setError("Please enter your Email");
            Password.requestFocus();
            return;
        }

        else if (TextUtils.isEmpty(password)) {
            Password.setError("Please enter your password");
            Password.requestFocus();
            return;
        }*/

        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            //if no error in response
                            if (obj.getBoolean("status")) {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                                //getting the user from the response


                                JSONObject userJson = obj.getJSONObject("data");


                                //creating a new user object
                                User user = new User(
                                        userJson.getInt("id"),
                                        userJson.getString("name"),
                                        userJson.getString("email"),
                                        userJson.getString("image"),
                                        userJson.getString("designation"),
                                        userJson.getString("mobile_no")
                                );

                                if (!((obj.getString("counters")).matches("0"))) {
                                    JSONObject counters = obj.getJSONObject("counters");
                                    int assigned = counters.getInt("enq_assigned");
                                    int pending = counters.getInt("enq_pending");
                                    int completed = counters.getInt("enq_completed");

                                    Counter ecount = Counter.findById(Counter.class, 1);
                                    ecount.assigned = assigned;
                                    ecount.pending = pending;
                                    ecount.completed = completed;
                                    ecount.save();
                                }

                                //storing the user in shared preferences
                                SharedPreManager.getInstance(getApplicationContext()).userLogin(user);


                                //Creating SubSource Table
                                createSubsourceTable();


                                //starting the profile activity

                                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                                ActivityOptions options =
                                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
                                startActivity(myIntent, options.toBundle());

                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("current_date", formattedDate);
                params.put("firebase_token", firebase_token);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(stringRequest);
    }


    public void createSubsourceTable() {


        final List<String> list = new ArrayList<>();

        list.add("Newspaper");
        list.add("TVC");
        list.add("Pole Banner");
        list.add("Existing Customer");
        list.add("Hoarding");
        list.add("Internet");
        list.add("FM");
        list.add("Leaflet");
        list.add("Mikeing");
        list.add("Road Show");
        list.add("Website");
        list.add("Digital Marketing");
        list.add("Others");
        list.add("Test Drive/Demo");
        list.add("Exchange Mela");

        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
            @Override
            public void manipulateInTransaction() {
                for (int i = 0; i < list.size(); i++) {
                    Subsource subList = new Subsource(list.get(i));
                    subList.save();
                }
            }
        });


    }


    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }

}























