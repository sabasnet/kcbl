package com.deltatechnepal.kcbl;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.adapter.NotificationsAdapter;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.service.EnquirySyncJob;
import com.deltatechnepal.service.NotificationSyncJob;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Context mContext;

    private String TAG = NotificationActivity.class.getSimpleName();
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvNotifications;
    private NotificationsAdapter adapter;
    private List<Notification> notif;
    LinearLayout placeholder;
    Animation anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_notification_center);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        placeholder = findViewById(R.id.placeholder);
        rvNotifications = findViewById(R.id.rvNotifications);
        linearLayoutManager =
                new LinearLayoutManager(NotificationActivity.this);
        rvNotifications.setLayoutManager(linearLayoutManager);
        rvNotifications.setHasFixedSize(true);
        rvNotifications.setNestedScrollingEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);


        swipeRefreshLayout.setOnRefreshListener(this);


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(800); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);


        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (ConnectionChecker.getInstance(mContext).Check()
                                                && SharedPreManager.getInstance(mContext).checkForNotifications()) {
                                            placeholder.setVisibility(View.VISIBLE);
                                            placeholder.startAnimation(anim);
                                            fetchNotificationsOnline();
                                        } else {
                                            placeholder.setVisibility(View.VISIBLE);
                                            placeholder.startAnimation(anim);
                                            fetchNotificationsOffline();
                                        }
                                    }
                                }
        );
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        fetchNotificationsOffline();
        swipeRefreshLayout.setRefreshing(false);


    }


    private void fetchNotificationsOffline() {
        notif = Select.from(Notification.class)
                .orderBy("unix_time_stamp DESC")
                .list();

        adapter = new NotificationsAdapter(NotificationActivity.this, notif);
        rvNotifications.setAdapter(adapter);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);
    }

    public void fetchNotificationsOnline() {
        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_FETCH_NOTIFICATIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // looping through json and adding to movies list

                        try {

                            JSONObject obj = new JSONObject(response);

                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            placeholder.clearAnimation();
                            placeholder.setVisibility(View.GONE);
                            if (obj.getBoolean("status")) {
                                Notification.deleteAll(Notification.class);
                                JSONArray notificationArray = obj.getJSONArray("data");
                                for (int i = 0; i < notificationArray.length(); i++) {

                                    JSONObject notifications = notificationArray.getJSONObject(i);
                                    long notifID = notifications.getInt("id");
                                    long actionID = notifications.getInt("action_id");
                                    String title = notifications.getString("title");
                                    String description = notifications.getString("description");
                                    int category = notifications.getInt("notification_category");
                                    String transBy = notifications.getString("trans_from");
                                    String date = notifications.getString("date");
                                    long unixTime = notifications.getLong("unix_time");


                                    if (!Notification.getNotificationById(notifID)) {
                                        Notification notif = new Notification(notifID, actionID, title, description, category, date, 0, transBy, unixTime);
                                        notif.save();
                                    }


                                }
                                fetchNotificationsOffline();

                                SharedPreManager.getInstance(mContext).setCheckNotification(false);

                            } else {


                                SharedPreManager.getInstance(mContext).setCheckNotification(false);
                                fetchNotificationsOffline();
                            }


                            // updating offset value to highest value
                                       /* if (rank >= offSet)
                                            offSet = rank;*/

                        } catch (JSONException e) {
                            Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                int id = SharedPreManager.getInstance(mContext).getUser().getId();
                params.put("id", String.valueOf(id));
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void onBackPressed() {
        NotificationSyncJob.runJobImmediately();
        finish();
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


}

