package com.deltatechnepal.kcbl;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.CustomToast;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.ResyncEnquiries;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.helper.VolleySingleton;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddFollowUpActivity extends AppCompatActivity {
    Context mContext;
    RecyclerView rvCollection;
    Spinner spinnerReasons;
    TextInputLayout etRemarkHolder;
    EditText etRemark;
    TextView tvNewLeadStatus, tvNextAction, tvNextFollowUpDate, tvReason, tvLastRemark, tvLastRemarkValue;
    LinearLayout layoutNewLeadStatus, layoutNextAction;
    ConstraintLayout layoutReasons;
    Button btnFollowUpDate, btnReceived,btnNotReceived, btnNotReachable,
            btnActive, btnWin, btnLoss, btnDrop1, btnVisit, btnCall, btnDrop2, temp1, temp2, temp3, btnSubmit;
    int mode = 0;
    DateConversion dc;
    CustomToast customToast;
    String eId, remark = "", lastRemark;
    int lastMode;
    ProgressDialog progressDialog;
    private String TAG = AddFollowUpActivity.class.getSimpleName();
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_ID = "keyid";
    List<Offenquiry> off;
    List<Products> pro;
    boolean first_followup;
    long ndate = 0;
    int result = 0, followUpStatus, followUpMode = 0, reason = 0,activity_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_add_followup);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        etRemarkHolder = findViewById(R.id.etHolder);
        etRemark = findViewById(R.id.followup_remark);
        btnFollowUpDate = findViewById(R.id.next_follow_up);
        tvNewLeadStatus = findViewById(R.id.tvNewLeadStatus);
        tvNextAction = findViewById(R.id.tvNextAction);
        tvReason = findViewById(R.id.tvResult);
        tvNextFollowUpDate = findViewById(R.id.tvNfd);
        tvLastRemark = findViewById(R.id.tvLastFollowUpRemark);
        tvLastRemarkValue = findViewById(R.id.tvLastFollowUpRemarkValue);

        layoutNewLeadStatus = findViewById(R.id.newLeadStatus);
        layoutNextAction = findViewById(R.id.nextAction);
        layoutReasons = findViewById(R.id.spinnerHolder);


        btnReceived = findViewById(R.id.received);
        btnNotReceived = findViewById(R.id.notreceived);
        btnNotReachable = findViewById(R.id.notreachable);
        btnActive = findViewById(R.id.active);
        btnWin = findViewById(R.id.win);
        btnLoss = findViewById(R.id.loss);
        btnDrop1 = findViewById(R.id.drop1);
        btnVisit = findViewById(R.id.visit);
        btnCall = findViewById(R.id.call);
        btnDrop2 = findViewById(R.id.drop2);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setEnabled(false);

        //Creating Custom Date and Toast object
        dc = new DateConversion();
        customToast = new CustomToast();


        //spinner
        spinnerReasons = findViewById(R.id.reasons);


        //set Current date
        btnFollowUpDate.setText(dc.getCurrentDate());

        //receiving data passed from previous activity
        Intent mIntent = getIntent();
        eId = mIntent.getStringExtra("enquiry_id");
        lastRemark = mIntent.getStringExtra("last_followup_note");
        lastMode=mIntent.getIntExtra("previous_next_followup_mode",0);
        if (!TextUtils.isEmpty(lastRemark))

        {
            tvLastRemarkValue.setText(lastRemark);
        } else {
            tvLastRemark.setVisibility(View.GONE);
            tvLastRemarkValue.setVisibility(View.GONE);
        }

        if(lastMode==1){
            btnNotReceived.setVisibility(View.GONE);
            btnReceived.setText("Met");
            btnNotReachable.setText("Not Met");
            }



        off = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);
        if (off == null) {
            Toast.makeText(mContext, "The Enquiry Does Not Exist or Is Deleted", Toast.LENGTH_LONG).show();
            return;
        }
        pro = Products.find(Products.class, "product_name = ?", off.get(0).productName);
        if (pro == null) {
            Toast.makeText(mContext, "The Product Does Not Exist or Is Deleted", Toast.LENGTH_LONG).show();
            return;
        }


        etRemark.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etRemark.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard(AddFollowUpActivity.this);
                    return true;
                }
                return false;
            }
        });


    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void setSpinner(int result) {
        tvReason.setVisibility(View.VISIBLE);
        layoutReasons.setVisibility(View.VISIBLE);
        tvNextFollowUpDate.setVisibility(View.GONE);
        btnFollowUpDate.setVisibility(View.GONE);
        etRemarkHolder.setVisibility(View.GONE);


        List<String> reasonsList = new ArrayList<>();
        switch (result) {

            case 1:
                tvReason.setText("Confirm Win Reason");
                reasonsList.add("Happy with Price/Discount");
                reasonsList.add("Convinced with brand");
                reasonsList.add("Customer was happy with dealing");
                reasonsList.add("Friend/family recommended");
                reasonsList.add("Convinced with the product");
                reasonsList.add("Others");
                break;
            case 2:
                tvReason.setText("Confirm Reason For Loss");
                reasonsList.add("Issue with product");
                reasonsList.add("Not happy with Price/Discount");
                reasonsList.add("Not convinced with brand");
                reasonsList.add("Got better finance facility");
                reasonsList.add("Got credit facility");
                reasonsList.add("Customer was not happy with dealing");
                reasonsList.add("Friend/family did not recommend");
                reasonsList.add("Desired model not available");
                reasonsList.add("Desired color not available");
                reasonsList.add("Others");
                break;
            case 3:
                tvReason.setText("Confirm Reason For Drop");
                reasonsList.add("Wrong Number");
                reasonsList.add("Customer postponed indefinitely");
                reasonsList.add("Poor customer response");
                reasonsList.add("Already Delivered");
                reasonsList.add("Duplicate entry");
                reasonsList.add("Unable to speak to customer");
                reasonsList.add("Purchased second hand");
                reasonsList.add("Others");
                break;


        }


        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                mContext, R.layout.reasons_list_spinner_item, reasonsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.reasons_list_spinner_item);
        spinnerReasons.setAdapter(spinnerArrayAdapter);


    }


    public void statusClicked(View v) {
        Button button = (Button) v;
        result = 0;
        followUpMode = 0;
        btnSubmit.setEnabled(false);
        btnSubmit.setBackground(getResources().getDrawable(R.drawable.rounded_disabled_grey_background));
        btnFollowUpDate.setVisibility(View.GONE);
        // clear state
        if (temp1 != null) {
            temp1.setSelected(false);
            temp1.setPressed(false);
            temp1.setTextColor(getResources().getColor(R.color.textGrey1));
        }
        // clear state
        if (temp2 != null) {
            temp2.setSelected(false);
            temp2.setPressed(false);
            temp2.setTextColor(getResources().getColor(R.color.textGrey1));
        }
        // clear state
        if (temp3 != null) {
            temp3.setSelected(false);
            temp3.setPressed(false);
            temp3.setTextColor(getResources().getColor(R.color.textGrey1));
        }


        // change state
        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(getResources().getColor(R.color.white));
        temp1 = button;
        switch (v.getId()) {

            case (R.id.received):
                if(btnReceived.getText().toString().matches("Met"))
                    followUpStatus=4;
                else
                followUpStatus = 1;
                tvNextAction.setVisibility(View.GONE);
                layoutNextAction.setVisibility(View.GONE);
                tvNewLeadStatus.setVisibility(View.VISIBLE);
                layoutNewLeadStatus.setVisibility(View.VISIBLE);
                tvReason.setVisibility(View.GONE);
                layoutReasons.setVisibility(View.GONE);
                etRemarkHolder.setVisibility(View.VISIBLE);
                break;

            case (R.id.notreachable):
                if(btnReceived.getText().toString().matches("Met"))
                    followUpStatus=5;
                else
                followUpStatus = 2;
                btnDrop2.setVisibility(View.VISIBLE);
                tvNewLeadStatus.setVisibility(View.GONE);
                layoutNewLeadStatus.setVisibility(View.GONE);
                tvNextAction.setVisibility(View.VISIBLE);
                layoutNextAction.setVisibility(View.VISIBLE);
                etRemarkHolder.setVisibility(View.VISIBLE);
                tvReason.setVisibility(View.GONE);
                layoutReasons.setVisibility(View.GONE);
                break;
            case (R.id.notreceived):
                followUpStatus = 3;
                btnDrop2.setVisibility(View.VISIBLE);
                tvNewLeadStatus.setVisibility(View.GONE);
                layoutNewLeadStatus.setVisibility(View.GONE);
                tvNextAction.setVisibility(View.VISIBLE);
                layoutNextAction.setVisibility(View.VISIBLE);
                etRemarkHolder.setVisibility(View.VISIBLE);
                tvReason.setVisibility(View.GONE);
                layoutReasons.setVisibility(View.GONE);
                break;


        }


    }


    public void newLeadStatusClicked(View v) {
        Button button = (Button) v;
        followUpMode = 0;
        // clear state
        if (temp2 != null) {
            temp2.setSelected(false);
            temp2.setPressed(false);
            temp2.setTextColor(getResources().getColor(R.color.textGrey1));
        }


        // change state
        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(getResources().getColor(R.color.white));
        temp2 = button;
        switch (v.getId()) {

            case (R.id.active):
                btnSubmit.setEnabled(false);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.rounded_disabled_grey_background));
                result = 0;
                btnDrop2.setVisibility(View.GONE);
                tvNextAction.setVisibility(View.VISIBLE);
                layoutNextAction.setVisibility(View.VISIBLE);
                tvReason.setVisibility(View.GONE);
                layoutReasons.setVisibility(View.GONE);
                tvNextFollowUpDate.setVisibility(View.VISIBLE);
                btnFollowUpDate.setVisibility(View.VISIBLE);
                break;

            case (R.id.win):
                btnSubmit.setEnabled(true);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.button_effect));
                result = 1;
                tvNextFollowUpDate.setVisibility(View.GONE);
                btnFollowUpDate.setVisibility(View.GONE);
                // clear state
                if (temp3 != null) {
                    temp3.setSelected(false);
                    temp3.setPressed(false);
                    temp3.setTextColor(getResources().getColor(R.color.textGrey1));
                }
                tvNextAction.setVisibility(View.GONE);
                layoutNextAction.setVisibility(View.GONE);
                setSpinner(1);
                break;

            case (R.id.loss):
                btnSubmit.setEnabled(true);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.button_effect));
                result = 2;
                tvNextFollowUpDate.setVisibility(View.GONE);
                btnFollowUpDate.setVisibility(View.GONE);
                // clear state
                if (temp3 != null) {
                    temp3.setSelected(false);
                    temp3.setPressed(false);
                    temp3.setTextColor(getResources().getColor(R.color.textGrey1));
                }
                tvNextAction.setVisibility(View.GONE);
                layoutNextAction.setVisibility(View.GONE);
                setSpinner(2);
                break;

            case (R.id.drop1):
                btnSubmit.setEnabled(true);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.button_effect));
                result = 3;
                tvNextFollowUpDate.setVisibility(View.GONE);
                btnFollowUpDate.setVisibility(View.GONE);
                // clear state
                if (temp3 != null) {
                    temp3.setSelected(false);
                    temp3.setPressed(false);
                    temp3.setTextColor(getResources().getColor(R.color.textGrey1));
                }
                tvNextAction.setVisibility(View.GONE);
                layoutNextAction.setVisibility(View.GONE);
                setSpinner(3);
                break;


        }


    }

    public void nextActionClicked(View v) {
        Button button = (Button) v;

        // clear state
        if (temp3 != null) {
            temp3.setSelected(false);
            temp3.setPressed(false);
            temp3.setTextColor(getResources().getColor(R.color.textGrey1));
        }


        // change state
        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(getResources().getColor(R.color.white));
        temp3 = button;
        switch (v.getId()) {

            case (R.id.drop2):
                btnSubmit.setEnabled(true);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.button_effect));
                result = 3;
                followUpMode = 0;
                tvNextFollowUpDate.setVisibility(View.GONE);
                btnFollowUpDate.setVisibility(View.GONE);
                /*tvNextAction.setVisibility(View.GONE);
                layoutNextAction.setVisibility(View.GONE);*/
                setSpinner(3);
                break;

            case (R.id.call):
                btnSubmit.setEnabled(true);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.button_effect));
                result = 0;
                followUpMode = 2;
                tvNextFollowUpDate.setVisibility(View.VISIBLE);
                btnFollowUpDate.setVisibility(View.VISIBLE);
                tvReason.setVisibility(View.GONE);
                layoutReasons.setVisibility(View.GONE);
                etRemarkHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.visit):
                btnSubmit.setEnabled(true);
                btnSubmit.setBackground(getResources().getDrawable(R.drawable.button_effect));
                result = 0;
                followUpMode = 1;
                tvNextFollowUpDate.setVisibility(View.VISIBLE);
                btnFollowUpDate.setVisibility(View.VISIBLE);
                tvReason.setVisibility(View.GONE);
                layoutReasons.setVisibility(View.GONE);
                etRemarkHolder.setVisibility(View.VISIBLE);
                break;
        }


    }

    public void openCalendarDialog(View v) {

        // To show current date in the datepicker
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(
                mContext, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);
                calendar.set(Calendar.HOUR_OF_DAY, 10);
                calendar.set(Calendar.MINUTE, 45);
                calendar.set(Calendar.SECOND, 0);

                ndate = calendar.getTimeInMillis();

                btnFollowUpDate.setText(dc.getFormattedDate("MM/dd/yyyy", ndate));


                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", calendar.getTimeInMillis());
                intent.putExtra("allDay", true);
                intent.putExtra("endTime", calendar.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra("title", " FollowUp for " + off.get(0).contactPerson);
                mContext.startActivity(intent);

            }
        }, mYear, mMonth, mDay);

        mDatePicker.setTitle("Select FollowUp Date");
        Calendar cal = Calendar.getInstance();


        switch (off.get(0).status) {
            case 7:
                cal.add(Calendar.DAY_OF_MONTH, 3);
                mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
                break;
            case 15:
                cal.add(Calendar.DAY_OF_MONTH, 7);
                mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
                break;
            case 25:
                cal.add(Calendar.DAY_OF_MONTH, 15);
                mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
                break;

        }


        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }


    public void SubmitFollowUp(View v) {


        if (ConnectionChecker.getInstance(mContext).Check()) {

            if (result != 0) {
                reason = spinnerReasons.getSelectedItemPosition();
                switch (result) {
                    case 1:
                        reason = reason + 1;
                        remark = "Win";
                        break;
                    case 2:
                        reason = reason + 7;
                        remark = "Loss";
                        break;
                    case 3:
                        reason = reason + 17;
                        remark = "Drop";
                        break;


                }
                progressDialog = new ProgressDialog(AddFollowUpActivity.this, R.style.MyAlertDialogStyle);
                progressDialog.setMessage("Updating data.Please Wait...");
                progressDialog.setCancelable(false);
                progressDialog.setTitle(null);
                progressDialog.show();
                updateResult();


            }
            if (followUpMode != 0) {

                if (!TextUtils.isEmpty(etRemark.getText().toString())) {
                    progressDialog = new ProgressDialog(AddFollowUpActivity.this, R.style.MyAlertDialogStyle);
                    progressDialog.setMessage("Updating data.Please Wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.setTitle(null);
                    progressDialog.show();
                    addFollowUp();
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(mContext, "Please enter remark to submit", Toast.LENGTH_SHORT).show();
                }

            }


        } else {
            Toast.makeText(mContext, "You need Internet Connection For Adding this", Toast.LENGTH_SHORT).show();

        }


    }

    public void updateResult() {

        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_UPDATE_RESULT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getBoolean("status")) {


                                EnquiryFollowUps efollowup = new EnquiryFollowUps(eId,
                                        String.valueOf(pro.get(0).productId), followUpMode,
                                        remark, dc.getCurrentDateinMilliseonds("MM/dd/yyyy H:m:s"), 0,
                                        followUpStatus);
                                efollowup.save();

                                List<Offenquiry> off = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);
                                off.get(0).enqResult = result;
                                off.get(0).reason = reason;
                                off.get(0).save();

                                if(result==1)
                                    activity_type=6;
                                else if(result==2)
                                    activity_type=7;
                                else
                                    activity_type=8;

                                ActivityLog log = new ActivityLog(activity_type, obj.getLong("time"), Integer.valueOf(eId));
                                log.save();
                                AlertDialog dialog = new AlertDialog.Builder(AddFollowUpActivity.this)
                                        .setTitle("Added")
                                        .setIcon(R.drawable.ic_info)
                                        .setMessage("The FollowUp has been successfully added")
                                        .setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        finish();
                                                    }
                                                }).create();
                                dialog.show();

                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            progressDialog.dismiss();

                        }


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("enquiry_id", eId);
                params.put("action", String.valueOf(result));
                params.put("reason", String.valueOf(reason));
                params.put("product_id", String.valueOf(pro.get(0).productId));
                params.put("followup_status", String.valueOf(followUpStatus));
                params.put("remark", remark);
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        VolleySingleton.getInstance(mContext).addToRequestQueue(req);


    }


    //Function for sending added inquiry to server in online mode

    public void addFollowUp() {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_ADD_FOLLOWUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getBoolean("status")) {
                                EnquiryFollowUps efollowup = new EnquiryFollowUps(eId,
                                        String.valueOf(pro.get(0).productId), followUpMode,
                                        etRemark.getText().toString(), dc.getCurrentDateinMilliseonds("MM/dd/yyyy H:m:s"), ndate,
                                        followUpStatus);
                                efollowup.save();

                                List<Offenquiry> off = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);

                                off.get(0).nextFollowUpDate = dc.getMilli(btnFollowUpDate.getText().toString());
                                off.get(0).save();

                                ActivityLog log = new ActivityLog(3, obj.getLong("time"), Integer.valueOf(eId));
                                log.save();

                                AlertDialog dialog = new AlertDialog.Builder(AddFollowUpActivity.this)
                                        .setTitle("Added")
                                        .setIcon(R.drawable.ic_info)
                                        .setMessage("The FollowUp has been successfully added")
                                        .setPositiveButton("Continue",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        finish();
                                                    }


                                                }).create();
                                dialog.show();


                            } else {

                                AlertDialog dialog = new AlertDialog.Builder(AddFollowUpActivity.this)
                                        .setTitle("Error")
                                        .setIcon(R.drawable.ic_info)
                                        .setMessage("Oops an error occured!PLease retry after sometime")
                                        .setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        finish();
                                                    }


                                                }).create();
                                dialog.show();
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                        }
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Log.e(TAG, "Server Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //This indicates that the reuest has either time out or there is no connection
                    Toast.makeText(getApplicationContext(), "Timeout Error ", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    //Error indicating that there was an Authentication Failure while performing the request
                    Toast.makeText(getApplicationContext(), "Authentication Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    //Indicates that the server responded with a error response
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    //Indicates that there was network error while performing the request
                    Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    // Indicates that the server response could not be parsed
                    Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_LONG).show();
                }

                ResyncEnquiries.getInstance(mContext).fetchserverEnquiries();
                progressDialog.dismiss();
                finish();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sharedPreferences = getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                String id = String.valueOf(sharedPreferences.getInt(KEY_ID, 0));
                params.put("product_id", String.valueOf(pro.get(0).productId));
                params.put("next_followup_mode", String.valueOf(followUpMode));
                params.put("followup_status", String.valueOf(followUpStatus));
                params.put("enquiry_id", eId);
                params.put("remark", etRemark.getText().toString());
                params.put("next_followup_date", btnFollowUpDate.getText().toString());
                params.put("salesman_id", id);
                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
