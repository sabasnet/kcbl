package com.deltatechnepal.kcbl;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.deltatechnepal.adapter.ActivityListAdapter;
import com.deltatechnepal.adapter.CollectionListAdapter;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.CollectionFollowUps;
import com.deltatechnepal.orm.Offenquiry;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class LogActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvActivity;
    private ActivityListAdapter adapteroff;
    private List<ActivityLog> logList;
    DateConversion dc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvActivity = findViewById(R.id.rvEnquiry);
        linearLayoutManager =
                new LinearLayoutManager(LogActivity.this);
        rvActivity.setLayoutManager(linearLayoutManager);
        rvActivity.setHasFixedSize(true);
        rvActivity.setNestedScrollingEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        fetchlocalLog();


    }




    private void fetchlocalLog() {

        logList = Select.from(ActivityLog.class)
                .orderBy("activity_time DESC")
                .list();
        adapteroff = new ActivityListAdapter(LogActivity.this, logList);
        rvActivity.setAdapter(adapteroff);


    }

    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        fetchlocalLog();
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}




