package com.deltatechnepal.kcbl;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.adapter.ProductsListAdapter;
import com.deltatechnepal.helper.CustomToast;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.model.Prolist;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuotationFormActivity extends AppCompatActivity {
    Context mContext;
    Spinner spType, spCompany, spBank, spCat, spPro, spdType, spdTime, spTerms;
    TextView tvSelectBank,tvCCEmails;
    String[] types, companies, banks, tbanks, dtype, dtime, terms;
    AlertDialog b;
    EditText etQuantity, etPrice,etCCMails;
    long productID;
    List<Offenquiry> enq;
    List<Products> productsList;
    List<Category> categoryList;
    List<Prolist> selectedList = new ArrayList<>();
    List<Prolist> tempSelectedList = new ArrayList<>();
    CheckBox cbSendBro;
    EditText etVat;
    String prodName, imageName;
    ProgressDialog progressDialog;
    String eId, account_no, product_id, rate;
    List<String> selectedNames = new ArrayList<>();
    List<String> tempselectedNames = new ArrayList<>();
    long total = 0;
    List<String> ccMails=new ArrayList<>();
    ProductsListAdapter adapter;
    String listCCEmails="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.quotation_performa_form);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle(null); // Setting Title
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Sending....");


        spType = findViewById(R.id.document_type);
        spCompany = findViewById(R.id.company_name);
        spBank = findViewById(R.id.bank_name);
        spdType = findViewById(R.id.delivery_type);
        spdTime = findViewById(R.id.delivery_time);
        spTerms = findViewById(R.id.payment_terms);
        tvSelectBank = findViewById(R.id.tvBankName);
        etVat = findViewById(R.id.vat_pan);
        cbSendBro = findViewById(R.id.cbSendBrochure);
        tvCCEmails=findViewById(R.id.tvCCMailsValue);




        Intent mIntent = getIntent();
        eId = mIntent.getStringExtra("enquiry_id");
        product_id = mIntent.getStringExtra("product_id");
        rate = mIntent.getStringExtra("rate");

        if (!TextUtils.isEmpty(product_id)) {
            List<Products> product = Products.find(Products.class, "product_id = ?", product_id);
            productID = product.get(0).productId;
            prodName = product.get(0).productName;
            imageName = product.get(0).imageName;

            Prolist pro = new Prolist(productID, imageName, prodName, 1, Long.valueOf(rate));

            selectedList.add(pro);
            selectedNames.add(prodName);
        }


        RecyclerView rvSelectedList = findViewById(R.id.rvSelectedProducts);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(QuotationFormActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvSelectedList.setLayoutManager(linearLayoutManager);
        rvSelectedList.setNestedScrollingEnabled(true);
        rvSelectedList.setHasFixedSize(true);
        adapter = new ProductsListAdapter(QuotationFormActivity.this, selectedList,selectedNames);
        rvSelectedList.setAdapter(adapter);


        setUpFromFields();

        etCCMails=findViewById(R.id.etCCMail);

        etCCMails.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String mail=etCCMails.getText().toString();
                    boolean isValidEmail = Patterns.EMAIL_ADDRESS.matcher(mail).matches();
                    if(ccMails.contains(mail)){
                        etCCMails.setError("Already Added");
                    }
                    else if(isValidEmail){
                        ccMails.add(mail);
                        if(ccMails.size()!=0)
                            listCCEmails=listCCEmails+"\n"+mail;
                            else
                            listCCEmails=mail;

                            tvCCEmails.setText(listCCEmails);
                            etCCMails.setText("");



                    }
                    else{
                        etCCMails.setError("Invalid Email");
                    }


                }
                return true;
            }
        });



        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1 && spCompany.getSelectedItemId() < 2) {
                    spBank.setVisibility(View.VISIBLE);
                    tvSelectBank.setVisibility(View.VISIBLE);

                } else {
                    spBank.setVisibility(View.GONE);
                    tvSelectBank.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        if (spType.getSelectedItemId() == 1) {
                            spBank.setVisibility(View.VISIBLE);
                            tvSelectBank.setVisibility(View.VISIBLE);
                        }
                        tbanks = getResources().getStringArray(R.array.bank_list);
                        tbanks = banks;

                        break;
                    case 1:
                        if (spType.getSelectedItemId() == 1) {
                            spBank.setVisibility(View.VISIBLE);
                            tvSelectBank.setVisibility(View.VISIBLE);
                        }
                        tbanks = new String[2];
                        tbanks[0] = banks[2];
                        tbanks[1] = banks[3];
                        break;
                    case 2:
                        tvSelectBank.setVisibility(View.GONE);
                        spBank.setVisibility(View.GONE);
                        break;


                }

                ArrayAdapter<String> adapterBank = new ArrayAdapter<String>(QuotationFormActivity.this, R.layout.product_list_spinner_item, tbanks);
                spBank.setAdapter(adapterBank);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvSelectBank.setVisibility(View.GONE);
        spBank.setVisibility(View.GONE);

    }


    public void setUpFromFields() {


        types = getResources().getStringArray(R.array.document_type);
        companies = getResources().getStringArray(R.array.company_name);
        banks = getResources().getStringArray(R.array.bank_list);
        dtype = getResources().getStringArray(R.array.delivery_type);
        dtime = getResources().getStringArray(R.array.delivery_time);
        terms = getResources().getStringArray(R.array.payment_terms);


        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(
                this,
                R.layout.product_list_spinner_item,
                types);
        spType.setAdapter(adapterType);

        ArrayAdapter<String> adapterCompany = new ArrayAdapter<String>(
                this,
                R.layout.product_list_spinner_item,
                companies);
        spCompany.setAdapter(adapterCompany);

        ArrayAdapter<String> adapterBank = new ArrayAdapter<String>(
                this,
                R.layout.product_list_spinner_item,
                banks);
        spBank.setAdapter(adapterBank);


        ArrayAdapter<String> adapterdType = new ArrayAdapter<String>(
                this,
                R.layout.product_list_spinner_item,
                dtype);
        spdType.setAdapter(adapterdType);

        ArrayAdapter<String> adapterdTime = new ArrayAdapter<String>(
                this,
                R.layout.product_list_spinner_item,
                dtime);
        spdTime.setAdapter(adapterdTime);

        ArrayAdapter<String> adapterTerms = new ArrayAdapter<String>(
                this,
                R.layout.product_list_spinner_item,
                terms);
        spTerms.setAdapter(adapterTerms);

    }


    public void openProductsDialog(View v) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_products, null);
        dialogBuilder.setView(dialogView);
        spCat = dialogView.findViewById(R.id.category_name);
        spPro = dialogView.findViewById(R.id.product_name);
        etQuantity = dialogView.findViewById(R.id.quantity);
        etPrice = dialogView.findViewById(R.id.price);
        Button btnAdd = dialogView.findViewById(R.id.add_product);

        List<String> list = new ArrayList<>();
        categoryList = Category.listAll(Category.class);
        for (int i = 0; i < categoryList.size(); i++) {
            list.add(categoryList.get(i).getCategoryName());
        }
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                QuotationFormActivity.this, R.layout.product_list_spinner_item, list) {
        };
        spCat.setAdapter(spinnerArrayAdapter);


        spCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                productID = 0;
                setProductList();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


        spPro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                String[] pro = (spPro.getSelectedItem().toString()).split("\n");
                List<Products> product = Products.find(Products.class, "product_name = ?", pro[0]);
                productID = product.get(0).productId;
                prodName = product.get(0).productName;
                imageName = product.get(0).imageName;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProduct();
            }
        });


        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();


            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tempSelectedList.clear();
                tempselectedNames.clear();

            }
        });

        b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedList.addAll(tempSelectedList);
                selectedNames.addAll(tempselectedNames);
                tempSelectedList.clear();
                tempselectedNames.clear();

                adapter.notifyDataSetChanged();
                b.dismiss();
            }
        });


    }


    public void setProductList() {

        String selectedCategory = spCat.getSelectedItem().toString();
        List<Category> category = Category.find(Category.class, "category_name = ?", selectedCategory);
        int cat_id = category.get(0).getCategoryId();
        productsList = Products.find(Products.class, "product_category = ?", String.valueOf(cat_id));
        List<String> list = new ArrayList<>();
        for (int i = 0; i < productsList.size(); i++) {
            list.add(productsList.get(i).productName);

        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                QuotationFormActivity.this, R.layout.product_list_spinner_item, list) {
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
        spPro.setAdapter(spinnerArrayAdapter);

    }


    public void addProduct() {
        String quantity = etQuantity.getText().toString();
        String price = etPrice.getText().toString();
        if (TextUtils.isEmpty(quantity) || TextUtils.isEmpty(price)) {
            Toast.makeText(mContext, "Please enter quantity and price ", Toast.LENGTH_LONG).show();


        } else {


            if (!selectedNames.contains(prodName)) {
                Prolist pro = new Prolist(productID, imageName, prodName, Integer.valueOf(quantity), Long.valueOf(price));
                etPrice.getText().clear();
                etQuantity.getText().clear();
                Toast toast = Toast.makeText(mContext, "Added " + prodName, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
                tempSelectedList.add(pro);
                tempselectedNames.add(prodName);

            } else {

                Toast.makeText(mContext, "Already Selected ", Toast.LENGTH_LONG).show();


            }


        }


    }


    public void sendDocuments(View v) {
        total = 0;
        // Volley's json array request object
        for (Prolist p : selectedList) {
            total = total + (p.getProductPrice() * p.getProductQuantity());

        }

        if (selectedList.size() == 0) {
            Toast.makeText(mContext, "No Products Selected", Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.show();
        callQuatationApi();
        progressDialog.dismiss();

    }


    public void callQuatationApi() {



        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_EMAIL_DOCUMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // looping through json and adding to movies list

                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getBoolean("status")) {
                                ActivityLog log = new ActivityLog(5, obj.getLong("time"), Integer.valueOf(eId));
                                log.save();

                                AlertDialog dialog = new AlertDialog.Builder(QuotationFormActivity.this)
                                        .setTitle("Success")
                                        .setIcon(R.drawable.ic_info)
                                        .setMessage("Documents Mailed Successfully to the client")
                                        .setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        finish();
                                                    }
                                                }).create();
                                dialog.show();

                            } else {
                                AlertDialog dialog = new AlertDialog.Builder(QuotationFormActivity.this)
                                        .setTitle("Oops")
                                        .setIcon(R.drawable.ic_info)
                                        .setMessage("Looks like some error occured. Failed to Send documents.")
                                        .setPositiveButton("Retry",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        callQuatationApi();
                                                    }
                                                }).setNegativeButton("Cancel",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Intent myIntent = new Intent(mContext, EnquiryDetailsActivity.class);
                                                        ActivityOptions options =
                                                                ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                                                        myIntent.putExtra("enquiry_id", eId);
                                                        mContext.startActivity(myIntent, options.toBundle());
                                                        finish();
                                                    }
                                                }).create();
                                dialog.show();

                            }


                            // Initializing an ArrayAdapter



                        } catch (JSONException e) {

                            Log.e("QuotationFormActivity", "JSON Parsing error: " + e.getMessage());
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                AlertDialog dialog = new AlertDialog.Builder(QuotationFormActivity.this)
                        .setTitle("Oops")
                        .setIcon(R.drawable.ic_info)
                        .setMessage("Network Issue. Failed to Send documents.")
                        .setPositiveButton("Retry",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        callQuatationApi();
                                    }
                                }).setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).create();
                dialog.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("enquiry_id", eId);
                params.put("document_type", spType.getSelectedItem().toString());
                params.put("company_name", spCompany.getSelectedItem().toString());

                if (spCompany.getSelectedItem().toString().matches("KIRAN TRADERS")) {
                    params.put("bank_name", "LAXMI BANK");
                    params.put("account_no", "00611034126");
                } else if (spCompany.getSelectedItem().toString().matches("KESHARICHAND BHANWARLALL")) {
                    params.put("bank_name", spBank.getSelectedItem().toString());
                    switch (spBank.getSelectedItem().toString()) {

                        case "SUNRISE BANK":
                            account_no = "002610062061014";
                            break;
                        case "MEGA BANK":
                            account_no = "0081050029758";
                            break;
                        case "NABIL BANK":
                            account_no = "0701017501150";
                            break;
                        case "CIVIL BANK":
                            account_no = "00610040556012";
                            break;
                        case "GLOBAL IME BANK":
                            account_no = "1501010000304";
                            break;
                        case "LAXMI BANK":
                            account_no = "00611033464";
                            break;
                        case "NIC ASIA BANK":
                            account_no = "0241454216524001";
                            break;
                        case "SANIMA BANK":
                            account_no = "0180000023701";
                            break;
                        case "SIDDHARTHA BANK":
                            account_no = "00300320620";
                            break;


                    }
                    params.put("account_no", account_no);
                } else {
                    params.put("bank_name", spBank.getSelectedItem().toString());
                    switch (spBank.getSelectedItem().toString()) {

                        case "MEGA BANK":
                            account_no = "0080010052580";
                            break;
                        case "LAXMI BANK":
                            account_no = "00611035437";
                            break;

                    }
                    params.put("account_no", account_no);
                }


                params.put("delivery_type", spdType.getSelectedItem().toString());
                params.put("delivery_time", spdTime.getSelectedItem().toString());
                params.put("payment_terms", spTerms.getSelectedItem().toString());
                params.put("product_list", new Gson().toJson(selectedList));
                params.put("cc_list", new Gson().toJson(ccMails));
                params.put("grand_total", String.valueOf(total));
                params.put("vat_pan", etVat.getText().toString());
                params.put("send_brochures", String.valueOf(cbSendBro.isChecked()));
                params.put("salesman_name", SharedPreManager.getInstance(mContext).getUser().getUsername());
                params.put("salesman_number", SharedPreManager.getInstance(mContext).getUser().getNumber());
                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
