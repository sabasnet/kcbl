package com.deltatechnepal.kcbl;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.ResyncEnquiries;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.helper.VolleySingleton;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;
import com.orm.SugarTransactionHelper;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EprActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private Context mContext;
    private String TAG = EprActivity.class.getSimpleName();
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_ID = "keyid";
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView rvEnquiry;
    private EprAdapter adapteroff;
    private List<Offenquiry> enq = new ArrayList<>();
    List<Offenquiry> tempEnq = new ArrayList<>();
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    EditText f1, f2;
    TextView date;
    String number;
    static boolean active = false;
    RadioButton hot, warm, cold;
    boolean visible, status;
    ViewGroup transitionsContainer;
    Animation anim;
    LinearLayout placeholder;
    DateConversion dc;
    private final String KEY_RECYCLER_STATE = "recycler_state";
    private static Bundle mBundleRecyclerViewState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        mContext = this;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        dc = new DateConversion();
        placeholder = findViewById(R.id.placeholder);


        rvEnquiry = findViewById(R.id.rvEnquiry);
        linearLayoutManager =
                new LinearLayoutManager(EprActivity.this);
        rvEnquiry.setLayoutManager(linearLayoutManager);
        rvEnquiry.setHasFixedSize(true);
        rvEnquiry.setNestedScrollingEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        //fabTransition
        transitionsContainer = findViewById(R.id.topContainer);


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(800); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvEnquiry);


        rvEnquiry.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            String offset = String.valueOf(totalItemCount + 1);
                            String limit = String.valueOf(totalItemCount + 50);

                            tempEnq = Select.from(Offenquiry.class)
                                    .where(Condition.prop("reg").eq(0), Condition.prop("reason").eq(0))
                                    .orderBy("id DESC")
                                    .limit(limit).offset(offset).list();

                            rvEnquiry.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapteroff.updateOffenquiryListItems(tempEnq);
                                    tempEnq.clear();
                                }
                            });


                        }
                    }
                }
            }
        });


        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                boolean status = ConnectionChecker.getInstance(mContext).Check();
                if (Offenquiry.count(Offenquiry.class) == 0 && status) {
                    ResyncEnquiries.getInstance(mContext).fetchserverEnquiries();


                } else {

                    fetchlocalEnquiries();
                }
            }
        });

    }


    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                    RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {

            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }


        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().clearView(foregroundView);

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {


            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }



     /*   @Override
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            return super.convertToAbsoluteDirection(flags, layoutDirection);
        }*/


        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            //Remove swiped item from list and notify the RecyclerView
            int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
            if (callPermission == 0) {
                adapteroff.notifyItemChanged(viewHolder.getAdapterPosition());
                Intent intent = new Intent(Intent.ACTION_CALL);
                int position = viewHolder.getAdapterPosition();


                number = enq.get(position).mobileNumber;

                intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + getApplication().getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                getApplication().startActivity(i);
            }
        }
    };


    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);

        fetchlocalEnquiries();

        swipeRefreshLayout.setRefreshing(false);

    }


    private void fetchlocalEnquiries() {

        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("reg").eq(0), Condition.prop("reason").eq(0))
                .orderBy("id DESC")
                .limit("50")
                .list();
        adapteroff = new EprAdapter(EprActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);


    }




    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_normal, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Intent myIntent = new Intent(mContext, SearchActivity.class);
                myIntent.putExtra("query_value", query);
                startActivity(myIntent);
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_notifications: {
                Intent i = new Intent(getBaseContext(), NotificationActivity.class);
                startActivity(i);
                break;
            }


        }
        return super.onOptionsItemSelected(item);
    }


    public void onResume(){
        super.onResume();
        onRefresh();
        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            rvEnquiry.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = rvEnquiry.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

   @Override
    public void onPause() {
        super.onPause();
       // save RecyclerView state
       mBundleRecyclerViewState = new Bundle();
       Parcelable listState = rvEnquiry.getLayoutManager().onSaveInstanceState();
       mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);

   }




    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}

