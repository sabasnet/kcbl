package com.deltatechnepal.kcbl;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.orm.Offenquiry;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;

    private RecyclerView.LayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvEnquiry;
    private EprAdapter adapteroff;


    private List<Offenquiry> enq = new ArrayList<>();


    boolean visible, status;

    ViewGroup transitionsContainer;
    Animation anim;
    String number;

    LinearLayout placeholder;
    DateConversion dc;
    String previousValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.search_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvEnquiry = findViewById(R.id.rvEnquiry);
        linearLayoutManager =
                new LinearLayoutManager(SearchActivity.this);
        rvEnquiry.setLayoutManager(linearLayoutManager);
        rvEnquiry.setHasFixedSize(true);
        rvEnquiry.setNestedScrollingEnabled(true);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(null);
        itemTouchHelper.attachToRecyclerView(rvEnquiry);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);


        //fabTransition
        transitionsContainer = findViewById(R.id.topContainer);


        Intent mIntent = getIntent();
        final String searchText = mIntent.getStringExtra("query_value");


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        placeholder = findViewById(R.id.placeholder);

        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        placeholder.setVisibility(View.VISIBLE);
                                        placeholder.startAnimation(anim);
                                        fetchMathingEnquiries(searchText);
                                    }

                                }
        );
    }


    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        fetchMathingEnquiries(previousValue);
        swipeRefreshLayout.setRefreshing(false);

    }

    private void fetchMathingEnquiries(String value) {

        previousValue = "%" + value + "%";

        enq = Offenquiry.find(Offenquiry.class, "customer_name LIKE ? or mobile_number LIKE ?" +
                        "or contact_person LIKE ? or customer_address LIKE ? or contact_person_address LIKE ?"
                , previousValue, previousValue, previousValue, previousValue, previousValue);
        adapteroff = new EprAdapter(SearchActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }


    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        /*  @Override
          public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {

                  final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

                  getDefaultUIUtil().onSelected(foregroundView);


          }*/
        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                    RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {

            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }


        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().clearView(foregroundView);

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {


            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }



     /*   @Override
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            return super.convertToAbsoluteDirection(flags, layoutDirection);
        }*/


        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            //Remove swiped item from list and notify the RecyclerView
            int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
            if (callPermission == 0) {
                adapteroff.notifyItemChanged(viewHolder.getAdapterPosition());
                Intent intent = new Intent(Intent.ACTION_CALL);
                int position = viewHolder.getAdapterPosition();

                number = enq.get(position).mobileNumber;
                intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + getApplication().getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                getApplication().startActivity(i);
            }
        }
    };

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_normal, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print

                fetchMathingEnquiries(query);
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
