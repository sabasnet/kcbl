package com.deltatechnepal.kcbl;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.adapter.CollectionListAdapter;
import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.adapter.QuotationListAdapter;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.model.Prolist;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Quotation;
import com.deltatechnepal.orm.Uenquiry;
import com.google.gson.Gson;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuotationListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;

    private RecyclerView.LayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvQuotation;
    private QuotationListAdapter adapteroff;
    private String enquiry_id;
    ProgressDialog progressDialog;



    private List<Quotation> quotationList = new ArrayList<>();


    boolean visible, status;
    Animation anim;
    String number;

    LinearLayout placeholder;
    DateConversion dc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.quotation_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent intent=getIntent();
        enquiry_id=intent.getStringExtra("enquiry_id");


        rvQuotation = findViewById(R.id.rvQuotations);
        linearLayoutManager =
                new LinearLayoutManager(QuotationListActivity.this);
        rvQuotation.setLayoutManager(linearLayoutManager);
        rvQuotation.setHasFixedSize(true);
        rvQuotation.setNestedScrollingEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);


        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        fetchQuotationList();
                                    }

                                }
        );
    }


    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setRefreshing(false);

    }

    private void fetchQuotationList() {
        progressDialog = new ProgressDialog(QuotationListActivity.this, R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Fetching Quotation List..."); // Setting Message
        progressDialog.setTitle(null); // Setting Title
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_FETCH_QUOTATIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        // looping through json and adding to movies list

                                try {


                                    JSONObject obj = new JSONObject(response);
                                    Toast.makeText(mContext,obj.getString("message"),Toast.LENGTH_SHORT).show();
                                    JSONArray quotationsArray = obj.getJSONArray("quotations");
                                    for (int i = 0; i < quotationsArray.length(); i++) {

                                        JSONObject quotation = quotationsArray.getJSONObject(i);

                                        int quoId=quotation.getInt("id");
                                        String docType = quotation.getString("document_type");
                                        String grandTotal = quotation.getString("grand_total");
                                        String sentDate = quotation.getString("created_at");
                                        String productList = quotation.getString("product_list");


                                        Quotation quo = new Quotation(quoId,docType, productList,grandTotal,sentDate);
                                        quotationList.add(quo);


                                    }

                                    Collections.sort(quotationList, new Comparator<Quotation>(){
                                        public int compare(Quotation obj1, Quotation obj2) {
                                            // ## Descending order
                                            return Integer.valueOf(obj2.quotationId).compareTo(Integer.valueOf(obj1.quotationId)); // To compare integer values
                                        }
                                    });
                                    adapteroff = new QuotationListAdapter(QuotationListActivity.this, quotationList);
                                    rvQuotation.setAdapter(adapteroff);
                                    progressDialog.dismiss();


                                } catch (JSONException e) {

                                }




                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("enquiry_id",enquiry_id);
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


    }










    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
