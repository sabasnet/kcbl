package com.deltatechnepal.kcbl;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.orm.Offenquiry;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class EnquiryListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;

    private String TAG = EprActivity.class.getSimpleName();
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_ID = "keyid";
    private RecyclerView.LayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvEnquiry;
    private EprAdapter adapteroff;
    private List<Offenquiry> offList = new ArrayList<>();
    private List<Offenquiry> enq = new ArrayList<>();

    EditText f1, f2;

    private FloatingActionButton fab;
    TextView date;
    String number;

    RadioButton hot, warm, cold;

    long oldCount = 0;

    boolean visible, status;

    ViewGroup transitionsContainer;
    Animation anim;
    int mode = 0;

    LinearLayout placeholder;
    DateConversion dc;
    private final String KEY_RECYCLER_STATE = "recycler_state";
    private static Bundle mBundleRecyclerViewState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.enquiry_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dc = new DateConversion();

        rvEnquiry = findViewById(R.id.rvEnquiry);
        linearLayoutManager =
                new LinearLayoutManager(EnquiryListActivity.this);
        rvEnquiry.setLayoutManager(linearLayoutManager);
        rvEnquiry.setHasFixedSize(true);
        rvEnquiry.setNestedScrollingEnabled(true);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(null);
        itemTouchHelper.attachToRecyclerView(rvEnquiry);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);


        //fabTransition
        transitionsContainer = findViewById(R.id.topContainer);

        fab = findViewById(R.id.fab);


        Intent mIntent = getIntent();
        mode = mIntent.getIntExtra("mode", 0);


        Toast.makeText(mContext, dc.getCurrentDate(), Toast.LENGTH_SHORT).show();


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        placeholder = findViewById(R.id.placeholder);

        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        placeholder.setVisibility(View.VISIBLE);
                                        placeholder.startAnimation(anim);
                                        setMode();
                                    }
                                }
        );
    }


    public void setMode() {

        switch (mode) {
            case 0:
                fetchlocalEnquiries();
                break;
            case 1:
                fetchWinEnquiries();
                getSupportActionBar().setTitle("Win");
                break;
            case 2:
                fetchLossEnquiries();
                getSupportActionBar().setTitle("Loss");
                break;
            case 3:
                fetchDropEnquiries();
                getSupportActionBar().setTitle("Drop");
                break;
            case 7:
                fetchHotEnquiries();
                getSupportActionBar().setTitle("Hot");
                break;
            case 15:
                fetchWarmEnquiries();
                getSupportActionBar().setTitle("Warm");
                break;
            case 25:
                fetchColdEnquiries();
                getSupportActionBar().setTitle("Cold");
                break;

        }

    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        setMode();
        swipeRefreshLayout.setRefreshing(false);
    }


    private void fetchWinEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("enq_result").eq(1))
                .orderBy("dateof_enquiry DESC")
                .list();
        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }

    private void fetchLossEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("enq_result").eq(2))
                .orderBy("dateof_enquiry DESC")
                .list();
        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }

    private void fetchDropEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("enq_result").eq(3))
                .orderBy("dateof_enquiry DESC")
                .list();
        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }

    private void fetchHotEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("status").eq(7), Condition.prop("enq_result").eq(0))
                .orderBy("dateof_enquiry DESC")
                .list();
        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }

    private void fetchWarmEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("status").eq(15), Condition.prop("enq_result").eq(0))
                .orderBy("dateof_enquiry DESC")
                .list();
        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }

    private void fetchColdEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("status").eq(25), Condition.prop("enq_result").eq(0))
                .orderBy("dateof_enquiry DESC")
                .list();
        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }


    private void fetchlocalEnquiries() {
        enq = Select.from(Offenquiry.class)
                .where(Condition.prop("enq_result").eq(0), Condition.prop("next_follow_up_date").lt(dc.getTomorrowDateinMilli()))
                .orderBy("next_follow_up_date DESC")
                .list();

        adapteroff = new EprAdapter(EnquiryListActivity.this, enq);
        rvEnquiry.setAdapter(adapteroff);
        oldCount = Offenquiry.count(Offenquiry.class);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }


    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        /*  @Override
          public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {

                  final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

                  getDefaultUIUtil().onSelected(foregroundView);


          }*/
        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                    RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {

            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }


        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().clearView(foregroundView);

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {


            final View foregroundView = ((EprAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }



     /*   @Override
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            return super.convertToAbsoluteDirection(flags, layoutDirection);
        }*/


        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            //Remove swiped item from list and notify the RecyclerView
            int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
            if (callPermission == 0) {
                adapteroff.notifyItemChanged(viewHolder.getAdapterPosition());
                Intent intent = new Intent(Intent.ACTION_CALL);
                int position = viewHolder.getAdapterPosition();
                number = enq.get(position).mobileNumber;

                intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + getApplication().getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                getApplication().startActivity(i);
            }
        }
    };

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_normal, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Intent myIntent = new Intent(mContext, SearchActivity.class);
                myIntent.putExtra("query_value", query);
                startActivity(myIntent);
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_notifications: {
                Intent i = new Intent(getBaseContext(), NotificationActivity.class);
                startActivity(i);
                break;
            }


        }
        return super.onOptionsItemSelected(item);
    }

    public void onResume(){
        super.onResume();
        onRefresh();
        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            rvEnquiry.getLayoutManager().onRestoreInstanceState(listState);
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = rvEnquiry.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    @Override
    public void onPause() {
        super.onPause();
        // save RecyclerView state
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = rvEnquiry.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
