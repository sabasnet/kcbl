package com.deltatechnepal.kcbl;


import android.Manifest;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.deltatechnepal.adapter.DashMenuAdapter;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.CountDrawable;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.ForceUpdateChecker;
import com.deltatechnepal.helper.ResyncEnquiries;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.model.DashMenu;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.CollectionFollowUps;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.service.BrochureFetchJob;
import com.deltatechnepal.service.CategoriesFetchJob;
import com.deltatechnepal.service.CollectionsFetchJob;
import com.deltatechnepal.service.EnquirySyncJob;
import com.deltatechnepal.service.NotificationSyncJob;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

;

public class MainActivity extends AppCompatActivity implements ForceUpdateChecker.OnUpdateNeededListener {

    DrawerLayout mDrawerLayout;
    private Context mContext;
    DashMenuAdapter dmAdapter;
    List<Offenquiry> enq;
    Offenquiry off;
    Collection coll;
    Notification notif;
    List<DashMenu> menuList = new ArrayList<>();
    GridView gvDash;
    TextView counter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        dmAdapter = new DashMenuAdapter(mContext, menuList);
        notif = new Notification();


        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();


        //Schedule Data Syncing
        EnquirySyncJob.scheduleJob();




        NotificationSyncJob.scheduleJob();
        CollectionsFetchJob.scheduleJob();
        EnquirySyncJob.runJobImmediately();
        if (Offenquiry.count(Offenquiry.class) == 0 && ConnectionChecker.getInstance(mContext).Check())
            ResyncEnquiries.getInstance(mContext).fetchserverEnquiries();
        if (Collection.count(Collection.class) == 0)
            CollectionsFetchJob.runJobImmediately();

        DateConversion dc = new DateConversion();
        off = new Offenquiry();
        coll = new Collection();


        enq = Offenquiry.find(Offenquiry.class, "next_follow_up_date = ?", (dc.getCurrentDateinMilli()));


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {



                        /*menuItem.setChecked(true);*/
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {
                            case R.id.nav_logout: {
                                new AlertDialog.Builder(mContext)
                                        .setTitle(getString(R.string.app_name))
                                        .setMessage("Want To Logout?")
                                        .setIcon(R.drawable.ic_info)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                SharedPreManager.getInstance(getApplicationContext()).logout();
                                                finish();
                                            }
                                        })
                                        .setNegativeButton("No", null).show();


                                break;
                            }


                            case R.id.nav_sync_enquiries: {
                                Toast.makeText(mContext, "Syncing Started in Background", Toast.LENGTH_SHORT).show();
                                CollectionFollowUps.deleteAll(CollectionFollowUps.class);
                                CollectionFollowUps.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "COLLECTION_FOLLOW_UPS" + "'");
                                ActivityLog.deleteAll(ActivityLog.class);
                                ActivityLog.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "ACTIVITY_LOG" + "'");


                                ResyncEnquiries.getInstance(mContext).fetchserverEnquiries();
                                CategoriesFetchJob.runJobImmediately();
                                BrochureFetchJob.runJobImmediately();
                                CollectionsFetchJob.runJobImmediately();

                                break;
                            }

                            case R.id.nav_epr: {
                                startActivity(new Intent(mContext, EprActivity.class));
                                break;
                            }

                            case R.id.nav_collection: {
                                Toast.makeText(mContext, "Coming Soon", Toast.LENGTH_SHORT).show();
                                break;
                            }
                            case R.id.nav_enquiry_followup: {
                                startActivity(new Intent(mContext, EnquiryListActivity.class));
                                break;
                            }
                            case R.id.nav_hot_leads: {
                                Intent myIntent = new Intent(mContext, EnquiryListActivity.class);
                                myIntent.putExtra("mode", 7);
                                startActivity(myIntent);
                                break;

                            }
                            case R.id.nav_warm_leads: {
                                Intent myIntent = new Intent(mContext, EnquiryListActivity.class);
                                myIntent.putExtra("mode", 15);
                                startActivity(myIntent);
                                break;

                            }
                            case R.id.nav_cold_leads: {
                                Intent myIntent = new Intent(mContext, EnquiryListActivity.class);
                                myIntent.putExtra("mode", 25);
                                startActivity(myIntent);
                                break;

                            }
                            case R.id.nav_win: {
                                Intent myIntent = new Intent(mContext, EnquiryListActivity.class);
                                myIntent.putExtra("mode", 1);
                                startActivity(myIntent);
                                break;
                            }
                            case R.id.nav_loss: {
                                Intent myIntent = new Intent(mContext, EnquiryListActivity.class);
                                myIntent.putExtra("mode", 2);
                                startActivity(myIntent);
                                break;
                            }
                            case R.id.nav_drop: {
                                Intent myIntent = new Intent(mContext, EnquiryListActivity.class);
                                myIntent.putExtra("mode", 3);
                                startActivity(myIntent);
                                break;

                            }

                        }


                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        //region Load and Set Profile Data in Navigation Header

        String imageUrl = URLs.URL_PROFILE_IMAGE + SharedPreManager.getInstance(mContext).getUser().getImage();


        View navHeader = navigationView.getHeaderView(0);
        ImageView profile = navHeader.findViewById(R.id.userImage);
        Glide.with(this).load(imageUrl).into(profile);
        TextView name = navHeader.findViewById(R.id.userName);
        name.setText(SharedPreManager.getInstance(mContext).getUser().getUsername());
        TextView email = navHeader.findViewById(R.id.userEmail);
        email.setText(SharedPreManager.getInstance(mContext).getUser().getEmail());

        //endregion

        setgvDash();


        //region Setting Counters
        Counter ecount = Counter.findById(Counter.class, 1);

        counter = findViewById(R.id.new_enquiry_counter);
        counter.setText(String.valueOf((ecount.assigned + ecount.pending) - ecount.completed));
        //endregion


        //region Permissions
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.READ_CALENDAR, Manifest.permission.READ_CALENDAR}, 101);
        //endregion


        //Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("notification-count-update"));


    }


    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setNotifCount();
            //region Setting Counters
            Counter ecount = Counter.findById(Counter.class, 1);
            counter.setText(String.valueOf((ecount.assigned + ecount.pending) - ecount.completed));
            //endregion
        }
    };


    public void AddNew(View v) {
        Intent i = new Intent(getBaseContext(), AddEnquiryActivity.class);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();

        DashMenu erpMenu = new DashMenu(off.getEPRCount(), "EPR", "EprActivity", R.drawable.menu_epr);
        menuList.set(0, erpMenu);

        DashMenu inquiryMenu = new DashMenu(off.getFollowUpCount(), "Enquiry\nFollowUp", "EnquiryListActivity", R.drawable.menu_call);
        menuList.set(1, inquiryMenu);

        DashMenu collection = new DashMenu(coll.getFreshCollectionCount(), "Collection", "CollectionListActivity", R.drawable.menu_collection);
        menuList.set(2, collection);


        DashMenu collectionFollowUp = new DashMenu(coll.getCollectionFollowUpCount(), "Collection\nFollowUp", "CollectionFollowUp", R.drawable.menu_home_visit);
        menuList.set(3, collectionFollowUp);

        dmAdapter.notifyDataSetChanged();
        /*  gvDash.setAdapter(dmAdapter);
         */
        setNotifCount();


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        setCount(menu, mContext, String.valueOf(notif.getNotificationCount()));
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Intent myIntent = new Intent(mContext, SearchActivity.class);
                myIntent.putExtra("query_value", query);
                startActivity(myIntent);

                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    private void setNotifCount() {
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home: {
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            }

            case R.id.action_notifications:
                Intent i = new Intent(getBaseContext(), NotificationActivity.class);
                startActivity(i);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void setCount(Menu menu, Context context, String count) {
        MenuItem menuItem = menu.findItem(R.id.action_notifications);
        LayerDrawable icon = (LayerDrawable) menuItem.getIcon();

        CountDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.app_name))
                    .setMessage("Want To Exit?")
                    .setIcon(R.drawable.ic_info)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            finish();
                        }
                    })
                    .setNegativeButton("No", null).show();
        }
    }

    private void setgvDash() {
        gvDash = findViewById(R.id.gvDash);


        DashMenu erpMenu = new DashMenu(off.getEPRCount(), "EPR", "EprActivity", R.drawable.menu_epr);
        menuList.add(erpMenu);

        DashMenu inquiryMenu = new DashMenu(off.getFollowUpCount(), "Enquiry\nFollowUp", "EnquiryListActivity", R.drawable.menu_call);
        menuList.add(inquiryMenu);

        DashMenu collectionMenu = new DashMenu(0, "Collection", "CollectionListActivity", R.drawable.menu_collection);
        menuList.add(collectionMenu);

        DashMenu visitMenu = new DashMenu(0, "Collection\nFollowUp", "CollectionFollowUp", R.drawable.menu_home_visit);
        menuList.add(visitMenu);

        DashMenu emiCalculator = new DashMenu(0, "EMI Calculator", "EmiActivity", R.drawable.ic_calculator);
        menuList.add(emiCalculator);

        DashMenu activityLog = new DashMenu(0, "Activity Log", "LogActivity", R.drawable.menu_log);
        menuList.add(activityLog);


        gvDash.setAdapter(dmAdapter);
        gvDash.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String activityToStart = "com.deltatechnepal.kcbl." + menuList.get(position).activity;
                Log.i("activity", activityToStart);
                try {
                    Class<?> c = Class.forName(activityToStart);

                    Intent intent = new Intent(mContext, c);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
                    startActivity(intent, options.toBundle());

                } catch (ClassNotFoundException ignored) {
                    Log.i("Ignored", ignored.toString());
                }

            }
        });
    }


    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void onUpdateNeeded() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setIcon(R.drawable.ic_info)
                .setMessage("Please, update app to new version for optimal experience")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or ActivityLog object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create();
        dialog.show();
    }


    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
























