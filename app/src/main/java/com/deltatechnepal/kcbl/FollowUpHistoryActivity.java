package com.deltatechnepal.kcbl;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.adapter.EnquiryFollowUpAdapter;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FollowUpHistoryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;
    AlertDialog b;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvEnquiry;
    private EnquiryFollowUpAdapter adapter;
    private List<EnquiryFollowUps> enq = new ArrayList<>();
    List<EnquiryFollowUps> tempEnq = new ArrayList<>();
    EditText f1, f2;
    int called = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private FloatingActionButton fab;
    TextView date;
    String number;
    int mode;

    boolean visible, status;

    ViewGroup transitionsContainer;
    Animation anim;

    LinearLayout placeholder;
    DateConversion dc;
    String eId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.follow_up_history_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dc = new DateConversion();


        rvEnquiry = findViewById(R.id.rvEnquiryHistory);
        linearLayoutManager =
                new LinearLayoutManager(FollowUpHistoryActivity.this);
        rvEnquiry.setLayoutManager(linearLayoutManager);
        rvEnquiry.setHasFixedSize(true);
        rvEnquiry.setNestedScrollingEnabled(true);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);


        rvEnquiry.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            String offset = String.valueOf(totalItemCount + 1);
                            String limit = String.valueOf(totalItemCount + 50);


                            tempEnq = Select.from(EnquiryFollowUps.class)
                                    .where(Condition.prop("enquiry_id").eq(eId))
                                    .orderBy("created_at DESC")
                                    .limit(limit).offset(offset).list();


                            rvEnquiry.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.updateOffenquiryListItems(tempEnq);
                                    tempEnq.clear();
                                }
                            });


                        }
                    }
                }
            }
        });


        //fabTransition
        transitionsContainer = findViewById(R.id.topContainer);

        fab = findViewById(R.id.fab);


        Intent mIntent = getIntent();
        long id = mIntent.getLongExtra("enquiry_id", 0);
        eId = String.valueOf(id);

        enq = Select.from(EnquiryFollowUps.class)
                .where(Condition.prop("enquiry_id").eq(eId))
                .orderBy("created_at DESC")
                .limit("50")
                .list();


        List<Offenquiry> find = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);

        List<Products> pro = Products.find(Products.class, "product_name = ?", find.get(0).productName);
        if (pro.size() == 0) {
            Toast.makeText(mContext, "The Product associated with this enquiry is deleted.So you can`t add the follow up", Toast.LENGTH_LONG).show();
            fab.setVisibility(View.GONE);
        } else {
            setUpModes();
        }


        getSupportActionBar().setTitle(find.get(0).customerName);
        if (find.get(0).enqResult != 0)
            fab.setEnabled(false);


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        placeholder = findViewById(R.id.placeholder);

        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        placeholder.setVisibility(View.VISIBLE);
                                        placeholder.startAnimation(anim);
                                        fetchlocalEnquiries();
                                    }

                                }
        );
    }


    public void setUpModes() {

        if (enq.size() != 0 && enq.get(0).nextFollowUpMode == 0) {
            fab.setVisibility(View.GONE);
        } else {


            if (enq.size() != 0) {

                if (enq.get(0).nextFollowUpMode == 2)
                    fab.setImageResource(R.drawable.ic_phone);
                else
                    fab.setImageResource(R.drawable.ic_add_black_24dp);
            } else {

                fab.setImageResource(R.drawable.ic_add_black_24dp);
            }
        }


    }
    public void openFollowUpModeDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_followup_mode, null);
        dialogBuilder.setView(dialogView);

        Button btnCall=dialogView.findViewById(R.id.btnCall);
        Button btnVisit=dialogView.findViewById(R.id.btnVisit);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                callFollowUp();
            }
        });

        btnVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(mContext, AddFollowUpActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                myIntent.putExtra("enquiry_id", eId);
                myIntent.putExtra("first_followup",true);
                myIntent.putExtra("previous_next_followup_mode",1);
                mContext.startActivity(myIntent, options.toBundle());
                b.dismiss();
            }
        });




        b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);




    }




    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        enq.clear();
        enq = Select.from(EnquiryFollowUps.class)
                .where(Condition.prop("enquiry_id").eq(eId))
                .orderBy("created_at DESC")
                .limit("50")
                .list();
        fetchlocalEnquiries();
        swipeRefreshLayout.setRefreshing(false);

    }

    private void fetchlocalEnquiries() {


        adapter = new EnquiryFollowUpAdapter(FollowUpHistoryActivity.this, enq);
        rvEnquiry.setAdapter(adapter);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }


    public void AddFollowUp(View v) {
        if (enq.size() != 0) {
            if (enq.get(0).nextFollowUpMode == 2) {
                callFollowUp();

            } else {

                Intent myIntent = new Intent(this, AddFollowUpActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                myIntent.putExtra("enquiry_id", eId);
                myIntent.putExtra("first_followup",false);
                myIntent.putExtra("previous_next_followup_mode",enq.get(0).nextFollowUpMode);
                myIntent.putExtra("last_followup_note", enq.get(0).remark);
                mContext.startActivity(myIntent, options.toBundle());

            }
        } else {

            openFollowUpModeDialog();


        }


    }

    public void callFollowUp() {

        Intent mIntent = getIntent();
        String number = mIntent.getStringExtra("mobile_number");
        int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        if (callPermission == 0) {

            if (!TextUtils.isEmpty(number)) {
                called = 1;
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "No Number to Call", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + getApplication().getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            getApplication().startActivity(i);
        }


    }


    public void onResume() {

        super.onResume();
        if (called == 1) {
            Intent myIntent = new Intent(this, AddFollowUpActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
            myIntent.putExtra("enquiry_id", eId);
            mContext.startActivity(myIntent, options.toBundle());
            called = 0;


        } else {

            onRefresh();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
