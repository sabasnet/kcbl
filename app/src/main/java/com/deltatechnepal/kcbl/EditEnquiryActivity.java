package com.deltatechnepal.kcbl;

import android.animation.LayoutTransition;
import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deltatechnepal.helper.ColorText;
import com.deltatechnepal.helper.CustomToast;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.helper.VolleySingleton;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.orm.Subsource;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditEnquiryActivity extends AppCompatActivity {
    private Context mContext;
    private static final String NULL = "";
    String customerName, contactPerson, customerAddress, mobileNumber, locationOfEnquiry, proName, sourceOfInfo, occupation, altMobileNumber, contactPersonAddress,
            contactPersonEmail, prodName, enquiryNote, bankName,phone;
    int leadSource = 0, purchaseMode, status = 0, firstTime = 0, interested = 0, age, titleName, subSource = 0, reg = 1;

    long dateOfEnquiry, nextFollowUp, rate, productID;
    private String TAG = EprActivity.class.getSimpleName();
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_ID = "keyid";
    EditText etCustomerName, etContactPersonName, etMobileNumber, etLocationOfEnquiry, etCustomerAddress, etRate,
            etAge, etOccupation, etPhone, etAltMobileNumber, etContactPersonAddress, etContactPersonEmail, etNote, etBankName;
    TextView tvSourceOfInfo, tvProductName,tvMRP;
    Spinner product_list, sourceofInfo_list, title, purchase_mode, category_list;
    RadioGroup leadstatusradioGroup, firsttimeradioGroup, interestedradioGroup;
    RadioButton hot, warm, cold, yes1, yes2, no1, no2, dontKnow;
    Button dateofEnquiry, nextFollowUpDate;
    Button btnWalkIn, btnTeleIn, btnFieldActivity, btnReferral, btnAgent, btnAdvertisement, btnOthers;
    ConstraintLayout hiddenLayout, spinnerHolder;
    ImageView product_image;
    TextView date;
    List<Products> productsList;
    List<Products> pro;
    List<Category> categoryList;
    CheckBox cbBrochure;
    private List<Offenquiry> enq = new ArrayList<>();
    List<Subsource> sub = new ArrayList<>();
    long cdate, ndate;
    DateConversion dc;
    Button temp;
    ProgressDialog progressDialog;
    String eId;
    ScrollView scrollView;
    boolean isFirst = true;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_enquiry);
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        dc = new DateConversion();


        //region Initializing Layout Components

        product_image = findViewById(R.id.product_image);

        //EditTexts
        etCustomerName = findViewById(R.id.customer_name);


        etCustomerAddress = findViewById(R.id.customer_address);

        etContactPersonName = findViewById(R.id.contact_person);

        etAge = findViewById(R.id.age);
        etOccupation = findViewById(R.id.occupation);
        etPhone = findViewById(R.id.phone);

        etMobileNumber = findViewById(R.id.mobile_number);


        etAltMobileNumber = findViewById(R.id.alt_mobile_number);

        etContactPersonAddress = findViewById(R.id.contact_person_address);


        etContactPersonEmail = findViewById(R.id.contact_person_mail);

        etLocationOfEnquiry = findViewById(R.id.location_of_enquiry);


        etRate = findViewById(R.id.rate);

        etBankName = findViewById(R.id.bank_name);

        etNote = findViewById(R.id.enquiry_note);


        ColorText.getInstance(mContext).multiColor(etCustomerName, getString(R.string.customerName));
        ColorText.getInstance(mContext).multiColor(etContactPersonName, getString(R.string.contactName));
        ColorText.getInstance(mContext).multiColor(etMobileNumber, getString(R.string.contactMobile));
        ColorText.getInstance(mContext).multiColor(etContactPersonAddress, getString(R.string.contactAddress));
        ColorText.getInstance(mContext).multiColor(etLocationOfEnquiry, getString(R.string.locationOfEnquiry));
        ColorText.getInstance(mContext).multiColor(etRate, getString(R.string.rate));


        //spinners
        category_list = findViewById(R.id.category_name);
        product_list = findViewById(R.id.product_name);
        sourceofInfo_list = findViewById(R.id.source_of_information);
        title = findViewById(R.id.title);
        purchase_mode = findViewById(R.id.purchase_mode);

        //RadioGroups
        firsttimeradioGroup = findViewById(R.id.first_time_radio_group);
        yes1 = findViewById(R.id.yes1);
        no1 = findViewById(R.id.no1);
        interestedradioGroup = findViewById(R.id.interested_radio_group);
        yes2 = findViewById(R.id.yes2);
        no2 = findViewById(R.id.no2);
        dontKnow = findViewById(R.id.dontKnow);
        leadstatusradioGroup = findViewById(R.id.status_radio_group);
        hot = findViewById(R.id.hot);
        warm = findViewById(R.id.warm);
        cold = findViewById(R.id.cold);


        //Buttons
        dateofEnquiry = findViewById(R.id.date_of_enquiry);
        nextFollowUpDate = findViewById(R.id.next_follow_up);
        tvMRP = findViewById(R.id.tvMRP);



        //TextViews and extra
        tvProductName = findViewById(R.id.tvProductName);
        tvSourceOfInfo = findViewById(R.id.tvSourceOfInfo);
        hiddenLayout = findViewById(R.id.hiddenSourceLayout);
        spinnerHolder = findViewById(R.id.spinnerHolderhidden);
        scrollView = findViewById(R.id.scrollView);


        //Button Selectors
        btnWalkIn = findViewById(R.id.walkIn);
        btnTeleIn = findViewById(R.id.teleIn);
        btnFieldActivity = findViewById(R.id.fieldActivity);
        btnReferral = findViewById(R.id.referral);
        btnAgent = findViewById(R.id.agent);
        btnAdvertisement = findViewById(R.id.advertisement);
        btnOthers = findViewById(R.id.others);


        //CheckBox
        cbBrochure = findViewById(R.id.cbSendBrochure);

        //endregion


        //region Fixing Animate Layout Changes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ((ViewGroup) findViewById(R.id.hiddenSourceLayout)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGE_APPEARING);
        }
        //endregion


        Intent mIntent = getIntent();
        eId = mIntent.getStringExtra("enquiry_id");

        enq = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);

        dateofEnquiry.setText(dc.getFormattedDate("MM/dd/yyyy", enq.get(0).dateofEnquiry));
        nextFollowUpDate.setText(dc.getFormattedDate("MM/dd/yyyy", enq.get(0).nextFollowUpDate));


        setSpinner(1);
        setSpinner(4);
        setSpinner(5);

        category_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (isFirst)
                    setProductList(true);
                else
                    setProductList(false);

                isFirst = false;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


        product_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                tvMRP.setText("");
                String[] pro = (product_list.getSelectedItem().toString()).split("\n");
                List<Products> product = Products.find(Products.class, "product_name = ?", pro[0]);
                productID = product.get(0).productId;
                prodName = product.get(0).productName;
                tvProductName.setText("Product Name: " + prodName);
                tvMRP.setText("Product MRP : Rs." + product.get(0).defaultPrice);
                Glide.with(mContext).load(URLs.URL_PRODUCT_IMAGES + product.get(0).imageName)
                        .apply(new RequestOptions()
                                .skipMemoryCache(true).placeholder(R.drawable.kcbl))
                        .into(product_image);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


        setValues();


    }


    public void setProductList(boolean isFirst) {
        String selectedCategory = category_list.getSelectedItem().toString();
        List<Category> category = Category.find(Category.class, "category_name = ?", selectedCategory);
        int cat_id = category.get(0).getCategoryId();
        productsList = Products.find(Products.class, "product_category = ?", String.valueOf(cat_id));
        List<String> list = new ArrayList<>();
        for (int i = 0; i < productsList.size(); i++) {
            list.add(productsList.get(i).productName);

        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                EditEnquiryActivity.this, R.layout.product_list_spinner_item, list) {
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
        product_list.setAdapter(spinnerArrayAdapter);

        if (isFirst) {
            int spinnerPosition = spinnerArrayAdapter.getPosition(pro.get(0).productName);
            product_list.setSelection(spinnerPosition);
        }


    }


    //region Lead Source Button Selectors Logic
    public void leadSource(View v) {
        Button button = (Button) v;

        // clear state
        if (temp != null) {
            temp.setSelected(false);
            temp.setPressed(false);
            temp.setTextColor(getResources().getColor(R.color.textGrey1));
        }


        // change state
        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(getResources().getColor(R.color.white));
        temp = button;


        switch (v.getId()) {

            case (R.id.walkIn):
                setSpinner(2);
                leadSource = 1;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.teleIn):
                setSpinner(2);
                leadSource = 2;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.fieldActivity):
                setSpinner(3);
                leadSource = 3;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                tvSourceOfInfo.setText("Type Of ActivityLog");
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.referral):
                leadSource = 4;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);

                break;
            case (R.id.agent):
                leadSource = 5;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);

                break;
            case (R.id.advertisement):
                setSpinner(2);
                leadSource = 6;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                tvSourceOfInfo.setText("Source Of Information");
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.others):
                leadSource = 7;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);

                break;

        }

    }
    //endregion


    //region Setting Up All Spinners
    public void setSpinner(int j) {
        List<String> list = new ArrayList<>();
        switch (j) {

            case 1:

                categoryList = Category.listAll(Category.class);
                for (int i = 0; i < categoryList.size(); i++) {
                    list.add(categoryList.get(i).getCategoryName());
                }
                break;

            case 2:
                list.add("Newspaper");
                list.add("TVC");
                list.add("Pole Banner");
                list.add("Existing Customer");
                list.add("Hoarding");
                list.add("Internet");
                list.add("FM");
                list.add("Leaflet");
                list.add("Mikeing");
                list.add("Road Show");
                list.add("Exchange Mela");
                list.add("Website");
                list.add("Digital Marketing");
                list.add("Others");
                break;
            case 3:
                list.add("Test Drive/Demo");
                list.add("Road Show");
                list.add("Exchange Mela");
                list.add("Existing Customer");
                list.add("Leaflet");
                list.add("Mikeing");
                list.add("Website");
                list.add("Digital Marketing");
                list.add("Others");
                break;

            case 4:
                list.add("Mr");
                list.add("Mrs");
                list.add("Miss");
                list.add("Ms");
                break;

            case 5:
                list.add("Cash");
                list.add("Finance");
                list.add("Exchange");
                list.add("Credit");
                break;


        }

        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                EditEnquiryActivity.this, R.layout.product_list_spinner_item, list) {
        };

        switch (j) {

            case 1:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                category_list.setAdapter(spinnerArrayAdapter);
                pro = Products.find(Products.class, "product_name = ?", enq.get(0).productName);
                String cat_id = String.valueOf(pro.get(0).productCategory);
                List<Category> category = Category.find(Category.class, "category_id = ?", cat_id);
                int spinnerPosition = spinnerArrayAdapter.getPosition(category.get(0).getCategoryName());
                category_list.setSelection(spinnerPosition);


                break;
            case 4:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                title.setAdapter(spinnerArrayAdapter);
                break;

            case 5:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                purchase_mode.setAdapter(spinnerArrayAdapter);
                break;


            default:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                sourceofInfo_list.setAdapter(spinnerArrayAdapter);
                break;


        }
    }
    //end region


    public void setValues() {


        etCustomerName.setText(enq.get(0).customerName);
        etCustomerAddress.setText(enq.get(0).customerAddress);
        etContactPersonName.setText(enq.get(0).contactPerson);

        if (enq.get(0).age != 0)
            etAge.setText(String.valueOf(enq.get(0).age));
        etOccupation.setText(enq.get(0).occupation);

        if (enq.get(0).phone.matches("0"))
            etPhone.setText("");

        etMobileNumber.setText(enq.get(0).mobileNumber);
        etAltMobileNumber.setText(enq.get(0).altMobileNumber);
        etContactPersonAddress.setText(enq.get(0).contactPersonAddress);
        etContactPersonEmail.setText(enq.get(0).contactPersonEmail);
        etLocationOfEnquiry.setText(enq.get(0).locationofEnquiry);

        if (enq.get(0).rate != 0)
            etRate.setText(String.valueOf(enq.get(0).rate));


        etNote.setText(enq.get(0).note);
        etBankName.setText(enq.get(0).bankName);


        int lSource = enq.get(0).leadSource;

        switch (lSource) {

            case (1):
                setSpinner(2);
                leadSource = 1;
                btnWalkIn.setSelected(true);
                btnWalkIn.setPressed(false);
                btnWalkIn.setTextColor(getResources().getColor(R.color.white));
                temp = btnWalkIn;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (2):
                setSpinner(2);
                leadSource = 2;
                btnTeleIn.setSelected(true);
                btnTeleIn.setPressed(false);
                btnTeleIn.setTextColor(getResources().getColor(R.color.white));
                temp = btnTeleIn;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (3):
                setSpinner(3);
                leadSource = 3;
                btnFieldActivity.setSelected(true);
                btnFieldActivity.setPressed(false);
                btnFieldActivity.setTextColor(getResources().getColor(R.color.white));
                temp = btnFieldActivity;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                tvSourceOfInfo.setText("Type Of ActivityLog");
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (4):
                leadSource = 4;
                btnReferral.setSelected(true);
                btnReferral.setPressed(false);
                btnReferral.setTextColor(getResources().getColor(R.color.white));
                temp = btnReferral;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);
                break;
            case (5):
                leadSource = 5;
                btnAgent.setSelected(true);
                btnAgent.setPressed(false);
                btnAgent.setTextColor(getResources().getColor(R.color.white));
                temp = btnAgent;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);
                break;
            case (6):
                setSpinner(2);
                leadSource = 6;
                btnAdvertisement.setSelected(true);
                btnAdvertisement.setPressed(false);
                btnAdvertisement.setTextColor(getResources().getColor(R.color.white));
                temp = btnAdvertisement;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                tvSourceOfInfo.setText("Source Of Information");
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (7):
                leadSource = 7;
                btnOthers.setSelected(true);
                btnOthers.setPressed(false);
                btnOthers.setTextColor(getResources().getColor(R.color.white));
                temp = btnOthers;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);

                break;


        }


        switch (enq.get(0).titleName) {
            case 1:
                title.setSelection(0);
                break;
            case 2:
                title.setSelection(1);
                break;
            case 3:
                title.setSelection(2);
                break;
            case 4:
                title.setSelection(3);
                break;


        }

        switch (enq.get(0).firstTime) {

            case 1:
                firsttimeradioGroup.check(R.id.yes1);
                break;
            case 2:
                firsttimeradioGroup.check(R.id.no1);
                break;

        }
        switch (enq.get(0).interestedInComp) {

            case 1:
                interestedradioGroup.check(R.id.yes2);
                break;

            case 2:
                interestedradioGroup.check(R.id.no2);
                break;
            case 3:
                interestedradioGroup.check(R.id.dontKnow);
                break;
        }


        switch (enq.get(0).status) {

            case 7:
                leadstatusradioGroup.check(R.id.hot);
                break;
            case 15:
                leadstatusradioGroup.check(R.id.warm);
                break;
            case 25:
                leadstatusradioGroup.check(R.id.cold);
                break;
        }


        purchase_mode.setSelection(enq.get(0).purchaseMode - 1);


        dateofEnquiry.setText(dc.getFormattedDate("MM/dd/yyyy", enq.get(0).dateofEnquiry));

        nextFollowUpDate.setText(dc.getFormattedDate("MM/dd/yyyy", enq.get(0).nextFollowUpDate));


        if (leadSource == 1 || leadSource == 2 || leadSource == 3 || leadSource == 6) {
            int source = enq.get(0).sourceofInfo;
            Subsource sub = Subsource.findById(Subsource.class, source);

            for (int i = 0; i < sourceofInfo_list.getAdapter().getCount(); i++) {

                if (sourceofInfo_list.getAdapter().getItem(i).toString().matches(sub.name)) {
                    sourceofInfo_list.setSelection(i);
                }

            }
        }


    }


    public void setEnquiryDate(View v) {


        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(
                EditEnquiryActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);

                cdate = calendar.getTimeInMillis();

                dateofEnquiry.setText(dc.getFormattedDate("MM/dd/yyyy", cdate));

            }
        }, mYear, mMonth, mDay);

        mDatePicker.setTitle("Select Notification Date");
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    public void setFollowUpDate(View v) {


        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(
                EditEnquiryActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);

                ndate = calendar.getTimeInMillis();

                nextFollowUpDate.setText(dc.getFormattedDate("MM/dd/yyyy", ndate));


            }
        }, mYear, mMonth, mDay);


        Calendar cal = Calendar.getInstance();


        int selectedId = leadstatusradioGroup.getCheckedRadioButtonId();
        if (selectedId == hot.getId()) {
            cal.add(Calendar.DAY_OF_MONTH, 7);
            mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

        } else if (selectedId == warm.getId()) {
            cal.add(Calendar.DAY_OF_MONTH, 15);
            mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
        } else if (selectedId == cold.getId()) {
            cal.add(Calendar.DAY_OF_MONTH, 25);
            mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

        }

        mDatePicker.setTitle("Select FollowUp Date");
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();

        mDatePicker.show();


    }


    /**
     * This method is called for checking internet connection
     */
    public boolean connectionChecker() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }


    public void submitEnquiry(View v) {

        if (!TextUtils.isEmpty(etCustomerName.getText().toString()) && !TextUtils.isEmpty(etContactPersonName.getText().toString())
                && !TextUtils.isEmpty(etMobileNumber.getText().toString())) {
            if (etMobileNumber.getText().toString().length() == 10) {

                if (TextUtils.isEmpty(etRate.getText().toString()) ||
                        TextUtils.isEmpty(etContactPersonAddress.getText().toString())
                        || TextUtils.isEmpty(etLocationOfEnquiry.getText().toString())) {
                    reg = 0;
                    Toast.makeText(mContext, "Please fill all fields with * for registering", Toast.LENGTH_LONG).show();

                } else {
                    reg = 1;
                    getAllValues();
                }

            } else {
                Toast.makeText(mContext, "Invalid Mobile Number", Toast.LENGTH_SHORT).show();
            }
        } else {
            etCustomerName.setError("Required");
            etContactPersonName.setError("Required");
            etMobileNumber.setError("Required");
            scrollView.scrollTo(0, 300);
            Toast.makeText(mContext, "Please Fill All Required Fields*", Toast.LENGTH_LONG).show();
        }


    }


    public void getAllValues() {

        if (leadSource == 1 || leadSource == 2 || leadSource == 3 || leadSource == 6) {
            sub = Subsource.find(Subsource.class, "name = ?", sourceofInfo_list.getSelectedItem().toString());
            subSource = (int) (long) sub.get(0).getId();
        }


        //Getting All values
        customerName = etCustomerName.getText().toString();
        customerAddress = etCustomerAddress.getText().toString();

        titleName = title.getSelectedItemPosition() + 1;

        if (etContactPersonName.getText().toString().matches(NULL)) {
            titleName = 0;
            contactPerson = NULL;
        } else {
            contactPerson = etContactPersonName.getText().toString();
        }


        if (etAge.getText().toString().matches(NULL))
            age = 0;
        else
            age = Integer.valueOf(etAge.getText().toString());

        occupation = etOccupation.getText().toString();

        if (etPhone.getText().toString().matches(NULL))
            phone = "0";
        else
            phone =etPhone.getText().toString();

        mobileNumber = etMobileNumber.getText().toString();
        altMobileNumber = etAltMobileNumber.getText().toString();
        contactPersonAddress = etContactPersonAddress.getText().toString();
        contactPersonEmail = etContactPersonEmail.getText().toString();

        purchaseMode = purchase_mode.getSelectedItemPosition() + 1;

        int firstTimeId = firsttimeradioGroup.getCheckedRadioButtonId();
        if (firstTimeId == yes1.getId()) {
            firstTime = 1;
        } else if (firstTimeId == no1.getId()) {
            firstTime = 2;
        }

        int interestedId = interestedradioGroup.getCheckedRadioButtonId();
        if (interestedId == yes2.getId()) {
            interested = 1;
        } else if (interestedId == no2.getId()) {
            interested = 2;
        } else if (interestedId == dontKnow.getId()) {
            interested = 3;
        }

        int statusId = leadstatusradioGroup.getCheckedRadioButtonId();
        if (statusId == hot.getId()) {
            status = 7;
        } else if (statusId == warm.getId()) {
            status = 15;
        } else if (statusId == cold.getId()) {
            status = 25;
        }


        locationOfEnquiry = etLocationOfEnquiry.getText().toString();
        dateOfEnquiry = dc.getMilli(dateofEnquiry.getText().toString());


        nextFollowUp = dc.getMilli(nextFollowUpDate.getText().toString());


        if (etRate.getText().toString().matches(NULL))
            rate = 0;
        else
            rate = Long.valueOf(etRate.getText().toString());


        enquiryNote = etNote.getText().toString();

        bankName = etBankName.getText().toString();


        if (connectionChecker()) {

            progressDialog = new ProgressDialog(EditEnquiryActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setMessage("Updating data.Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.setTitle(null);
            progressDialog.show();

            //Invoking Function to send Notification data to server
            sendEnquiryDetails();

        } else {

            Toast.makeText(mContext, "You need Internet For Editing Notification", Toast.LENGTH_SHORT).show();
            finish();
        }


    }


    //Function for sending added inquiry to server in online mode

    public void sendEnquiryDetails() {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URLs.URL_UPDATE_ENQUIRY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Saving enquiry locally

                        final long id = enq.get(0).getId();
                        Offenquiry oenquiry = Offenquiry.findById(Offenquiry.class, (int) id);

                        oenquiry.productName = prodName;
                        oenquiry.leadSource = leadSource;
                        oenquiry.sourceofInfo = subSource;
                        oenquiry.customerName = customerName;
                        oenquiry.customerAddress = customerAddress;
                        oenquiry.titleName = titleName;
                        oenquiry.contactPerson = contactPerson;
                        oenquiry.age = age;
                        oenquiry.occupation = occupation;
                        oenquiry.phone = phone;
                        oenquiry.mobileNumber = mobileNumber;
                        oenquiry.altMobileNumber = altMobileNumber;
                        oenquiry.contactPersonAddress = contactPersonAddress;
                        oenquiry.contactPersonEmail = contactPersonEmail;
                        oenquiry.purchaseMode = purchaseMode;
                        oenquiry.firstTime = firstTime;
                        oenquiry.interestedInComp = interested;
                        oenquiry.status = status;
                        oenquiry.locationofEnquiry = locationOfEnquiry;
                        oenquiry.dateofEnquiry = dateOfEnquiry;
                        oenquiry.nextFollowUpDate = nextFollowUp;
                        oenquiry.rate = rate;
                        oenquiry.reg = reg;
                        oenquiry.note = enquiryNote;
                        oenquiry.bankName = bankName;
                        oenquiry.save();

                        progressDialog.dismiss();
                        if (cbBrochure.isChecked()) {
                            AlertDialog dialog = new AlertDialog.Builder(EditEnquiryActivity.this)
                                    .setTitle("Edited")
                                    .setIcon(R.drawable.ic_info)
                                    .setMessage("The Enquiry has been successfully edited.Press Continue for quoation form")
                                    .setPositiveButton("Continue",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    Intent i = new Intent(EditEnquiryActivity.this, QuotationFormActivity.class);
                                                    i.putExtra("enquiry_id", String.valueOf(eId));
                                                    i.putExtra("product_id", String.valueOf(productID));
                                                    i.putExtra("rate", String.valueOf(rate));
                                                    startActivity(i);
                                                    finish();
                                                }


                                            }).setNegativeButton("Cancel",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            }).create();
                            dialog.show();


                        } else {
                            AlertDialog dialog = new AlertDialog.Builder(EditEnquiryActivity.this)
                                    .setTitle("Edited")
                                    .setIcon(R.drawable.ic_info)
                                    .setMessage("The Enquiry has been successfully edited")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent myIntent = new Intent(mContext, EnquiryDetailsActivity.class);
                                                    ActivityOptions options =
                                                            ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                                                    myIntent.putExtra("enquiry_id", Long.valueOf(eId));
                                                    mContext.startActivity(myIntent, options.toBundle());
                                                    finish();
                                                }


                                            }).create();
                            dialog.show();

                        }


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Log.e(TAG, "Server Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //This indicates that the reuest has either time out or there is no connection
                    Toast.makeText(getApplicationContext(), "Timeout Error ", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    //Error indicating that there was an Authentication Failure while performing the request
                    Toast.makeText(getApplicationContext(), "Authentication Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    //Indicates that the server responded with a error response
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    //Indicates that there was network error while performing the request
                    Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    // Indicates that the server response could not be parsed
                    Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sharedPreferences = getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                String id = String.valueOf(sharedPreferences.getInt(KEY_ID, 0));
                params.put("product_id", String.valueOf(productID));
                params.put("source_of_enquiry", String.valueOf(leadSource));
                params.put("sub_source", String.valueOf(subSource));
                params.put("customer_name", customerName);
                params.put("customer_address", customerAddress);
                params.put("contact_title", String.valueOf(titleName));
                params.put("contact_person", contactPerson);
                params.put("contact_age", String.valueOf(age));
                params.put("contact_occupation", occupation);
                params.put("contact_phone", phone);
                params.put("contact_number", mobileNumber);
                params.put("alt_contact_number", altMobileNumber);
                params.put("contact_address", contactPersonAddress);
                params.put("contact_email", contactPersonEmail);
                params.put("purchase_mode", String.valueOf(purchaseMode));
                params.put("first_time", String.valueOf(firstTime));
                params.put("competition_interest", String.valueOf(interested));
                params.put("status", String.valueOf(status));
                params.put("location_of_enquiry", locationOfEnquiry);
                params.put("date_of_enquiry", dateofEnquiry.getText().toString());
                params.put("next_followup_date", nextFollowUpDate.getText().toString());
                params.put("rate", String.valueOf(rate));
                params.put("is_reg", String.valueOf(reg));
                params.put("bank_name", bankName);
                params.put("enquiry_note", enquiryNote);
                params.put("current_date", dc.getCurrentDate());
                params.put("enquiry_id", eId);
                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


}
