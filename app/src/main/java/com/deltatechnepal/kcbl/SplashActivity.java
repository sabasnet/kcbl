package com.deltatechnepal.kcbl;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.helper.VolleySingleton;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Products;
import com.orm.SugarTransactionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SplashActivity extends Activity {
    private RelativeLayout layoutMain;
    private long oldsize = 0;
    Products productList;
    private String TAG = SplashActivity.class.getSimpleName();
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_ID = "keyid";
    private static final String KEY_USERNAME = "keyusername";
    String formattedDate;
    String URL;
    Animator animator;
    Context mContext;


    private static SharedPreManager mInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);

        }
        formattedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        System.out.println("Formatted time => " + formattedDate);
        setContentView(R.layout.activity_splash);
        layoutMain = findViewById(R.id.layoutMain);
        layoutMain.post(new Runnable() {
            @Override
            public void run() {
                revealAnimation();
            }
        });


    }

    public void revealAnimation() {


        View myView = findViewById(R.id.logoLayout);

        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight()) / 2;
        int cy = myView.getBottom();

        // get the final radius for the clipping circle

        float finalRadius = (float) (Math.hypot(myView.getWidth(), myView.getHeight()));


        animator =
                io.codetail.animation.ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);


        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(1500);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
               /* Intent i = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(i);
                finish();*/
                boolean status = ConnectionChecker.getInstance(getApplicationContext()).Check();

                if (!status && !SharedPreManager.getInstance(getApplicationContext()).isLoggedIn()) {
                    Toast.makeText(mContext, "No Internet Connection.please Connect to a Network", Toast.LENGTH_LONG).show();
                } else {
                    if (Products.count(Products.class) == 0) {
                        URL = URLs.URL_PRODUCTS;
                        fetchData();
                    }
                    if (status) {
                        URL = URLs.URL_FETCH_COUNTERS;
                        fetchData();
                        exitSplash();
                    } else
                        exitSplash();


                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {


            }
        });

    }

    public void fetchData() {
        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        // looping through json and adding to movies list
                        SugarTransactionHelper.doInTransaction(new SugarTransactionHelper.Callback() {
                            @Override
                            public void manipulateInTransaction() {
                                try {

                                    JSONObject obj = new JSONObject(response);


                                    Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                                    if (Products.count(Products.class) == 0) {
                                        JSONArray productsArray = obj.getJSONArray("products");
                                        JSONArray categoriesArray = obj.getJSONArray("categories");
                                        for (int i = 0; i < productsArray.length(); i++) {

                                            JSONObject product = productsArray.getJSONObject(i);

                                            String productName = product.getString("name");
                                            int defaultPrice = product.getInt("default_price");
                                            long productId = product.getLong("id");
                                            String imageName = product.getString("images");
                                            int category = product.getInt("category_id");

                                            Products productList = new Products(productId, productName, defaultPrice, imageName, category);
                                            productList.save();


                                        }
                                        for (int i = 0; i < categoriesArray.length(); i++) {

                                            JSONObject categories = categoriesArray.getJSONObject(i);

                                            int categoryId = categories.getInt("id");
                                            String categoryName = categories.getString("name");


                                            Category category = new Category(categoryId, categoryName);
                                            category.save();


                                        }
                                    } else {
                                        if (obj.getBoolean("status")) {
                                            JSONObject counter = obj.getJSONObject("counters");

                                            int assigned = counter.getInt("enq_assigned");
                                            int pending = counter.getInt("enq_pending");
                                            int completed = counter.getInt("enq_completed");


                                            Counter count = Counter.findById(Counter.class, 1);
                                            count.assigned = assigned;
                                            count.pending = pending;
                                            count.completed = completed;
                                            count.save();
                                        }
                                    }


                                    // updating offset value to highest value
                                       /* if (rank >= offSet)
                                            offSet = rank;*/

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }
                            }
                        });


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sharedPreferences = getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                String id = String.valueOf(sharedPreferences.getInt(KEY_ID, 0));
                params.put("id", id);
                params.put("current_date", formattedDate);


                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);

    }

    public void exitSplash() {

        Intent i = new Intent(getBaseContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


}
