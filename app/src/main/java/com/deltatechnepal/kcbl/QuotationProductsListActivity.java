package com.deltatechnepal.kcbl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.adapter.QuotationListAdapter;
import com.deltatechnepal.adapter.QuotationProductsListAdapter;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.model.Prolist;
import com.deltatechnepal.orm.Quotation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuotationProductsListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;

    private RecyclerView.LayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvProducts;
    private QuotationProductsListAdapter adapteroff;
    private String products_list;
    private List<Prolist> productsList = new ArrayList<>();


    private List<Quotation> quotationList = new ArrayList<>();


    boolean visible, status;
    Animation anim;
    String number;

    LinearLayout placeholder;
    DateConversion dc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.quotation_product_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent intent=getIntent();
        products_list=intent.getStringExtra("products_list");
        Gson gson= new Gson();
        productsList= gson.fromJson(products_list,new TypeToken<List<Prolist>>(){}.getType());


        rvProducts = findViewById(R.id.rvProducts);
        linearLayoutManager =
                new LinearLayoutManager(QuotationProductsListActivity.this);
        rvProducts.setLayoutManager(linearLayoutManager);
        rvProducts.setHasFixedSize(true);
        rvProducts.setNestedScrollingEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);


        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        fetchProductsList();
                                    }

                                }
        );
    }


    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setRefreshing(false);

    }

    private void fetchProductsList() {

        adapteroff = new QuotationProductsListAdapter(QuotationProductsListActivity.this, productsList);
        rvProducts.setAdapter(adapteroff);

    }










    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
