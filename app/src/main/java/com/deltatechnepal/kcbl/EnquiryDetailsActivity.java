package com.deltatechnepal.kcbl;

import android.Manifest;
import android.animation.Animator;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.CustomToast;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.orm.Subsource;
import com.transitionseverywhere.TransitionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnquiryDetailsActivity extends AppCompatActivity {
    Context mContext;
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_USERNAME = "keyusername";
    private String TAG = EnquiryDetailsActivity.class.getSimpleName();
    TextView customerName, customerAddress, contactPerson, contactAge, contactOccupation, contactPhone,
            contactNumber, contactAltnumber, contactAddress, contactEmail, purchaseMode, firstTime, interestedComp, status,
            locationOfEnquiry, dateOfEnquiry, productName, nextFollowUp, sourceOfEnquiry, rate, enquiryNote;
    Toolbar toolbar;
    NestedScrollView nestedScrollView;
    FloatingActionButton fabOptions, fabReverse, fabEdit, fabSend, fabTransfer;
    boolean visible;
    List<Offenquiry> enq;
    ConstraintLayout parentLayout;
    Animator animator;
    LinearLayout dimmer;
    private ConstraintSet fabClose = new ConstraintSet();
    private ConstraintSet fabOpen = new ConstraintSet();
    private ConstraintLayout mConstraintLayout;
    private int mode;
    String dirpath;
    String eId, source;
    DateConversion dc;
    String semi = ": ", dot = ".", title, purchase;
    Spinner smanList;
    ProgressDialog progressDialog;
    List<String> salesman = new ArrayList<>();
    boolean check,trans_enquiry;
    String number, product_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_inquiry_details);
        mConstraintLayout = findViewById(R.id.parentLayout);
        fabClose.clone(mConstraintLayout);
        fabOpen.clone(this, R.layout.activity_enquiry_details_fab);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dc = new DateConversion();

        dimmer = findViewById(R.id.dimmmer);

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle(null); // Setting Title
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        fabOptions = findViewById(R.id.fabOptions);
        fabReverse = findViewById(R.id.fabOptionsReverse);
        fabEdit = findViewById(R.id.fabEdit);
        fabSend = findViewById(R.id.fabSendQuotation);
        fabTransfer = findViewById(R.id.fabTransferEnquiry);


        nestedScrollView = findViewById(R.id.scrollView);


        customerName = findViewById(R.id.tvcustomer_name_value);
        customerAddress = findViewById(R.id.tvcustomer_address_value);
        contactPerson = findViewById(R.id.tvcontact_person_value);
        contactAge = findViewById(R.id.tvcontact_age_value);
        contactOccupation = findViewById(R.id.tvcontact_occupation_value);
        contactPhone = findViewById(R.id.tvcontact_phone_value);
        contactAltnumber = findViewById(R.id.tvcontact_alt_mobile_value);
        contactAddress = findViewById(R.id.tvcontact_person_address_value);
        contactEmail = findViewById(R.id.tvcontact_person_mail_value);
        purchaseMode = findViewById(R.id.tvpurchase_mode_value);
        contactNumber = findViewById(R.id.tvcontact_number_value);
        firstTime = findViewById(R.id.tvfirst_time_value);
        interestedComp = findViewById(R.id.tvinterested_in_comp);
        status = findViewById(R.id.tvstatus_value);
        locationOfEnquiry = findViewById(R.id.tvlocation_of_enquiry_value);
        dateOfEnquiry = findViewById(R.id.tvdate_of_enquiry_value);
        productName = findViewById(R.id.tvproduct_name_value);
        nextFollowUp = findViewById(R.id.tvnext_follow_up_date_value);
        sourceOfEnquiry = findViewById(R.id.tvsource_of_enquiry_value);
        rate = findViewById(R.id.tvrate_value);
        enquiryNote = findViewById(R.id.tvnote_value);


        Intent mIntent = getIntent();
        long id = mIntent.getLongExtra("enquiry_id", 0);
        trans_enquiry=mIntent.getBooleanExtra("trans_enquiry",false);
        eId = String.valueOf(id);


        if(trans_enquiry){
            fabOptions.setVisibility(View.GONE);
            String response=mIntent.getStringExtra("response");
             getAllValues(response);

        }
        else {
            enq = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);
            List<Products> pro = Products.find(Products.class, "product_name = ?", enq.get(0).productName);
            if (pro.size() == 0) {
                Toast.makeText(mContext, "The Product associated with this enquiry is deleted.So you can`t perform any action here", Toast.LENGTH_LONG).show();

                fabOptions.setVisibility(View.GONE);
            }
            else{
                product_id = String.valueOf(pro.get(0).productId);
            }
            setValues();
        }




        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(mContext, EditEnquiryActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                myIntent.putExtra("enquiry_id", eId);
                mContext.startActivity(myIntent, options.toBundle());
            }
        });


        fabTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Progress Dialog Style Spinner
                check = ConnectionChecker.getInstance(mContext).Check();
                if (check) {
                    progressDialog.setMessage("Working....");
                    progressDialog.show();
                    salesman.clear();
                    Transfer(0, URLs.URL_FETCH_SALESMAN_LIST);
                } else {
                    Toast.makeText(mContext, "You need internet for transferring enquiry", Toast.LENGTH_LONG).show();
                }


            }
        });

        fabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ConnectionChecker.getInstance(mContext).Check()) {
                    if (TextUtils.isEmpty(enq.get(0).contactPersonEmail)) {
                        Toast.makeText(mContext, "Please Fill Email", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(enq.get(0).productName)) {
                        Toast.makeText(mContext, "No Product Selected", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(enq.get(0).customerName)) {
                        Toast.makeText(mContext, "Please Fill Customer Name", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(enq.get(0).customerAddress)) {
                        Toast.makeText(mContext, "PLease Fill Customer Address", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (enq.get(0).rate == 0) {
                        Toast.makeText(mContext, "PLease Fill the Rate", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Intent i = new Intent(EnquiryDetailsActivity.this, QuotationFormActivity.class);
                    i.putExtra("enquiry_id", eId);
                    i.putExtra("product_id", product_id);
                    i.putExtra("rate", rate.getText().toString());
                    startActivity(i);
                } else {
                    Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                if (scrollY > oldScrollY) {
                    Log.i(TAG, "Scroll DOWN");
                    if (fabOptions.getVisibility() == View.VISIBLE) {
                        fabOptions.startAnimation(
                                AnimationUtils.loadAnimation(getApplication(), R.anim.zoom_out_animation));
                        fabOptions.setVisibility(View.INVISIBLE);
                    }
                }
                if (scrollY < oldScrollY) {
                    Log.i(TAG, "Scroll UP");
                }

                if (scrollY == 0) {
                    Log.i(TAG, "TOP SCROLL");
                    if (fabOptions.getVisibility() == View.INVISIBLE) {
                        fabOptions.setVisibility(View.VISIBLE);
                        fabOptions.startAnimation(
                                AnimationUtils.loadAnimation(getApplication(), R.anim.zoom_in_animation));
                    }
                }

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    Log.i(TAG, "BOTTOM SCROLL");
                }
            }
        });


    }


    //Function to show/hide actions on Fab click
    public void openOptions(View v) {


        if (dimmer.getVisibility() == View.GONE) {


            fabOptions.startAnimation(
                    AnimationUtils.loadAnimation(getApplication(), R.anim.rotate_anticlockwise));
            TransitionManager.beginDelayedTransition(mConstraintLayout);
            fabOpen.applyTo(mConstraintLayout);


        } else {

            fabOptions.startAnimation(
                    AnimationUtils.loadAnimation(getApplication(), R.anim.rotate_clockwise));
            TransitionManager.beginDelayedTransition(mConstraintLayout);
            fabClose.applyTo(mConstraintLayout);


        }
    }


    public void openQuotationList(View v){
        if(ConnectionChecker.getInstance(mContext).Check()) {
            Intent myIntent = new Intent(mContext, QuotationListActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
            myIntent.putExtra("enquiry_id", eId);
            mContext.startActivity(myIntent, options.toBundle());
        }
        else
            Toast.makeText(mContext,"You need internet for viewing the quotation list",Toast.LENGTH_SHORT).show();
    }


    public void setValues() {


        int lead = enq.get(0).leadSource;

        switch (lead) {
            case 1:
                source = "Walk-In";
                break;
            case 2:
                source = "Tele-In ";
                break;
            case 3:
                source = "Field ActivityLog";
                break;
            case 4:
                source = "Referral";
                break;
            case 5:
                source = "Agent";
                break;
            case 6:
                source = "Advertisement";
                break;
            case 7:
                source = "Others ";
                break;
            default:
                source = "";
                break;

        }


        if (lead == 1 || lead == 2 || lead == 3 || lead == 6) {
            Subsource sub = Subsource.findById(Subsource.class, enq.get(0).sourceofInfo);
            sourceOfEnquiry.setText(source + " : " + sub.name);
        }

        switch (enq.get(0).titleName) {
            case 1:
                title = "Mr. ";
                break;
            case 2:
                title = "Mrs. ";
                break;
            case 3:
                title = "Miss. ";
                break;
            case 4:
                title = "Ms. ";
                break;

        }


        customerName.setText(enq.get(0).customerName);
        customerAddress.setText(enq.get(0).customerAddress);
        contactPerson.setText(title + enq.get(0).contactPerson);

        if (String.valueOf(enq.get(0).age).matches("0"))
            contactAge.setText("");
        else
            contactAge.setText(String.valueOf(enq.get(0).age));

        contactOccupation.setText(enq.get(0).occupation);

        if (String.valueOf(enq.get(0).phone).matches("0"))
            contactPhone.setText("");
        else
            contactPhone.setText(enq.get(0).phone);

        contactAddress.setText(enq.get(0).contactPersonAddress);

        contactNumber.setText(enq.get(0).mobileNumber);
        contactAltnumber.setText(enq.get(0).altMobileNumber);
        contactEmail.setText(enq.get(0).contactPersonEmail);

        switch (enq.get(0).purchaseMode) {
            case 1:
                purchase = "Cash";
                break;
            case 2:
                purchase = "Finance";
                break;
            case 3:
                purchase = "Exchange";
                break;
            case 4:
                purchase = "Credit";
                break;

        }

        purchaseMode.setText(purchase);


        switch (enq.get(0).firstTime) {
            case 1:
                firstTime.setText("Yes");
                break;

            case 2:
                firstTime.setText("No");
                break;
        }

        switch (enq.get(0).interestedInComp) {

            case 1:
                interestedComp.setText("Yes");
                break;

            case 2:
                interestedComp.setText("No");
                break;

            case 3:
                interestedComp.setText("Don`t Know");
                break;

        }


        int status = enq.get(0).status;
        switch (status)

        {
            case 7:
                setEnquiryStatus("Hot", R.drawable.rounded_red_background, R.color.flatred);
                setStatusBarColor(R.color.flatreddark);
                break;

            case 15:
                setEnquiryStatus("Warm", R.drawable.rounded_orange_background, R.color.flatorange);
                setStatusBarColor(R.color.flatorangedark);
                break;

            case 25:
                setEnquiryStatus("Cold", R.drawable.rounded_blue_background, R.color.flatblue);
                setStatusBarColor(R.color.flatblue);
                break;

        }


        locationOfEnquiry.setText(enq.get(0).locationofEnquiry);


        dateOfEnquiry.setText(dc.getFormattedDate("MM/dd/yyyy", enq.get(0).dateofEnquiry));

        productName.setText(enq.get(0).productName);

        nextFollowUp.setText(dc.getFormattedDate("MM/dd/yyyy", enq.get(0).nextFollowUpDate));

        rate.setText(String.valueOf(enq.get(0).rate));

        enquiryNote.setText(enq.get(0).note);

    }

    //Function to change ActionBar,FAB and Notification Status Color
    public void setEnquiryStatus(String enquiryStatus, int background, int color) {
        status.setText(enquiryStatus);
        status.setBackgroundResource(background);
        fabOptions.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        fabReverse.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        fabEdit.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        fabSend.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        fabTransfer.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(color)));
    }






    public void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT < 21)
            return;
        Window window = this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getApplication(), color));
    }

    public void transferEnquiry() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_dialog_transfer, null);
        dialogBuilder.setView(dialogView);
        TextView header = dialogView.findViewById(R.id.dialogTitle);

        switch (status.getText().toString()) {

            case "Hot":
                header.setBackgroundColor(getResources().getColor(R.color.flatred));
                break;

            case "Warm":
                header.setBackgroundColor(getResources().getColor(R.color.flatorange));
                break;

            case "Cold":
                header.setBackgroundColor(getResources().getColor(R.color.flatblue));
                break;


        }


        smanList = dialogView.findViewById(R.id.salesmanList);
        TextView customerName = dialogView.findViewById(R.id.tvName);
        TextView customerNumber = dialogView.findViewById(R.id.tvNumber);
        TextView productName = dialogView.findViewById(R.id.tvProduct);


        customerName.setText(enq.get(0).customerName);
        customerNumber.setText(enq.get(0).mobileNumber);
        productName.setText(enq.get(0).productName);


        setSpinner();


        dialogBuilder.setMessage(null);

        dialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);

                if (check)
                    Transfer(1, URLs.URL_TRANSFER_ENQUIRY);
                //do something with edt.getText().toString();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white_background);
        b.show();


    }


    public void setSpinner() {


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.reasons_list_spinner_item, salesman);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.reasons_list_spinner_item);
        smanList.setAdapter(spinnerArrayAdapter);


    }

    public void Call(View v) {

        switch (v.getId()) {


            case R.id.tvcontact_number_value:
                number = contactNumber.getText().toString();
                break;
            case R.id.tvcontact_phone_value:
                number = enq.get(0).phone;
                break;
            case R.id.tvcontact_alt_mobile_value:
                number = contactAltnumber.getText().toString();
                break;


        }

        int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        if (callPermission == 0) {

            if (!TextUtils.isEmpty(number)) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + number));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "No Number to Call", Toast.LENGTH_SHORT).show();

            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + getApplication().getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            getApplication().startActivity(i);
        }


    }


    public void sendEmail(View v) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", contactEmail.getText().toString(), null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hello");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));


    }


    public void Transfer(final int check, String url) {


        // Volley's json array request object

        final StringRequest req = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // looping through json and adding to movies list

                        try {


                            JSONObject obj = new JSONObject(response);
                            if (check == 0) {


                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                if (obj.getBoolean("status")) {
                                    JSONArray enquiriesArray = obj.getJSONArray("data");
                                    for (int i = 0; i < enquiriesArray.length(); i++) {

                                        JSONObject enquiry = enquiriesArray.getJSONObject(i);

                                        String Name = enquiry.getString("name");
                                        salesman.add(Name);


                                    }
                                }

                                // Initializing an ArrayAdapter

                                transferEnquiry();


                            } else {
                                if (obj.getBoolean("status")) {
                                    progressDialog.dismiss();
                                    List<Offenquiry> offList = Offenquiry.find(Offenquiry.class, "enquiry_id = ? ", eId);
                                    Offenquiry off = offList.get(0);
                                    off.delete();
                                    List<Notification> notif = Notification.find(Notification.class, "action_id = ?", eId);
                                    if (notif.size() > 0) {
                                        Notification not = notif.get(0);
                                        not.delete();
                                    }

                                    ActivityLog log = new ActivityLog(2, obj.getLong("time"), Integer.valueOf(eId));
                                    log.save();
                                    AlertDialog dialog = new AlertDialog.Builder(EnquiryDetailsActivity.this)
                                            .setTitle("Transferred")
                                            .setIcon(R.drawable.ic_info)
                                            .setMessage("The Enquiry has been successfully transferred")
                                            .setPositiveButton("Ok",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            finish();
                                                        }
                                                    }).create();
                                    dialog.show();


                                }

                            }

                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                            progressDialog.dismiss();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                if (check != 0) {
                    SharedPreferences sharedPreferences = getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    String username = String.valueOf(sharedPreferences.getString(KEY_USERNAME, ""));
                    params.put("customer_name", enq.get(0).customerName);
                    params.put("product_name", enq.get(0).productName);
                    params.put("transferred_by", username);
                    params.put("enquiry_id", eId);
                    params.put("salesman_name", smanList.getSelectedItem().toString());
                } else {
                    params.put("exception_salesman", String.valueOf(SharedPreManager.getInstance(mContext).getUser().getId()));

                }


                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);

    }

    public void onResume() {
        super.onResume();
        if(!trans_enquiry) {
            enq = Offenquiry.find(Offenquiry.class, "enquiry_id = ?", eId);
            setValues();
        }
    }




    public void getAllValues(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray enquiry = jsonObject.getJSONArray("data");
            JSONObject receivedData = enquiry.getJSONObject(0);
            String productId = receivedData.getString("product_id");
            List<Products> products = Products.find(Products.class, "product_id = ?", productId);
            String productName = products.get(0).productName;
            long enquiryId = receivedData.getLong("id");
            int sourceofEnquiry = receivedData.getInt("source_of_enquiry");
            int subSource = receivedData.getInt("sub_source");
            String customerName = receivedData.getString("customer_name");
            String customerAddress = receivedData.getString("customer_address");
            int titleName = receivedData.getInt("contact_title");
            String contactPerson = receivedData.getString("contact_person");
            int contactAge = receivedData.getInt("contact_age");
            String contactOccupation = receivedData.getString("contact_occupation");
            String contactPhone = receivedData.getString("contact_phone");
            String contactNumber = receivedData.getString("contact_number");
            String altContactNumber = receivedData.getString("alt_contact_number");
            String contactAddress = receivedData.getString("contact_address");
            String contactEmail = receivedData.getString("contact_email");
            int purchaseMode = receivedData.getInt("purchase_mode");
            int firstTime = receivedData.getInt("first_time");
            int interestedComp = receivedData.getInt("competition_interest");
            int status = receivedData.getInt("status");
            String locationofEnquiry = receivedData.getString("location_of_enquiry");
            String dateofEnquiry = receivedData.getString("doe");
            String nextFollowUpDate = receivedData.getString("next_followup_date");
            long rate = receivedData.getLong("rate");
            int result = receivedData.getInt("result");
            int reason = receivedData.getInt("reason");
            int reg = receivedData.getInt("is_reg");
            String note = receivedData.getString("note");
            String bankName = receivedData.getString("bank_name");
            DateConversion dc = new DateConversion();
            Offenquiry off = new Offenquiry(enquiryId, productName, sourceofEnquiry, subSource, customerName, customerAddress, titleName,
                    contactPerson, contactAge,
                    contactOccupation, contactPhone, contactNumber, altContactNumber, contactAddress, contactEmail, purchaseMode,
                    firstTime, interestedComp,
                    status, locationofEnquiry, dc.getMilli(dateofEnquiry), dc.getMilli(nextFollowUpDate),
                    rate, result, reason, reg, note, bankName);
            enq = new ArrayList<>();
            enq.add(off);

            setValues();

        }catch (Exception e){

        }










    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


}
