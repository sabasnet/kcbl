package com.deltatechnepal.kcbl;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.adapter.CollectionFollowUpAdapter;
import com.deltatechnepal.adapter.EnquiryFollowUpAdapter;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.CollectionFollowUps;
import com.deltatechnepal.orm.EnquiryFollowUps;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class CollectionFollowUpHistoryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;
    AlertDialog b;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvEnquiry;
    private CollectionFollowUpAdapter adapter;
    private List<CollectionFollowUps> coll = new ArrayList<>();
    EditText f1, f2;
    int called = 0;
    private FloatingActionButton fab;
    TextView date;
    String number;
    long totalCAmount=0;
    TextView tvTCAmount;


    boolean visible, status;

    ViewGroup transitionsContainer;
    Animation anim;

    LinearLayout placeholder;
    DateConversion dc;
    String cId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.follow_up_history_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dc = new DateConversion();


        rvEnquiry = findViewById(R.id.rvEnquiryHistory);
        linearLayoutManager =
                new LinearLayoutManager(CollectionFollowUpHistoryActivity.this);
        rvEnquiry.setLayoutManager(linearLayoutManager);
        rvEnquiry.setHasFixedSize(true);
        rvEnquiry.setNestedScrollingEnabled(true);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(this);
        //fabTransition
        transitionsContainer = findViewById(R.id.topContainer);

        fab = findViewById(R.id.fab);
        tvTCAmount=findViewById(R.id.collectedAmount);

        Intent mIntent = getIntent();
        cId = mIntent.getStringExtra("mobile_number");


        coll = Select.from(CollectionFollowUps.class)
                .where(Condition.prop("customer_mobile").eq(cId))
                .orderBy("created_at DESC")
                .list();





        List<Collection> find = Collection.find(Collection.class, "customer_mobile = ?", cId);


        setUpModes();


        getSupportActionBar().setTitle(find.get(0).customerName);


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        placeholder = findViewById(R.id.placeholder);

        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        /*   fetchlocalEnquiries();*/
                                    }

                                }
        );
    }


    public void setUpModes() {

        if (coll.size() != 0 && coll.get(0).nextFollowUpMode == 0) {
            fab.setVisibility(View.GONE);
        } else {


            if (coll.size() != 0) {

                if (coll.get(0).nextFollowUpMode == 2)
                    fab.setImageResource(R.drawable.ic_phone);
                else
                    fab.setImageResource(R.drawable.ic_add_black_24dp);
            } else {

                fab.setImageResource(R.drawable.ic_add_black_24dp);
            }
        }


    }


    @Override
    public void onRefresh() {


        swipeRefreshLayout.setRefreshing(true);
        coll.clear();
        coll = Select.from(CollectionFollowUps.class)
                .where(Condition.prop("customer_mobile").eq(cId))
                .orderBy("created_at DESC")
                .list();
        fetchlocalEnquiries();
        swipeRefreshLayout.setRefreshing(false);
        setTotalAmount();

    }

    private void fetchlocalEnquiries() {


        adapter = new CollectionFollowUpAdapter(CollectionFollowUpHistoryActivity.this, coll);
        rvEnquiry.setAdapter(adapter);
        placeholder.clearAnimation();
        placeholder.setVisibility(View.GONE);


    }


    public void AddFollowUp(View v) {
        if (coll.size() != 0) {
            if (coll.get(0).nextFollowUpMode == 2) {
                callFollowUp();

            } else {

                Intent myIntent = new Intent(this, AddFollowUpCollectionsActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                myIntent.putExtra("customer_mobile", cId);
                myIntent.putExtra("previous_next_followup_mode",coll.get(0).nextFollowUpMode);
                myIntent.putExtra("last_followup_note", coll.get(0).followUpNote);
                mContext.startActivity(myIntent, options.toBundle());

            }
        } else {

          openFollowUpModeDialog();

        }


    }
    public void openFollowUpModeDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_followup_mode, null);
        dialogBuilder.setView(dialogView);

        Button btnCall=dialogView.findViewById(R.id.btnCall);
        Button btnVisit=dialogView.findViewById(R.id.btnVisit);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                callFollowUp();
            }
        });

        btnVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(mContext, AddFollowUpCollectionsActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                myIntent.putExtra("customer_mobile", cId);
                myIntent.putExtra("previous_next_followup_mode",1);
                mContext.startActivity(myIntent, options.toBundle());
                b.dismiss();
            }
        });




        b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);




    }


    public void callFollowUp() {

        Intent mIntent = getIntent();
        String number = mIntent.getStringExtra("mobile_number");
        int callPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        if (callPermission == 0) {

            if (!TextUtils.isEmpty(number)) {
                called = 1;
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + Long.parseLong(number)));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "No Number to Call", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Grant permission from settings", Toast.LENGTH_SHORT).show();
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + getApplication().getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            getApplication().startActivity(i);
        }


    }


    public void onResume() {

        super.onResume();
        if (called == 1) {
            Intent myIntent = new Intent(this, AddFollowUpCollectionsActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
            myIntent.putExtra("customer_mobile", cId);
            mContext.startActivity(myIntent, options.toBundle());
            called = 0;


        } else {

            onRefresh();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

   public void setTotalAmount(){
       totalCAmount=0;
       if(coll.size()!=0) {
           for (int i = 0; i < coll.size(); i++) {

               totalCAmount = totalCAmount + coll.get(i).collectedAmount;


           }
           tvTCAmount.setVisibility(View.VISIBLE);
           tvTCAmount.setText("Total Collected Amount: " + String.valueOf(totalCAmount));
       }

   }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
