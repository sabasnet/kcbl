package com.deltatechnepal.kcbl;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class EmiActivity extends AppCompatActivity {
    Context mContext;
    RecyclerView rvCollection;
    EditText etLoanAmount, etInterestRate, etTimePeriod;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_emi_calculator);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etLoanAmount = findViewById(R.id.etLoanAmount);
        etInterestRate = findViewById(R.id.etInterestRate);
        etTimePeriod = findViewById(R.id.etTimePeriod);
        tvResult = findViewById(R.id.tvResult);


    }


    public void calculateEmi(View v) {


        float principal, rate, time, emi;
        principal = Float.valueOf(etLoanAmount.getText().toString());
        rate = Float.valueOf(etInterestRate.getText().toString());
        time = Float.valueOf(etTimePeriod.getText().toString());

        emi = emi_calculator(principal, rate, time);

        tvResult.setText("Result = " + String.valueOf(emi));


    }

    static float emi_calculator(float p,
                                float r, float t) {
        float emi;

        r = r / (12 * 100); // one month interest
        t = t * 12; // one month period
        emi = (p * r * (float) Math.pow(1 + r, t))
                / (float) (Math.pow(1 + r, t) - 1);

        return (emi);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
