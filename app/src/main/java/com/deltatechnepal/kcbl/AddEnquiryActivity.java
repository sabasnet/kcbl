package com.deltatechnepal.kcbl;

import android.animation.LayoutTransition;
import android.app.ActivityOptions;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deltatechnepal.adapter.BrochureAdapter;
import com.deltatechnepal.helper.ColorText;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.CustomToast;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.ResyncEnquiries;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.helper.URLs;
import com.deltatechnepal.helper.VolleySingleton;
import com.deltatechnepal.orm.ActivityLog;
import com.deltatechnepal.orm.Brochure;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.orm.Subsource;
import com.deltatechnepal.orm.Uenquiry;
import com.deltatechnepal.service.BrochureFetchJob;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddEnquiryActivity extends AppCompatActivity {
    private Context mContext;
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_USERNAME = "keyusername";
    private String TAG = AddEnquiryActivity.class.getSimpleName();
    private static final String NULL = "";
    String customerName, contactPerson, customerAddress, mobileNumber, locationOfEnquiry, proName, sourceOfInfo, occupation, altMobileNumber, contactPersonAddress,
            contactPersonEmail, enquiryNote, bankName,phone;
    int leadSource = 0, purchaseMode, status = 0, firstTime = 0, interested = 0, age, titleName, subSource = 0, reg = 1, sendBro = 0, sendQuo = 0;

    long dateOfEnquiry, nextFollowUp, rate, productID;
    private static final String KEY_ID = "keyid";
    EditText etCustomerName, etContactPersonName, etMobileNumber, etLocationOfEnquiry, etCustomerAddress, etRate,
            etAge, etOccupation, etPhone, etAltMobileNumber, etContactPersonAddress, etContactPersonEmail, etNote, etBankName;
    TextView tvSourceOfInfo, tvProductName, tvMRP;
    Spinner product_list, sourceofInfo_list, title, purchase_mode, category_list;
    RadioGroup leadstatusradioGroup, firsttimeradioGroup, interestedradioGroup;
    RadioButton hot, warm, cold, yes1, yes2, no1, no2, dontKnow;
    Button dateofEnquiry, nextFollowUpDate;
    CheckBox cbBrochure;
    ConstraintLayout scrollHolder;
    List<Products> productsList;
    List<Category> categoryList;
    List<Subsource> sub = new ArrayList<>();
    long cdate, ndate;
    ProgressDialog progressDialog;
    DateConversion dc;
    ImageView product_image;
    Button btnWalkIn, btnTeleIn, btnFieldActivity, btnReferral, btnAgent, btnAdvertisement, btnOthers;
    ConstraintLayout hiddenLayout, spinnerHolder;
    ScrollView scrollView;
    Spinner smanList;
    Button temp;
    List<String> salesmanList = new ArrayList<>();
    CustomToast customToast;
    SharedPreferences sharedPreferences;
    String id, prodName, catName, temp_prod_name;
    boolean check;
    int product_id, temp_cat_id;
    Brochure bron = new Brochure();
    long productId;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_enquiry);
        mContext = this;


        //For Showing new product from notification click
        Intent mIntent = getIntent();
        productId = mIntent.getLongExtra("product_id", 0);
        if (productId != 0) {
            List<Products> findPro = Products.find(Products.class, "product_id = ?", String.valueOf(productId));
            temp_cat_id = findPro.get(0).productCategory;
            temp_prod_name = findPro.get(0).productName;
            List<Category> cat = Category.find(Category.class, "category_id = ?", String.valueOf(temp_cat_id));
            catName = cat.get(0).getCategoryName();


        }


        if (bron.isBrochureListEmpty())
            BrochureFetchJob.runJobImmediately();

        //region SettingUp Toolbar and actions
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        //endregion
        dc = new DateConversion();
        customToast = new CustomToast();

        check = ConnectionChecker.getInstance(mContext).Check();
        //region Initializing Layout Components

        product_image = findViewById(R.id.product_image);

        //EditTexts
        etCustomerName = findViewById(R.id.customer_name);


        etCustomerAddress = findViewById(R.id.customer_address);

        etContactPersonName = findViewById(R.id.contact_person);

        etAge = findViewById(R.id.age);
        etOccupation = findViewById(R.id.occupation);
        etPhone = findViewById(R.id.phone);

        etMobileNumber = findViewById(R.id.mobile_number);

        etAltMobileNumber = findViewById(R.id.alt_mobile_number);

        etContactPersonAddress = findViewById(R.id.contact_person_address);

        etContactPersonEmail = findViewById(R.id.contact_person_mail);

        etLocationOfEnquiry = findViewById(R.id.location_of_enquiry);


        etRate = findViewById(R.id.rate);

        etBankName = findViewById(R.id.bank_name);

        etNote = findViewById(R.id.enquiry_note);


        ColorText.getInstance(mContext).multiColor(etCustomerName, getString(R.string.customerName));
        ColorText.getInstance(mContext).multiColor(etContactPersonName, getString(R.string.contactName));
        ColorText.getInstance(mContext).multiColor(etMobileNumber, getString(R.string.contactMobile));


        //spinners
        category_list = findViewById(R.id.category_name);
        product_list = findViewById(R.id.product_name);
        sourceofInfo_list = findViewById(R.id.source_of_information);
        title = findViewById(R.id.title);
        purchase_mode = findViewById(R.id.purchase_mode);

        //RadioGroups
        firsttimeradioGroup = findViewById(R.id.first_time_radio_group);
        yes1 = findViewById(R.id.yes1);
        no1 = findViewById(R.id.no1);
        interestedradioGroup = findViewById(R.id.interested_radio_group);
        yes2 = findViewById(R.id.yes2);
        no2 = findViewById(R.id.no2);
        dontKnow = findViewById(R.id.dontKnow);
        leadstatusradioGroup = findViewById(R.id.status_radio_group);
        hot = findViewById(R.id.hot);
        warm = findViewById(R.id.warm);
        cold = findViewById(R.id.cold);


        //Buttons
        dateofEnquiry = findViewById(R.id.date_of_enquiry);
        nextFollowUpDate = findViewById(R.id.next_follow_up);


        //TextViews and extra
        tvProductName = findViewById(R.id.tvProductName);
        tvSourceOfInfo = findViewById(R.id.tvSourceOfInfo);
        tvMRP = findViewById(R.id.tvMRP);

        hiddenLayout = findViewById(R.id.hiddenSourceLayout);
        spinnerHolder = findViewById(R.id.spinnerHolderhidden);
        scrollView = findViewById(R.id.scrollView);
        scrollHolder = findViewById(R.id.scrollHolder);

        //Button Selectors
        btnWalkIn = findViewById(R.id.walkIn);
        btnTeleIn = findViewById(R.id.teleIn);
        btnFieldActivity = findViewById(R.id.fieldActivity);
        btnReferral = findViewById(R.id.referral);
        btnAgent = findViewById(R.id.agent);
        btnAdvertisement = findViewById(R.id.advertisement);
        btnOthers = findViewById(R.id.others);


        //CheckBox
        cbBrochure = findViewById(R.id.cbSendBrochure);


        //endregion


        //getting ID from sharedPrefs
        sharedPreferences = getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        id = String.valueOf(sharedPreferences.getInt(KEY_ID, 0));


        //region Fixing Animate Layout Changes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ((ViewGroup) findViewById(R.id.hiddenSourceLayout)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGE_APPEARING);
        }
        //endregion


        setSpinner(1);
        setSpinner(4);
        setSpinner(5);

        /*        setProductList();*/


        dateofEnquiry.setText(dc.getCurrentDate());
        nextFollowUpDate.setText(dc.getCurrentDate());

        cdate = dc.getMilli(dc.getCurrentDate());
        ndate = cdate;


        category_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                productID = 0;
                setProductList();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


        product_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                tvMRP.setText("");
                String[] pro = (product_list.getSelectedItem().toString()).split("\n");
                List<Products> product = Products.find(Products.class, "product_name = ?", pro[0]);
                productID = product.get(0).productId;
                prodName = product.get(0).productName;
                tvProductName.setText("Product Name: " + prodName);
                tvMRP.setText("Product MRP : Rs." + product.get(0).defaultPrice);
                Glide.with(mContext).load(URLs.URL_PRODUCT_IMAGES + product.get(0).imageName)
                        .apply(new RequestOptions()
                                .skipMemoryCache(true).placeholder(R.drawable.kcbl))
                        .into(product_image);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


    }


    public void openBrochureDialog(View v) {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_brochure, null);

        BottomSheetDialog dialog = new BottomSheetDialog(this, R.style.BaseBottomSheetDialog);
        dialog.setContentView(view);
        dialog.show();


        List<Brochure> brochures = Brochure.find(Brochure.class, "product_id = ?", String.valueOf(productID));
        List<String> list = new ArrayList<>();
        for (int i = 0; i < brochures.size(); i++) {
            list.add(brochures.get(i).getBrochure());
        }


        if (!bron.isBrochureListEmpty()) {
            ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);
            BrochureAdapter adapter = new BrochureAdapter(this, list);
            viewPager.setAdapter(adapter);
        }


    }


    public void setProductList() {

        String selectedCategory = category_list.getSelectedItem().toString();
        List<Category> category = Category.find(Category.class, "category_name = ?", selectedCategory);
        int cat_id = category.get(0).getCategoryId();
        productsList = Products.find(Products.class, "product_category = ?", String.valueOf(cat_id));
        List<String> list = new ArrayList<>();
        for (int i = 0; i < productsList.size(); i++) {
            list.add(productsList.get(i).productName);

        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                AddEnquiryActivity.this, R.layout.product_list_spinner_item, list) {
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
        product_list.setAdapter(spinnerArrayAdapter);


        if (productId != 0) {
            int spinnerPosition = spinnerArrayAdapter.getPosition(temp_prod_name);
            product_list.setSelection(spinnerPosition);
            productId = 0;
        }


    }


    private final void focusOnView(final EditText editText) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollBy(0, -100);
            }
        });
    }


    //region Lead Source Button Selectors Logic
    public void leadSource(View v) {
        Button button = (Button) v;

        // clear state
        if (temp != null) {
            temp.setSelected(false);
            temp.setPressed(false);
            temp.setTextColor(getResources().getColor(R.color.textGrey1));
        }


        // change state
        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(getResources().getColor(R.color.white));
        temp = button;


        switch (v.getId()) {

            case (R.id.walkIn):
                setSpinner(2);
                leadSource = 1;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.teleIn):
                setSpinner(2);
                leadSource = 2;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.fieldActivity):
                setSpinner(3);
                leadSource = 3;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                tvSourceOfInfo.setText("Type Of ActivityLog");
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.referral):
                leadSource = 4;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);

                break;
            case (R.id.agent):
                leadSource = 5;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);

                break;
            case (R.id.advertisement):
                setSpinner(2);
                leadSource = 6;
                tvSourceOfInfo.setVisibility(View.VISIBLE);
                tvSourceOfInfo.setText("Source Of Information");
                spinnerHolder.setVisibility(View.VISIBLE);
                break;
            case (R.id.others):
                leadSource = 7;
                tvSourceOfInfo.setVisibility(View.GONE);
                spinnerHolder.setVisibility(View.GONE);
                break;


        }

    }
    //endregion


    //region Setting Up All Spinners
    public void setSpinner(int j) {
        List<String> list = new ArrayList<>();
        switch (j) {

            case 1:

                categoryList = Category.listAll(Category.class);
                for (int i = 0; i < categoryList.size(); i++) {
                    list.add(categoryList.get(i).getCategoryName());
                }
                break;

            case 2:
                list.add("Newspaper");
                list.add("TVC");
                list.add("Pole Banner");
                list.add("Existing Customer");
                list.add("Hoarding");
                list.add("Internet");
                list.add("FM");
                list.add("Leaflet");
                list.add("Mikeing");
                list.add("Road Show");
                list.add("Exchange Mela");
                list.add("Website");
                list.add("Digital Marketing");
                list.add("Others");
                break;
            case 3:
                list.add("Test Drive/Demo");
                list.add("Road Show");
                list.add("Exchange Mela");
                list.add("Existing Customer");
                list.add("Leaflet");
                list.add("Mikeing");
                list.add("Website");
                list.add("Digital Marketing");
                list.add("Others");
                break;

            case 4:
                list.add("Mr");
                list.add("Mrs");
                list.add("Miss");
                list.add("Ms");
                break;

            case 5:
                list.add("Cash");
                list.add("Finance");
                list.add("Exchange");
                list.add("Credit");
                break;


        }

        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                AddEnquiryActivity.this, R.layout.product_list_spinner_item, list) {
        };

        switch (j) {

            case 1:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                category_list.setAdapter(spinnerArrayAdapter);
                if (productId != 0) {
                    int spinnerPosition = spinnerArrayAdapter.getPosition(catName);
                    category_list.setSelection(spinnerPosition);
                }
                break;
            case 4:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                title.setAdapter(spinnerArrayAdapter);
                break;

            case 5:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                purchase_mode.setAdapter(spinnerArrayAdapter);
                break;


            default:
                spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
                sourceofInfo_list.setAdapter(spinnerArrayAdapter);
                break;


        }


    }
    //endregion


    public void setEnquiryDate(View v) {


        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(
                AddEnquiryActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);

                cdate = calendar.getTimeInMillis();

                dateofEnquiry.setText(dc.getFormattedDate("MM/dd/yyyy", cdate));

            }
        }, mYear, mMonth, mDay);

        mDatePicker.setTitle("Select Notification Date");
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();
    }


    public void setFollowUpDate(View v) {


        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(
                AddEnquiryActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);

                ndate = calendar.getTimeInMillis();

                nextFollowUpDate.setText(dc.getFormattedDate("MM/dd/yyyy", ndate));


            }
        }, mYear, mMonth, mDay);


        Calendar cal = Calendar.getInstance();


        int selectedId = leadstatusradioGroup.getCheckedRadioButtonId();
        if (selectedId == hot.getId()) {
            cal.add(Calendar.DAY_OF_MONTH, 7);
            mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

        } else if (selectedId == warm.getId()) {
            cal.add(Calendar.DAY_OF_MONTH, 15);
            mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
        } else if (selectedId == cold.getId()) {
            cal.add(Calendar.DAY_OF_MONTH, 25);
            mDatePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

        }


        mDatePicker.setTitle("Select FollowUp Date");
        Calendar minCal = Calendar.getInstance();
        mcurrentDate.add(Calendar.DATE, 1);
        mDatePicker.getDatePicker().setMinDate(minCal.getTimeInMillis());
        mDatePicker.show();


    }


    public void transferLead(View v) {

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Working..."); // Setting Message
        progressDialog.setTitle(null); // Setting Title
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);  // Progress Dialog Style Spinner
        check = ConnectionChecker.getInstance(mContext).Check();

        if (check) {
            if (!TextUtils.isEmpty(etCustomerName.getText().toString()) && !TextUtils.isEmpty(etContactPersonName.getText().toString())
                    && !TextUtils.isEmpty(etMobileNumber.getText().toString())) {
                if (!(etMobileNumber.getText().toString().length() == 10)) {
                    Toast.makeText(mContext, "Invalid Mobile Number", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.show();
                    salesmanList.clear();
                    if (TextUtils.isEmpty(etRate.getText().toString()) ||
                            TextUtils.isEmpty(etContactPersonAddress.getText().toString())
                            || TextUtils.isEmpty(etLocationOfEnquiry.getText().toString())) {
                        reg = 0;
                    }
                    sendEnquiryDetails(1, NULL, URLs.URL_FETCH_SALESMAN_LIST);
                    progressDialog.dismiss();
                }
            } else {
                etCustomerName.setError("Required");
                etContactPersonName.setError("Required");
                etMobileNumber.setError("Required");
                scrollView.scrollTo(0, 1300);
                Toast.makeText(mContext, "Please Fill All Required Fields*", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(mContext, "You need internet for transferring enquiry", Toast.LENGTH_LONG).show();
        }

    }


    public void transferEnquiry() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_dialog_transfer, null);
        dialogBuilder.setView(dialogView);

        smanList = dialogView.findViewById(R.id.salesmanList);
        TextView customerName = dialogView.findViewById(R.id.tvName);
        TextView customerNumber = dialogView.findViewById(R.id.tvNumber);
        TextView productName = dialogView.findViewById(R.id.tvProduct);


        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                AddEnquiryActivity.this, R.layout.product_list_spinner_item, salesmanList) {
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.product_list_spinner_item);
        smanList.setAdapter(spinnerArrayAdapter);

        customerName.setText(etContactPersonName.getText().toString());
        customerNumber.setText(etMobileNumber.getText().toString());
        productName.setText(prodName);


        dialogBuilder.setMessage(null);

        dialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);
                if (check)
                    getAllValues();
                sendEnquiryDetails(0, smanList.getSelectedItem().toString(), URLs.URL_ADD_ENQUIRY);

                //do something with edt.getText().toString();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white_background);
        b.show();

    }


    public void checkEnquiry(View v) {

        if (cbBrochure.isChecked()) {
            if (TextUtils.isEmpty(etContactPersonEmail.getText().toString())) {

                Toast.makeText(mContext, "Please enter Email Address", Toast.LENGTH_SHORT).show();
                return;

            }


        }


        if(!TextUtils.isEmpty(etCustomerName.getText().toString()) && !TextUtils.isEmpty(etContactPersonName.getText().toString())
         && !TextUtils.isEmpty(etMobileNumber.getText().toString())) {
         if(etMobileNumber.getText().toString().length()==10)
         {

             if(TextUtils.isEmpty(etRate.getText().toString()) ||
                     TextUtils.isEmpty(etContactPersonAddress.getText().toString())
                     || TextUtils.isEmpty(etLocationOfEnquiry.getText().toString()))
             {
                 reg=0;
             }

             //boolean isValidEmail = etContactPersonEmail.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+");
             boolean isValidEmail = Patterns.EMAIL_ADDRESS.matcher(etContactPersonEmail.getText().toString()).matches();

             if(!isValidEmail && !TextUtils.isEmpty(etContactPersonEmail.getText().toString())){
                etContactPersonEmail.setError("Invalid Email");
                etContactPersonEmail.requestFocus();
             }
             else {
                 if(productID!=0)
                   submitEnquiry();
                 else
                     Toast.makeText(mContext,"No Product Selected",Toast.LENGTH_SHORT).show();
             }

         }
         else{
             Toast.makeText(mContext,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
        }
        }
        else {
            etCustomerName.setError("Required");
            etContactPersonName.setError("Required");
            etMobileNumber.setError("Required");
            scrollView.scrollTo(0, 1300);
            Toast.makeText(mContext, "Please Fill All Required Fields*", Toast.LENGTH_LONG).show();
        }

    }


    public void submitEnquiry() {
        progressDialog = new ProgressDialog(AddEnquiryActivity.this, R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Adding Enquiry.Please Wait..."); // Setting Message
        progressDialog.setTitle(null); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        getAllValues();

        if (ConnectionChecker.getInstance(mContext).Check()) {

            //Invoking Function to send Notification data to server
            sendEnquiryDetails(0, NULL, URLs.URL_ADD_ENQUIRY);


        } else {
            //Saving enquiry locally
            Uenquiry uenquiry = new Uenquiry(productID, leadSource, subSource, customerName,
                    customerAddress, titleName, contactPerson, age, occupation, phone, mobileNumber, altMobileNumber,
                    contactPersonAddress, contactPersonEmail, purchaseMode, firstTime, interested,
                    status, locationOfEnquiry, dc.getFormattedDate("MM/dd/yyyy", dateOfEnquiry), dc.getFormattedDate("MM/dd/yyyy", nextFollowUp), rate,
                    0, 0, 0, reg, Integer.valueOf(id), enquiryNote, bankName);
            uenquiry.save();
            progressDialog.dismiss();
            Toast.makeText(mContext, "Notification Saved In device." +
                    "Will be automatically synced when connected to Internet", Toast.LENGTH_LONG).show();
            finish();

        }


    }


    public void getAllValues() {


        if (leadSource == 1 || leadSource == 2 || leadSource == 3 || leadSource == 6) {
            sub = Subsource.find(Subsource.class, "name = ?", sourceofInfo_list.getSelectedItem().toString());
            subSource = (int) (long) sub.get(0).getId();
        }


        //Getting All values
        customerName = etCustomerName.getText().toString();
        customerAddress = etCustomerAddress.getText().toString();

        titleName = title.getSelectedItemPosition() + 1;

        if (etContactPersonName.getText().toString().matches(NULL)) {
            titleName = 0;
            contactPerson = NULL;
        } else {
            contactPerson = etContactPersonName.getText().toString();
        }


        if (etAge.getText().toString().matches(NULL)) {
            age = 0;
        } else
            age = Integer.valueOf(etAge.getText().toString());

        occupation = etOccupation.getText().toString();

        if (etPhone.getText().toString().matches(NULL)) {
            phone = "0";
        } else
            phone = etPhone.getText().toString();

        mobileNumber = etMobileNumber.getText().toString();
        altMobileNumber = etAltMobileNumber.getText().toString();
        contactPersonAddress = etContactPersonAddress.getText().toString();
        contactPersonEmail = etContactPersonEmail.getText().toString();

        purchaseMode = purchase_mode.getSelectedItemPosition() + 1;

        int firstTimeId = firsttimeradioGroup.getCheckedRadioButtonId();
        if (firstTimeId == yes1.getId()) {
            firstTime = 1;
        } else if (firstTimeId == no1.getId()) {
            firstTime = 2;
        }


        int interestedId = interestedradioGroup.getCheckedRadioButtonId();
        if (interestedId == yes2.getId()) {
            interested = 1;
        } else if (interestedId == no2.getId()) {
            interested = 2;
        } else if (interestedId == dontKnow.getId()) {
            interested = 3;
        }

        int statusId = leadstatusradioGroup.getCheckedRadioButtonId();
        if (statusId == hot.getId()) {
            status = 7;
        } else if (statusId == warm.getId()) {
            status = 15;
        } else if (statusId == cold.getId()) {
            status = 25;
        }


        locationOfEnquiry = etLocationOfEnquiry.getText().toString();
        dateOfEnquiry = cdate;


        nextFollowUp = ndate;


        if (etRate.getText().toString().matches(NULL))
            rate = 0;
        else
            rate = Long.valueOf(etRate.getText().toString());

        enquiryNote = etNote.getText().toString();

        bankName = etBankName.getText().toString();


    }


//Function for sending added inquiry to server in online mode

    public void sendEnquiryDetails(final int mode, final String tName, final String URL) {


        // Volley's json array request object
        final StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {


                            JSONObject obj = new JSONObject(response);
                            if (obj.getBoolean("status")) {

                                switch (mode) {

                                    // Case 0 for inserting And Transferring Enquiries
                                    case 0:
                                        final long eid = obj.getLong("enquiry_id");
                                        //Actions performed for success for Insering new eNQUIRY
                                        if (tName.matches(NULL)) {


                                            //Saving enquiry locally
                                            Offenquiry oenquiry = new Offenquiry(eid, prodName, leadSource, subSource, customerName,
                                                    customerAddress, titleName, contactPerson, age, occupation, phone, mobileNumber, altMobileNumber,
                                                    contactPersonAddress, contactPersonEmail, purchaseMode, firstTime, interested,
                                                    status, locationOfEnquiry, dateOfEnquiry, nextFollowUp, rate,
                                                    0, 0, reg, enquiryNote, bankName);
                                            oenquiry.save();


                                            JSONObject counter = obj.getJSONObject("counters");

                                            int assigned = counter.getInt("enq_assigned");
                                            int pending = counter.getInt("enq_pending");
                                            int completed = counter.getInt("enq_completed");

                                            //Checking that counter does not go negative and then increasing counter
                                            if ((assigned + pending) >= (completed + 1)) {
                                                Counter count = Counter.findById(Counter.class, 1);
                                                count.assigned = assigned;
                                                count.pending = pending;
                                                int temp = count.completed + 1;
                                                count.completed = temp;
                                                count.save();
                                            }


                                            ActivityLog log = new ActivityLog(1, obj.getLong("time"), (int) eid);
                                            log.save();


                                            if (cbBrochure.isChecked()) {
                                                AlertDialog dialog = new AlertDialog.Builder(AddEnquiryActivity.this)
                                                        .setTitle("Added")
                                                        .setIcon(R.drawable.ic_info)
                                                        .setMessage("The Enquiry has been successfully added.Press Continue for quotation form")
                                                        .setPositiveButton("Continue",
                                                                new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {

                                                                        Intent i = new Intent(AddEnquiryActivity.this, QuotationFormActivity.class);
                                                                        i.putExtra("enquiry_id", String.valueOf(eid));
                                                                        i.putExtra("product_id", String.valueOf(productID));
                                                                        i.putExtra("rate", String.valueOf(rate));
                                                                        startActivity(i);
                                                                        finish();
                                                                    }


                                                                }).setNegativeButton("Cancel",
                                                                new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        finish();
                                                                    }
                                                                }).create();
                                                dialog.show();


                                            } else {
                                                AlertDialog dialog = new AlertDialog.Builder(AddEnquiryActivity.this)
                                                        .setTitle("Added")
                                                        .setIcon(R.drawable.ic_info)
                                                        .setMessage("The Enquiry has been successfully added")
                                                        .setPositiveButton("Ok",
                                                                new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        Intent myIntent = new Intent(mContext, EnquiryDetailsActivity.class);
                                                                        ActivityOptions options =
                                                                                ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_right, R.anim.slide_left);
                                                                        myIntent.putExtra("enquiry_id", eid);
                                                                        mContext.startActivity(myIntent, options.toBundle());
                                                                        finish();
                                                                    }


                                                                }).create();
                                                dialog.show();

                                            }


                                        } else {
                                            ActivityLog log = new ActivityLog(2, obj.getLong("time"), (int) eid);
                                            log.save();
                                            AlertDialog dialog = new AlertDialog.Builder(AddEnquiryActivity.this)
                                                    .setTitle("Transferred")
                                                    .setIcon(R.drawable.ic_info)
                                                    .setMessage("The Enquiry has been successfully transferred")
                                                    .setPositiveButton("Continue",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    finish();
                                                                }
                                                            }).create();
                                            dialog.show();
                                        }

                                        break;

                                    //Case 1 for fetching salesman list
                                    case 1:
                                        String exception = SharedPreManager.getInstance(mContext).getUser().getUsername();
                                        if (obj.getBoolean("status")) {
                                            JSONArray enquiriesArray = obj.getJSONArray("data");
                                            for (int i = 0; i < enquiriesArray.length(); i++) {

                                                JSONObject enquiry = enquiriesArray.getJSONObject(i);
                                                String Name = enquiry.getString("name");
                                                if (!Name.matches(exception))
                                                    salesmanList.add(Name);


                                            }

                                        }
                                        transferEnquiry();
                                        break;

                                }
                            } else {

                                Toast.makeText(mContext, obj.getString("message"), Toast.LENGTH_LONG).show();

                            }

                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                            progressDialog.dismiss();

                        }


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //This indicates that the reuest has either time out or there is no connection
                    Toast.makeText(getApplicationContext(), "Timeout Error ", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    //Error indicating that there was an Authentication Failure while performing the request
                    Toast.makeText(getApplicationContext(), "Authentication Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    //Indicates that the server responded with a error response
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    //Indicates that there was network error while performing the request
                    Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    // Indicates that the server response could not be parsed
                    Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_LONG).show();
                }


                ResyncEnquiries.getInstance(mContext).fetchserverEnquiries();
                progressDialog.dismiss();

                finish();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                if (mode == 0) {
                    String edate = dc.getFormattedDate("MM/dd/yyyy", dateOfEnquiry);
                    String fdate = dc.getFormattedDate("MM/dd/yyyy", nextFollowUp);

                    params.put("id", id);
                    params.put("product_id", String.valueOf(productID));
                    params.put("source_of_enquiry", String.valueOf(leadSource));
                    params.put("sub_source", String.valueOf(subSource));
                    params.put("customer_name", customerName);
                    params.put("customer_address", customerAddress);
                    params.put("contact_title", String.valueOf(titleName));
                    params.put("contact_person", contactPerson);
                    params.put("contact_age", String.valueOf(age));
                    params.put("contact_occupation", occupation);
                    params.put("contact_phone",phone);
                    params.put("contact_number", mobileNumber);
                    params.put("alt_contact_number", altMobileNumber);
                    params.put("contact_address", contactPersonAddress);
                    params.put("contact_email", contactPersonEmail);
                    params.put("purchase_mode", String.valueOf(purchaseMode));
                    params.put("first_time", String.valueOf(firstTime));
                    params.put("competition_interest", String.valueOf(interested));
                    params.put("status", String.valueOf(status));
                    params.put("location_of_enquiry", locationOfEnquiry);
                    params.put("date_of_enquiry", edate);
                    params.put("next_followup_date", fdate);
                    params.put("rate", String.valueOf(rate));
                    params.put("enquiry_note", enquiryNote);
                    params.put("bank_name", bankName);
                    params.put("is_reg", String.valueOf(reg));
                    params.put("current_date", dc.getCurrentDate());

                }

                if (!tName.matches(NULL)) {
                    params.put("customer_name", customerName);
                    params.put("product_name", prodName);
                    params.put("transferred_by", SharedPreManager.getInstance(mContext).getUser().getUsername());
                    params.put("insert_mode", "1");
                    params.put("id", id);
                    params.put("salesman_name", tName);

                } else {
                    params.put("insert_mode", "0");
                }

                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_notifications: {
                Intent i = new Intent(getBaseContext(), NotificationActivity.class);
                startActivity(i);
                break;
            }


        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
