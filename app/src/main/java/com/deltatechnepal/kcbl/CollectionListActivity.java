package com.deltatechnepal.kcbl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.adapter.CollectionListAdapter;
import com.deltatechnepal.adapter.EprAdapter;
import com.deltatechnepal.helper.ConnectionChecker;
import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.ResyncEnquiries;
import com.deltatechnepal.orm.Collection;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.service.CollectionsFetchJob;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class CollectionListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvCollection;
    private CollectionListAdapter adapteroff;
    private List<Collection> coll;
    private final String KEY_RECYCLER_STATE = "recycler_state";
    private static Bundle mBundleRecyclerViewState;
    String previousValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvCollection = findViewById(R.id.rvEnquiry);
        linearLayoutManager =
                new LinearLayoutManager(CollectionListActivity.this);
        rvCollection.setLayoutManager(linearLayoutManager);
        rvCollection.setHasFixedSize(true);
        rvCollection.setNestedScrollingEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        if (Collection.count(Collection.class) > 0 && ConnectionChecker.getInstance(mContext).Check())
            fetchlocalCollections();

        swipeRefreshLayout.setOnRefreshListener(this);


    }


    @Override
    public void onRefresh() {

        swipeRefreshLayout.setRefreshing(true);
        fetchlocalCollections();
        swipeRefreshLayout.setRefreshing(false);


    }


    private void fetchlocalCollections() {

        coll = Collection.find(Collection.class,"followup_start_flag = ?","0");
        adapteroff = new CollectionListAdapter(CollectionListActivity.this, coll);
        rvCollection.setAdapter(adapteroff);


    }







    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_normal, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchItem.collapseActionView();
                fetchlocalCollections();
                return false;

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print

                fetchMatchingCollections(query);
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    public void fetchMatchingCollections(String value){
        previousValue = "%" + value + "%";

        coll = Collection.find(Collection.class, "customer_name LIKE ? or customer_mobile LIKE ?"
                ,previousValue, previousValue);
        adapteroff = new CollectionListAdapter(CollectionListActivity.this, coll);
        rvCollection.setAdapter(adapteroff);


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    public void onResume(){
        super.onResume();
        onRefresh();
        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            rvCollection.getLayoutManager().onRestoreInstanceState(listState);
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = rvCollection.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    @Override
    public void onPause() {
        super.onPause();
        // save RecyclerView state
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = rvCollection.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);

    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}












