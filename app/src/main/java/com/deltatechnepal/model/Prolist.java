package com.deltatechnepal.model;

public class Prolist {
    private long productId;
    private String imageName;
    private String productName;
    private int productQuantity;
    private long productPrice;


    public Prolist() {

    }
    public Prolist(long productId,String imageName,String productName, int productQuantity,long productPrice) {
        this.productId=productId;
        this.imageName=imageName;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    }

    public long getProductId() {
        return productId;
    }

    public String getImageName() {
        return imageName;
    }

    public String getProductName() {
        return productName;
    }

    public int getProductQuantity() {
        return productQuantity;
    }


    public long getProductPrice() {
        return productPrice;
    }
}