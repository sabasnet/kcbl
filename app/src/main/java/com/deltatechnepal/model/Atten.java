package com.deltatechnepal.model;

public class Atten {
    private int atten_id;
    private String atten_time;
    private int atten_type;
    private String atten_location;
    private String sync_status;

    public Atten () {

    }
    public Atten(int atten_id, String atten_time) {
        this.atten_id = atten_id;
        this.atten_time = atten_time;
    }

    public int getAttenID() {
        return this.atten_id;
    }
    public void setAttenID(int attenID) {
        this.atten_id = attenID;
    }

    public String getAttenTime() {
        return this.atten_time;
    }
    public void setAttenTime(String attenTime) {
        this.atten_time = attenTime;
    }

    public int getAttenType() {
        return this.atten_type;
    }
    public void setAttenType(int attenType) {
        this.atten_type = attenType;
    }

    public String getSyncStatus() {
        return this.sync_status;
    }
    public void setSyncStatus(String syncStatus) {
        this.sync_status = syncStatus;
    }
}