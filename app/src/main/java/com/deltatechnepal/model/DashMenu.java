package com.deltatechnepal.model;

/**
 * Created by DeltaTech on 5/21/2018.
 */

public class DashMenu {
    public int counter;
    public String title;
    public String activity;
    public int drawable;


    public DashMenu() {
    }

    public DashMenu(int counter,String title, String activity ,int drawable) {
        this.counter=counter;
        this.title = title;
        this.activity = activity;
        this.drawable = drawable;
    }
}
