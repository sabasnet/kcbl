package com.deltatechnepal.orm;

import com.deltatechnepal.helper.DateConversion;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class ActivityLog extends SugarRecord {
    @Ignore
    DateConversion dc;
    public int activityType;
    public long activityTime;
    public long activityId;


    public ActivityLog() {

    }

    public ActivityLog(int activityType, long activityTime, long activityId) {
        this.activityType=activityType;
        this.activityTime= activityTime;
        this.activityId=activityId;

    }

    public String getActivityTime() {
        dc=new DateConversion();
        return dc.getFormattedDate("MM/dd/yyyy HH:mm",activityTime) ;
    }
}
