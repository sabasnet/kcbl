package com.deltatechnepal.orm;

import com.deltatechnepal.helper.DateConversion;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class CollectionFollowUps extends SugarRecord {

    @Ignore
    DateConversion dc;
    public String customerMobile;
    public int followUpStatus;
    public int collectedAmount;
    public int nextFollowUpMode;
    public long nextFollowUpDate;
    public String followUpNote;
    public long createdAt;





    public CollectionFollowUps() {
    }

    public CollectionFollowUps(String customerMobile, int followUpStatus, int collectedAmount,
                               int nextFollowUpMode, long nextFollowUpDate, String followUpNote,long createdAt) {

        this.customerMobile = customerMobile;
        this.followUpStatus=followUpStatus;
        this.collectedAmount=collectedAmount;
        this.nextFollowUpMode=nextFollowUpMode;
        this.followUpNote=followUpNote;
        this.nextFollowUpDate=nextFollowUpDate;
        this.createdAt=createdAt;


    }

}