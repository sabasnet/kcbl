package com.deltatechnepal.orm;


import com.orm.SugarRecord;

public class Counter extends SugarRecord {

    public int assigned;
    public int pending;
    public int completed;

    public Counter() {
    }

    public Counter(int assigned, int pending,int completed) {
        this.assigned= assigned;
        this.pending = pending;
        this.completed=completed;


    }
}