package com.deltatechnepal.orm;


import com.deltatechnepal.helper.DateConversion;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;

public class Notification extends SugarRecord {
    @Ignore
    DateConversion dc;
    public long notificationId;
    public long actionId;
    public String title;
    public String description;
    public int category;
    public long date;
    public int markRead;
    public String transBy;
    public long unixTimeStamp;

    public Notification() {
    }

    public Notification(long notificationId,
                        long actionId,String title,
                        String description,int category,
                        String currdate,
                        int markRead,String transBy,long unixTimeStamp) {
        this.notificationId=notificationId;
        this.actionId=actionId;
        this.title = title;
        this.description = description;
        dc=new DateConversion();
        this.date=dc.getMilliAlt(currdate);
        this.category=category;
        this.markRead=markRead;
        this.transBy=transBy;
        this.unixTimeStamp=unixTimeStamp;


    }
    public int getNotificationCount(){
        List<Notification> notif=Notification.find(Notification.class,"mark_read = ?","0" );
     return notif.size();
     }

    public static boolean getNotificationById(long notificationId){
        List<Notification> notif=Notification.find(Notification.class,"notification_id = ?",String.valueOf(notificationId) );
        if(notif.size()==0)
            return false;

        return true;
    }




}