package com.deltatechnepal.orm;

import com.orm.SugarRecord;

public class Brochure extends SugarRecord {
    private int productId;
    private String brochure;


    public Brochure() {
    }

    public Brochure(int productId,String brochure) {
        this.productId=productId;
        this.brochure = brochure;

    }


    public int getProductId() {
        return productId;
    }

    public String getBrochure() {
        return brochure;
    }


    public boolean isBrochureListEmpty(){

        if(Brochure.count(Brochure.class)!=0)
            return false;
        else
        return true;


    }


}