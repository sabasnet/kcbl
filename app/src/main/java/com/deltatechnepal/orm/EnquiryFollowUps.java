package com.deltatechnepal.orm;

import com.deltatechnepal.helper.DateConversion;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class EnquiryFollowUps extends SugarRecord {

    @Ignore
    DateConversion dc;
    public String enquiryId;
    public long productId;
    public int nextFollowUpMode;
    public String remark;
    public long createdAt;
    public long nextFollowUpDate;
    public int followUpStatus;




    public EnquiryFollowUps() {
    }

    public EnquiryFollowUps(String enquiryId,String productId, int nextFollowUpMode,String remark,
                            long createdAt,long nextFollowUpDate,int followUpStatus) {

        this.enquiryId = enquiryId;
        this.productId=Long.valueOf(productId);
        this.nextFollowUpMode=Integer.valueOf(nextFollowUpMode);
        this.remark=remark;
        this.createdAt=createdAt;
        this.nextFollowUpDate=nextFollowUpDate;
        this.followUpStatus=Integer.valueOf(followUpStatus);


    }
    public String getCreatedAtDate() {
        dc=new DateConversion();
        return dc.getFormattedDate("MM/dd/yyyy H:m:s",createdAt);
    }
    public String getNextFollowUpDate() {
        dc=new DateConversion();
        return dc.getFormattedDate("MM/dd/yyyy",nextFollowUpDate);
    }
}