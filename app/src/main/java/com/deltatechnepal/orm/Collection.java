package com.deltatechnepal.orm;

import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.kcbl.CollectionFollowUp;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

/**
 * Created by DeltaTech on 5/22/2018.
 */

public class Collection extends SugarRecord{
    public int customerId;
    public String customerMobile;
    public String customerName;
    public String pendingAmount;
    public long nextFollowupDate;
    public String uploadedOn;
    public String lessThanThirty;
    public String betweenThirtySixty;
    public String betweenSixtyNinety;
    public String greaterThanNinety;
    public String onAccount;
    public int followupStartFlag;

    public Collection() {

    }

    public Collection(int customerId,
                      String customerMobile,
                      String customerName,
                      String pendingAmount,
                      long nextFollowupDate,
                      String uploadedOn,String lessThanThirty,
                              String betweenThirtySixty,
                              String betweenSixtyNinety,
                              String greaterThanNinety,
                              String onAccount, int followupStartFlag) {
        this.customerId=customerId;
        this.customerMobile= customerMobile;
        this.customerName=customerName;
        this.pendingAmount=pendingAmount;
        this.nextFollowupDate=nextFollowupDate;
        this.uploadedOn=uploadedOn;
        this.lessThanThirty=lessThanThirty;
        this.betweenThirtySixty=betweenThirtySixty;
        this.betweenSixtyNinety=betweenSixtyNinety;
        this.greaterThanNinety=greaterThanNinety;
        this.onAccount=onAccount;
        this.followupStartFlag=followupStartFlag;
        }

    public String getNextFollowupDate() {
        DateConversion dc=new DateConversion();
        if(nextFollowupDate!=0)
        return dc.getFormattedDate("MM/dd/yyyy",nextFollowupDate);
        else
            return "";
    }

    public int getCollectionFollowUpCount(){
        DateConversion dc= new DateConversion();
        List<Collection>  coll = Select.from(Collection.class)
                .where(Condition.prop("next_followup_date").lt(dc.getTomorrowDateinMilli()),Condition.prop("next_followup_date").notEq(0))
                .orderBy("next_followup_date DESC")
                .list();

        return coll.size();
    }

    public int getFreshCollectionCount(){
        DateConversion dc= new DateConversion();
        List<Collection>  coll = Collection.find(Collection.class,"followup_start_flag = ?","0");
        return coll.size();
    }


}
