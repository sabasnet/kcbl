package com.deltatechnepal.orm;

import com.deltatechnepal.helper.DateConversion;
import com.google.gson.JsonObject;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class Offenquiry extends SugarRecord {

    public long enquiryId;
    public String productName;
    public int leadSource;
    public int sourceofInfo;
    public String customerName;
    public String customerAddress;
    public int titleName;
    public String contactPerson;
    public int age;
    public String occupation;
    public String phone;
    public String mobileNumber;
    public String altMobileNumber;
    public String contactPersonAddress;
    public String contactPersonEmail;
    public int purchaseMode;
    public int firstTime;
    public int interestedInComp;
    public int status;
    public String locationofEnquiry;
    public long dateofEnquiry;
    public long nextFollowUpDate;
    public long rate;
    public int enqResult;
    public int reason;
    public int reg;
    public String note;
    public String bankName;



    public Offenquiry() {
    }


    public Offenquiry ( long enquiryId, String productName,
             int leadSource,
             int sourceofInfo,
             String customerName,
             String customerAddress,
             int titleName,
             String contactPerson,
             int age,
             String occupation,
             String phone,
             String mobileNumber,
             String altMobileNumber,
             String contactPersonAddress,
             String contactPersonEmail,
             int purchaseMode,
             int firstTime,
             int interestedInComp,
             int status,
             String locationofEnquiry,
             long dateofEnquiry,
             long nextFollowUpDate,
             long rate,
             int enqResult,
             int reason,
             int reg,
                        String note,String bankName) {
        this.enquiryId=enquiryId;
        this.productName=productName;
        this.leadSource=leadSource;
        this.sourceofInfo=sourceofInfo;
        this.customerName=customerName;
        this.customerAddress=customerAddress;
        this.titleName=titleName;
        this.contactPerson=contactPerson;
        this.age=age;
        this.occupation=occupation;
        this.phone=phone;
        this.mobileNumber=mobileNumber;
        this.altMobileNumber=altMobileNumber;
        this.contactPersonAddress=contactPersonAddress;
        this.contactPersonEmail=contactPersonEmail;
        this.purchaseMode=purchaseMode;
        this.firstTime=firstTime;
        this.interestedInComp=interestedInComp;
        this.status=status;
        this.locationofEnquiry=locationofEnquiry;
        this.dateofEnquiry=dateofEnquiry;
        this.nextFollowUpDate=nextFollowUpDate;
        this.rate=rate;
        this.bankName=bankName;
        this.enqResult=enqResult;
        this.reason=reason;
        this.reg=reg;
        this.note=note;
        }

    public String getCustomDiffChecker() {
        return contactPerson+productName;
    }




    public int getEPRCount(){

     List<Offenquiry> off = Select.from(Offenquiry.class)
             .where(Condition.prop("reg").eq(0),Condition.prop("reason").eq(0))
             .list();


     return off.size();
    }


    public int getFollowUpCount(){
        DateConversion dc= new DateConversion();
        List<Offenquiry>  off = Select.from(Offenquiry.class)
                .where(Condition.prop("enq_result").eq(0),Condition.prop("next_follow_up_date").lt(dc.getTomorrowDateinMilli()))
                .orderBy("next_follow_up_date DESC")
                .list();

        return off.size();
    }

    public List<Offenquiry> findEnquiry(long enquiry_id) {
        List<Offenquiry> enq=Offenquiry.find(Offenquiry.class,"enquiry_id = ? ",String.valueOf(enquiry_id));
        return enq;
    }

}