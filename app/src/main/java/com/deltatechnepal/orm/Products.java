package com.deltatechnepal.orm;

import com.orm.SugarRecord;

public class Products extends SugarRecord {
    public long productId;
    public String productName;
    public int productCategory;
    public int defaultPrice;
    public String imageName;


    public Products() {
    }

    public Products(long productId,String productName, int defaultPrice,String imageName,int productCategory) {
        this.productName = productName;
        this.defaultPrice = defaultPrice;
        this.productId=productId;
        this.imageName=imageName;
        this.productCategory=productCategory;

    }
}