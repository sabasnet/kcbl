package com.deltatechnepal.orm;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class Uenquiry extends SugarRecord {

    public long productId;
    public int leadSource;
    public int sourceofInfo;
    public String customerName;
    public String customerAddress;
    public int titleName;
    public String contactPerson;
    public int age;
    public String occupation;
    public String phone;
    public String mobileNumber;
    public String altMobileNumber;
    public String contactPersonAddress;
    public String contactPersonEmail;
    public int purchaseMode;
    public int firstTime;
    public int interestedInComp;
    public int status;
    public String locationofEnquiry;
    public String dateofEnquiry;
    public String nextFollowUpDate;
    public long rate;
    public int enqResult;
    public long enquiryId;
    public int reason;
    public int reg;
    public int userId;
    public String note;
    public String bankName;



    public Uenquiry() {
    }


    public Uenquiry (  long productId,
                         int leadSource,
                         int sourceofInfo,
                         String customerName,
                         String customerAddress,
                         int titleName,
                         String contactPerson,
                         int age,
                         String occupation,
                         String phone,
                         String mobileNumber,
                         String altMobileNumber,
                         String contactPersonAddress,
                         String contactPersonEmail,
                         int purchaseMode,
                         int firstTime,
                         int interestedInComp,
                         int status,
                         String locationofEnquiry,
                         String dateofEnquiry,
                         String nextFollowUpDate,
                         long rate,
                         int enqResult,
                         long enquiryId,
                         int reason,
                         int reg,
                         int userId,
                       String note,
                       String bankName) {
        this.productId=productId;
        this.leadSource=leadSource;
        this.sourceofInfo=sourceofInfo;
        this.customerName=customerName;
        this.customerAddress=customerAddress;
        this.titleName=titleName;
        this.contactPerson=contactPerson;
        this.age=age;
        this.occupation=occupation;
        this.phone=phone;
        this.mobileNumber=mobileNumber;
        this.altMobileNumber=altMobileNumber;
        this.contactPersonAddress=contactPersonAddress;
        this.contactPersonEmail=contactPersonEmail;
        this.purchaseMode=purchaseMode;
        this.firstTime=firstTime;
        this.interestedInComp=interestedInComp;
        this.status=status;
        this.locationofEnquiry=locationofEnquiry;
        this.dateofEnquiry=dateofEnquiry;
        this.nextFollowUpDate=nextFollowUpDate;
        this.rate=rate;
        this.enqResult=enqResult;
        this.enquiryId=enquiryId;
        this.reason=reason;
        this.reg=reg;
        this.userId=userId;
        this.note=note;
        this.bankName=bankName;
    }


    public static String  getUnsyncedData(){
        List<Uenquiry> logs = Uenquiry.listAll(Uenquiry.class);
        String strData = new Gson().toJson(logs);
        return strData;
    }




}