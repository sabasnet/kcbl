package com.deltatechnepal.orm;

import com.orm.SugarRecord;

public class Category extends SugarRecord {
    private int categoryId;
    private String categoryName;


    public Category() {
    }

    public Category(int categoryId,String categoryName) {
        this.categoryId=categoryId;
        this.categoryName = categoryName;

    }

    public int getCategoryId() {

        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }


}
