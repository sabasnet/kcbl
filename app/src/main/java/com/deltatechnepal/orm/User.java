package com.deltatechnepal.orm;

public class User {

    private int id;
    private String username, email,image,designation,number;

    public User(int id, String username, String email,String image,String designation,String number) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.image=image;
        this.designation=designation;
        this.number=number;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getImage() {
        return image;
    }

    public String getDesignation() {
        return designation;
    }

    public String getNumber() {
        return number;
    }
}
