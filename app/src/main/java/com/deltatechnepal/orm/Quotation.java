package com.deltatechnepal.orm;

import com.deltatechnepal.helper.DateConversion;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

/**
 * Created by DeltaTech on 5/22/2018.
 */

public class Quotation{
    public int quotationId;
    public String documentType;
    public String productList;
    public String grandTotal;
    public String sentDate;

    public Quotation() {

    }

    public Quotation( int quotationId,String documentType,
             String productList,
                       String grandTotal,String sentDate) {
        this.quotationId=quotationId;
       this.documentType=documentType;
       this.productList=productList;
       this.grandTotal=grandTotal;
       this.sentDate=sentDate;
        }


}
