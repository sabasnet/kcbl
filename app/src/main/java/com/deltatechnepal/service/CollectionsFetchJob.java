package com.deltatechnepal.service;


import android.support.annotation.NonNull;
import android.widget.Toast;

import com.deltatechnepal.helper.fetchCategories;
import com.deltatechnepal.helper.fetchCollections;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.concurrent.TimeUnit;

public class CollectionsFetchJob extends Job {
    boolean result;
    public static final String TAG = "job_fetch_collections";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        fetchCollections sdata = new fetchCollections();

        result = sdata.initializeFetch();
        if (result)
            return Result.SUCCESS;
        else
            return Result.FAILURE;



    }
    public static void scheduleJob() {
        new JobRequest.Builder(CollectionsFetchJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(1400), TimeUnit.MINUTES.toMillis(5))
                .setUpdateCurrent(true) // calls cancelAllForTag(NoteSyncJob.TAG) for you
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
        //
    }

    public static void runJobImmediately() {
        int jobId = new JobRequest.Builder(CollectionsFetchJob.TAG)
                .startNow()
                .build()
                .schedule();
    }

}
