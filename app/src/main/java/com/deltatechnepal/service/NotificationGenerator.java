package com.deltatechnepal.service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.kcbl.AddEnquiryActivity;
import com.deltatechnepal.kcbl.NotificationActivity;
import com.deltatechnepal.kcbl.R;

public class NotificationGenerator {
    private static NotificationGenerator mInstance;
    private Context mCtx;


    private NotificationGenerator(Context context) {
        mCtx = context;
    }

    public static synchronized NotificationGenerator getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NotificationGenerator(context);
        }
        return mInstance;
    }

    public void addProductDeleteNotification(String extra) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(mCtx)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle("Removed Product")
                        .setContentText(extra + " has been removed");

        Intent notificationIntent = new Intent(mCtx, NotificationActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mCtx, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    public void addProductUpdateNotification(String extra) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(mCtx)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle("Updated Product")
                        .setContentText(extra + " has been updated");

        Intent notificationIntent = new Intent(mCtx, NotificationActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mCtx, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }


    public void addAssignNotification(int enq_assigned, long enquiryId) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(mCtx)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle("Daily Assign")
                        .setContentText("Enquiries Assigned: " + String.valueOf(enq_assigned));

        Intent notificationIntent = new Intent(mCtx, NotificationActivity.class);
        notificationIntent.putExtra("enquiry_id", enquiryId);
        PendingIntent contentIntent = PendingIntent.getActivity(mCtx, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
        Intent intent = new Intent("notification-count-update");
        LocalBroadcastManager.getInstance(mCtx).sendBroadcast(intent);
    }

    public void addTransferNotification(String trans_by) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(mCtx)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle("Lead Transfer")
                        .setContentText("Transeferred by: " + trans_by);

        Intent notificationIntent = new Intent(mCtx, NotificationActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mCtx, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }


    public void addProductNotification(String productName, long productId) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(mCtx)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle("New Product Added")
                        .setContentText("Product Name: " + productName);

        Intent notificationIntent = new Intent(mCtx, AddEnquiryActivity.class);
        notificationIntent.putExtra("product_id", productId);
        PendingIntent contentIntent = PendingIntent.getActivity(mCtx, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }
}
