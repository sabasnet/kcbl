
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.deltatechnepal.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.deltatechnepal.helper.DateConversion;
import com.deltatechnepal.helper.SharedPreManager;
import com.deltatechnepal.kcbl.AddEnquiryActivity;
import com.deltatechnepal.kcbl.EnquiryDetailsActivity;
import com.deltatechnepal.kcbl.EprActivity;
import com.deltatechnepal.kcbl.NotificationActivity;
import com.deltatechnepal.kcbl.R;
import com.deltatechnepal.orm.Brochure;
import com.deltatechnepal.orm.Category;
import com.deltatechnepal.orm.Counter;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Offenquiry;
import com.deltatechnepal.orm.Products;
import com.deltatechnepal.orm.Token;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String trans_by, extra, date;
    String productName, customerName;
    long enquiryId, productId, actionID, unixTime, notifID;
    JSONObject obj;
    int pushMode, enq_assigned;
    Notification notification;
    Intent intent;

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Log.i("remote_message", message.getData().toString());
        /*sendMyNotification(message.getNotification().getBody());*/
        String data = message.getData().get("message");
        Log.d("ReceivedData", data);
        try {
            obj = new JSONObject(data);
            pushMode = obj.getInt("push_mode");
            if(pushMode==0){
                if(SharedPreManager.getInstance(getApplicationContext()).isLoggedIn())
                SharedPreManager.getInstance(getApplicationContext()).logout();
            }
            else if (pushMode <= 5) {
                date = obj.getString("notification_date");
                notifID = obj.getInt("notif_id");
                actionID = obj.getInt("action_id");
                unixTime = obj.getLong("unix_time");
                if (!Notification.getNotificationById(notifID)) {
                    intent = new Intent("notification-count-update");
                }
            }
            switch (pushMode) {

                case 1:
                    trans_by = obj.getString("transferred_from");
                    if (!trans_by.equalsIgnoreCase(SharedPreManager.getInstance(this).getUser().getUsername())) {
                        saveTransferredEnquiry();
                        notification = new Notification(notifID, actionID, "Lead Transfer",
                                "A lead belonging to " + customerName + " for the product " + productName + " was transferred.", 1,
                                date, 0, trans_by, unixTime);
                        notification.save();
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                    break;
                case 2:
                    saveProduct();
                    notification = new Notification(notifID, actionID, "New Product Added",
                            "A new product " + productName + " has been added to the product list.", 2,
                            date, 0, "", unixTime);
                    notification.save();

                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                    break;
                case 3:
                    List<Products> proRem = Products.find(Products.class, "product_id = ?", String.valueOf(actionID));
                    if (proRem.size() > 0) {

                        extra = proRem.get(0).productName;
                        notification = new Notification(notifID, actionID, "Removed Product",
                                "The product " + proRem.get(0).productName + " has been removed from the product list.", 3,
                                date, 0, "", unixTime);
                        notification.save();
                        proRem.get(0).delete();
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        NotificationGenerator.getInstance(this).addProductDeleteNotification(extra);
                    }
                    break;
                case 4:
                    List<Products> proUp = Products.find(Products.class, "product_id = ?", String.valueOf(actionID));
                    if (proUp.size() > 0) {
                        extra = proUp.get(0).productName;
                        updateProduct();
                        NotificationGenerator.getInstance(this).addProductUpdateNotification(extra);
                        notification = new Notification(notifID, actionID, "Updated Product",
                                "The product " + productName + " has been updated.", 4,
                                date, 0, "", unixTime);
                        notification.save();

                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }

                    break;
                case 5:

                    enq_assigned = obj.getInt("enq_assigned");
                    int enq_pending = obj.getInt("enq_pending");
                    Counter count = Counter.findById(Counter.class, 1);
                    count.assigned = enq_assigned;
                    count.pending = enq_pending;
                    count.completed = 0;
                    count.save();

                    notification = new Notification(notifID, actionID, "Daily Assign",
                            "You have been assigned a total of " + String.valueOf(enq_assigned) + " daily enquiry today ", 5,
                            date, 0, "", unixTime);
                    notification.save();


                    NotificationGenerator.getInstance(this).addAssignNotification(enq_assigned, enq_pending);
                    break;

                case 6:
                    Brochure.deleteAll(Brochure.class);
                    Token.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "BROCHURE" + "'");
                    BrochureFetchJob.runJobImmediately();
                    break;
                case 7:
                    Brochure.deleteAll(Brochure.class);
                    Brochure.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "BROCHURE" + "'");
                    BrochureFetchJob.runJobImmediately();
                    break;
                case 8:
                    CategoriesFetchJob.runJobImmediately();
                    break;
                case 9:
                    CategoriesFetchJob.runJobImmediately();
                    break;


            }

        } catch (JSONException e) {
            Log.e("Error :", "JSON Parsing error: " + e.getMessage());
        }

    }


    public void saveTransferredEnquiry() {
        try {
            JSONArray enq = obj.getJSONArray("transferred_enquiry");
            JSONObject receivedData = enq.getJSONObject(0);
            String productId = receivedData.getString("product_id");
            List<Products> products = Products.find(Products.class, "product_id = ?", productId);
            productName = products.get(0).productName;
            enquiryId = receivedData.getLong("id");
            int sourceofEnquiry = receivedData.getInt("source_of_enquiry");
            int subSource = receivedData.getInt("sub_source");
            customerName = receivedData.getString("customer_name");
            String customerAddress = receivedData.getString("customer_address");
            int titleName = receivedData.getInt("contact_title");
            String contactPerson = receivedData.getString("contact_person");
            int contactAge = receivedData.getInt("contact_age");
            String contactOccupation = receivedData.getString("contact_occupation");
            String contactPhone = receivedData.getString("contact_phone");
            String contactNumber = receivedData.getString("contact_number");
            String altContactNumber = receivedData.getString("alt_contact_number");
            String contactAddress = receivedData.getString("contact_address");
            String contactEmail = receivedData.getString("contact_email");
            int purchaseMode = receivedData.getInt("purchase_mode");
            int firstTime = receivedData.getInt("first_time");
            int interestedComp = receivedData.getInt("competition_interest");
            int status = receivedData.getInt("status");
            String locationofEnquiry = receivedData.getString("location_of_enquiry");
            String dateofEnquiry = receivedData.getString("doe");
            String nextFollowUpDate = receivedData.getString("next_followup_date");
            long rate = receivedData.getLong("rate");
            int result = receivedData.getInt("result");
            int reason = receivedData.getInt("reason");
            int reg = receivedData.getInt("is_reg");
            String note = receivedData.getString("note");
            String bankName = receivedData.getString("bank_name");
            DateConversion dc = new DateConversion();
            Offenquiry off = new Offenquiry(enquiryId, productName, sourceofEnquiry, subSource, customerName, customerAddress, titleName,
                    contactPerson, contactAge,
                    contactOccupation, contactPhone, contactNumber, altContactNumber, contactAddress, contactEmail, purchaseMode,
                    firstTime, interestedComp,
                    status, locationofEnquiry, dc.getMilli(dateofEnquiry), dc.getMilli(nextFollowUpDate),
                    rate, result, reason, reg, note, bankName);
            off.save();
            NotificationGenerator.getInstance(this).addTransferNotification(trans_by);

        } catch (JSONException e) {

            Log.e("Error :", "JSON Parsing error: " + e.getMessage());

        }


    }


    public void saveProduct() {
        try {
            String product = obj.getString("product");
            JSONObject enq = new JSONObject(product);
            productName = enq.getString("name");
            int defaultPrice = enq.getInt("default_price");
            productId = enq.getLong("id");
            String imageName = obj.getString("picture_name");
            int category = enq.getInt("category_id");

            Products productList = new Products(productId, productName, defaultPrice, imageName, category);
            productList.save();


            NotificationGenerator.getInstance(this).addProductNotification(productName, productId);
        } catch (JSONException e) {

            Log.e("Error :", "JSON Parsing error: " + e.getMessage());


        }


    }

    public void updateProduct() {
        try {
            String product = obj.getString("product");
            JSONObject enq = new JSONObject(product);
            productName = enq.getString("name");
            int defaultPrice = enq.getInt("default_price");
            productId = enq.getLong("id");
            String imageName = obj.getString("picture_name");
            int category = enq.getInt("category_id");

            List<Products> pro = Products.find(Products.class, "product_id = ? ", String.valueOf(productId));
            Products prod = pro.get(0);
            prod.productName = productName;
            prod.defaultPrice = defaultPrice;
            prod.productCategory = category;
            prod.save();


            NotificationGenerator.getInstance(this).addProductUpdateNotification(extra);
        } catch (JSONException e) {

            Log.e("Error :", "JSON Parsing error: " + e.getMessage());


        }


    }


}