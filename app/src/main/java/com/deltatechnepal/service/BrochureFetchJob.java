package com.deltatechnepal.service;


import android.support.annotation.NonNull;

import com.deltatechnepal.helper.fetchBrochures;
import com.deltatechnepal.helper.syncEnquiry;
import com.deltatechnepal.orm.Uenquiry;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class BrochureFetchJob extends Job {
    boolean result;
    public static final String TAG = "job_fetch_brochure";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        fetchBrochures sdata = new fetchBrochures();

        result = sdata.initializeFetch();
        if (result)
            return Result.SUCCESS;
        else
            return Result.FAILURE;
    }


    public static void runJobImmediately() {
        int jobId = new JobRequest.Builder(BrochureFetchJob.TAG)
                .startNow()
                .build()
                .schedule();
    }

}
