package com.deltatechnepal.service;

import android.support.annotation.NonNull;

import com.deltatechnepal.helper.syncEnquiry;
import com.deltatechnepal.helper.syncNotifications;
import com.deltatechnepal.orm.Notification;
import com.deltatechnepal.orm.Uenquiry;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class NotificationSyncJob extends Job {
    boolean result;
    public static final String TAG = "job_notification_sync";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        List<Notification> notif = Notification.find(Notification.class, "mark_read = ?", "1");
        if (notif.size() > 0) {
            syncNotifications sdata = new syncNotifications();

            result = sdata.initializeSync();

        }
        if (result) {
            return Result.SUCCESS;
        } else
            return Result.FAILURE;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(NotificationSyncJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(7))
                .setUpdateCurrent(true) // calls cancelAllForTag(NoteSyncJob.TAG) for you
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
        //
    }

    public static void runJobImmediately() {
        int jobId = new JobRequest.Builder(NotificationSyncJob.TAG)
                .startNow()
                .build()
                .schedule();
    }

}