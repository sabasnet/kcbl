package com.deltatechnepal.service;

import android.support.annotation.NonNull;

import com.deltatechnepal.helper.syncEnquiry;
import com.deltatechnepal.orm.Uenquiry;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class EnquirySyncJob extends Job {
    boolean result;
    public static final String TAG = "job_enquiry_sync";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        List<Uenquiry> uenquiries = Uenquiry.listAll(Uenquiry.class);
        if (uenquiries.size() > 0) {
            syncEnquiry sdata = new syncEnquiry();

            result = sdata.initializeSync();

        }
        if (result)
            return Result.SUCCESS;
        else
            return Result.FAILURE;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(EnquirySyncJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(7))
                .setUpdateCurrent(true) // calls cancelAllForTag(NoteSyncJob.TAG) for you
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
        //
    }

    public static void runJobImmediately() {
        int jobId = new JobRequest.Builder(EnquirySyncJob.TAG)
                .startNow()
                .build()
                .schedule();
    }

}