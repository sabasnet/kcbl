package com.deltatechnepal.service;

import android.util.Log;

import com.deltatechnepal.orm.Token;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        //For registration of token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //To displaying token on logcat
        Log.d("TOKEN: ", refreshedToken);

        if (Token.count(Token.class) == 0) {
            Token tk = new Token(refreshedToken);
            tk.save();
        }


    }


}