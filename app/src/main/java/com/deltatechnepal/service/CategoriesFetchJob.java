package com.deltatechnepal.service;


import android.support.annotation.NonNull;

import com.deltatechnepal.helper.fetchBrochures;
import com.deltatechnepal.helper.fetchCategories;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

public class CategoriesFetchJob extends Job {
    boolean result;
    public static final String TAG = "job_fetch_categories";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        fetchCategories sdata = new fetchCategories();

        result = sdata.initializeFetch();
        if (result)
            return Result.SUCCESS;
        else
            return Result.FAILURE;
    }


    public static void runJobImmediately() {
        int jobId = new JobRequest.Builder(CategoriesFetchJob.TAG)
                .startNow()
                .build()
                .schedule();
    }

}
